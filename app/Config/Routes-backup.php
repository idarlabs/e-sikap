<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Home::index');

$routes->get('/goldarah', 'Master/GolDarah::index');
$routes->post('/goldarah/tambah', 'Master/GolDarah::tambah');
$routes->post('/goldarah/delete/(:num)', 'Master/GolDarah::delete/$1');
$routes->post('/goldarah/update', 'Master/GolDarah::update');

$routes->get('/jeniskelamin', 'Master/JenisKelamin::index');
$routes->post('/jeniskelamin/tambah', 'Master/JenisKelamin::tambah');
$routes->post('/jeniskelamin/delete/(:num)', 'Master/JenisKelamin::delete/$1');
$routes->post('/jeniskelamin/update', 'Master/JenisKelamin::update');

$routes->get('/pekerjaan', 'Master/Pekerjaan::index');
$routes->post('/pekerjaan/tambah', 'Master/Pekerjaan::tambah');
$routes->post('/pekerjaan/delete/(:num)', 'Master/Pekerjaan::delete/$1');
$routes->post('/pekerjaan/update', 'Master/Pekerjaan::update');

$routes->get('/pendidikan', 'Master/Pendidikan::index');
$routes->post('/pendidikan/tambah', 'Master/Pendidikan::tambah');
$routes->post('/pendidikan/delete/(:num)', 'Master/Pendidikan::delete/$1');
$routes->post('/pendidikan/update', 'Master/Pendidikan::update');

$routes->get('/spesialisasi', 'Master/Spesialisasi::index');
$routes->post('/spesialisasi/tambah', 'Master/Spesialisasi::tambah');
$routes->post('/spesialisasi/delete/(:num)', 'Master/Spesialisasi::delete/$1');
$routes->post('/spesialisasi/update', 'Master/Spesialisasi::update');

$routes->get('/instalasi', 'Master/Instalasi::index');
$routes->post('/instalasi/tambah', 'Master/Instalasi::tambah');
$routes->post('/instalasi/delete/(:num)', 'Master/Instalasi::delete/$1');
$routes->post('/instalasi/update', 'Master/Instalasi::update');

$routes->get('/unit', 'Master/Unit::index');
$routes->post('/unit/tambah', 'Master/Unit::tambah');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
