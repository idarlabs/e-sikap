<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Pengajuan_all extends BaseController
{
	public function index()
	{
		if(session()->get('level') == "Admin"){
			$q1 = $this->db->query("select * from buka_menu")->getResult();
			$kodeperiode = "0";
			if(count($q1) > 0){
				$kodeperiode = $q1[0]->idperiode;
			}
			$modelpengajuan = new \App\Models\Pengajuan_all();
			// $modelpenilai = new \App\Models\ModelTimPenilai();
			// $datapenilai = $modelpenilai->datapenilaiku(session()->get('nip'))->getResult()[0];
			$datapengajuan = $modelpengajuan->data($kodeperiode)->getResult();
			$data =  [
				'bread' => 'Daftar Pengajuan',
				'h1' => 'Daftar Pengajuan',
				'datapengajuan' => $datapengajuan
			];
			// return var_dump($datapengajuan);die;
			return view('pengajuan_all/list', $data);
		}else{
			return redirect()->to('home');
		}
	}

	private function dd($data){
		return var_dump($data);die;
	}

	public function detail($id = null){
		if($id !== null){
			if(session()->get('level') == "Admin"){
				$model = new \App\Models\Riwayat_pengajuan();
				$pengajuan = $model->data($id)->getResultArray();
				$riwayat_pengajuan_baru = $model->ambildata($id, '3')->getResult();
				// var_dump($riwayat_pengajuan_baru); die;
				$modelpengajuan = new \App\Models\Pengajuan();
				$datapengaju = $modelpengajuan->datapengajuan($id)->getResult()[0];

				$pengajuansebelumnya = $this->db->table('log_pengajuan_terakhir')
				->where(['nip' => $datapengaju->nip])
				->orderBy('idlog','DESC')
				->limit(1)
				->get()
				->getResult();

				$pendidikan_1 = 0;
				$pendidikan_2 = 0;
				$pendidikan_3 = 0;
				$pemeriksaan = 0;
				$pengembangan = 0;
				$penunjang = 0; //penunjang kalo di form

				$datasubunsur = $this->db->query("select s.idsubunsur, s.keterangan FROM m_sub_unsur s inner join m_unsur u on u.idunsur = s.idunsur WHERE  s.idunsur = (select idunsur from m_unsur WHERE idkategori = '1' and idbidang = '".$datapengaju->idbidang."')")->getResult();

				foreach ($riwayat_pengajuan_baru as $rpb) {
					switch ($rpb->idkategori) {
						case '1':
							if($rpb->keterangan == $datasubunsur[0]->keterangan){
								$pendidikan_1 += $rpb->kredit;
							}else if($rpb->keterangan == $datasubunsur[1]->keterangan){
								$pendidikan_2 += $rpb->kredit;
							}else if($rpb->keterangan == $datasubunsur[2]->keterangan){
								$pendidikan_3 += $rpb->kredit;
							}
							break;
						case '2':
							$pemeriksaan += $rpb->kredit;
							break;
						case '3':
							$pengembangan += $rpb->kredit;
							break;
						case '4':
							$penunjang += $rpb->kredit;
							break;
						default:
							break;
					}
				}

				$jmlunsurutama = $pendidikan_1 + $pendidikan_2 + $pendidikan_3 + $pemeriksaan + $pengembangan;
				$jmlkeduanya = $jmlunsurutama + $penunjang;

				$pen1 = 0;
				$pen2 = 0;
				$pen3 = 0;
				$pem = 0;
				$peng = 0;
				$pen = 0;

				$jmlunsurutama2 = 0;
				$jmlkeduanya2 = 0;

				$pen1x = 0;
				$pen2x = 0;
				$pen3x = 0;
				$pemx = 0;
				$pengx = 0;
				$penx = 0;

				$jmlunsurutama2x = 0;
				$jmlkeduanya2x = 0;

				$semuariwayatpengaju = $model->semuariwayatpengaju($datapengaju->nip, '5')->getResult();

				if(count($semuariwayatpengaju) > 0){
					$riwayat_pengajuan_lama = $semuariwayatpengaju;
					foreach($riwayat_pengajuan_lama as $rpl):
						if($rpl->idpengajuan != $id){
							switch ($rpl->idkategori) {
								case '1':
									if($rpl->keterangan == $datasubunsur[0]->keterangan){
										$pen1 += $rpl->kredit;
									}else if($rpl->keterangan == $datasubunsur[1]->keterangan){
										$pen2 += $rpl->kredit;
									}else if($rpl->keterangan == $datasubunsur[2]->keterangan){
										$pen3 += $rpl->kredit;
									}
									break;
								case '2':
									$pem += $rpl->kredit;
									break;
								case '3':
									$peng += $rpl->kredit;
									break;
								case '4':
									$pen += $rpl->kredit;
									break;
								default:
									break;
							}
						}

					endforeach;
				}

				//tambah dengan nilai kredit awal
				$modelprecutoff = new \App\Models\Precutoff();
				$nilaiawal = $modelprecutoff->nilaiku($datapengaju->nip)->getResult();
				if($nilaiawal !== NULL){
					foreach($nilaiawal as $na){
						$pen1x = $na->pendidikan_1;
						$pen2x = $na->pendidikan_2;
						$pen3x = $na->pendidikan_3;
						$pemx = $na->pemeriksaan;
						$pengx = $na->pengembangan;
						$penx = $na->penunjang;
					}
				}
				$pen1 += $pen1x; $pen2 += $pen2x; $pen3 += $pen3x; $pem += $pemx; $peng += $pengx; $pen += $penx;
				$jmlunsurutama2 = $pen1 + $pen2 + $pen3 + $pem + $peng;
				$jmlkeduanya2 = $jmlunsurutama2 + $pen;

				$modelpengajuan = new \App\Models\ModelPengangkatan();
				$pengangkatan = $modelpengajuan->dataid($datapengaju->idgolongan)->getResult();

				$data =  [
					'bread' => 'Detail Pengajuan',
					'h1' => 'Detail Pengajuan',
					'pengajuan' => $pengajuan,
					'pendidikan_1' => $pendidikan_1,
					'pendidikan_2' => $pendidikan_2,
					'pendidikan_3' => $pendidikan_3,
					'pengembangan' => $pengembangan,
					'pemeriksaan' => $pemeriksaan,
					'penunjang' => $penunjang,
					'jmlunsurutama' => $jmlunsurutama,
					'jmlkeduanya' => $jmlkeduanya,

					'pen1' => $pen1,
					'pen2' => $pen2,
					'pen3' => $pen3,
					'pem' => $pem,
					'peng' => $peng,
					'pen' => $pen,
					'jmlunsurutama2' => $jmlunsurutama2,
					'jmlkeduanya2' => $jmlkeduanya2,

					'datapengaju' => $datapengaju,
					'pengangkatan' => $pengangkatan,
					'id' => $id,
					'idperiode' => $datapengaju->idperiode,
				];

				return view('pengajuan_all/detail', $data);
			}else{
				return redirect()->to('/home');
			}
		}else{
			return redirect()->to('/home');
		}
	}

	private function hitungunsur($pendidikan_1, $pendidikan_2, $pendidikan_3, $pemeriksaan, $penunjang){

	}

	function tolak(){
		if($this->request->getPost('validasi') === "go"){
			$model = new \App\Models\Riwayat_pengajuan();
			$ditolak = new \App\Models\PengajuanDitolak();
			$idriwayatpengajuan = $this->request->getPost('idriwayatpengajuan');
			$d = $model->ambilsebaris($idriwayatpengajuan);
			$dataditolak = [
				'idriwayatpengajuan' => $d['idriwayatpengajuan'],
				'idpengajuan' => $d['idpengajuan'],
				'tgl_submit' => date('Y-m-d'),
				'jam_submit' => date('h:i:s'),
				'kredit' => $d['kredit'],
				'idkegiatan' => $d['idkegiatan'],
				'idstep' => '4',
				'nip' => $d['nip'],
				'keterangan' => $this->request->getPost('keterangan')
			];
			$ditolak->simpan($dataditolak);
			$model->hapus($idriwayatpengajuan);
			return true;
		}
		return false;
	}

	function proses(){
		if($this->request->getPost('validasi') === "go"){
			$model = new \App\Models\Riwayat_pengajuan();
			$idriwayatpengajuan = $this->request->getPost('idriwayatpengajuan');
			$data = ['idstep' => '3'];
			$model->updatedata($data, $idriwayatpengajuan);
			return true;
		}
		return false;
	}

	public function submit(){
		if($this->request->isAjax()){
			$this->response->setContentType('application/json; charset=utf-8');
			$json_response=array();
			$dataInput = json_decode(file_get_contents('php://input'),true);
			if ($this->request->getPost('submit')) {
				$rules = [
					'file' => [
						'label' => 'File',
						'rules' => 'uploaded[file]',
						'errors' => [
							'uploaded' => 'Terdapat kegagalan sistem. Laporkan pengembang (err. {field} | {value} {param} | uploaded)',
						],
					],
					'idpengajuan' => [
						'label' => 'Pengajuan',
						'rules' => 'required|is_not_unique[pengajuan.idpengajuan]',
						'errors' => [
							'required' => 'Kolom {field} tidak boleh kosong',
							'is_not_unique' => '{field} yang dimaksud tidak tersedia',
						],
					],
					'sk' => [
						'label' => 'Nomor Surat Keputusan',
						'rules' => 'required',
						'errors' => [
							'required' => 'Kolom {field} tidak boleh kosong',
						],
					],
					'tmt' => [
						'label' => 'Terhitung Mulai Tanggal',
						'rules' => 'required',
						'errors' => [
							'required' => 'Kolom {field} tidak boleh kosong',
						],
					],
				];
				if ($this->validate($rules)) {
						$modelpegawai = new \App\Models\Pegawai_model();
						$modeltotalkredit = new \App\Models\ModelTotalKredit();
						$dbLogPengajuanTerakhir = new \App\Models\Log_pengajuan_terakhir();
						$riwayatbaru = new \App\Models\ModelRiwayatBaru();
						$riwayatlama = new \App\Models\ModelRiwayatLama();
						$dbBukaMenu = new \App\Models\Buka_menu();
						$dbPengangkatan = new \App\Models\Pengangkatan();
						$dbPengajuan = new \App\Models\Pengajuan();
						$dbRiwayatPengajuan = new \App\Models\Riwayat_pengajuan();
						$dbRiwayatDokumen = new \App\Models\Riwayat_dokumen();
						$dbPegawai = new \App\Models\Pegawai_model();
						$idPengajuan = $this->request->getPost('idpengajuan');
						$idRiwayatPengajuan = $dbRiwayatPengajuan->where('idpengajuan',$idPengajuan)
												->first()
												->idriwayatpengajuan;
						$idRiwayatDokumen = $dbRiwayatDokumen->where('idriwayatpengajuan',$idRiwayatPengajuan)
												->first()
												->idriwayatdokumen;
						$golonganSebelum = $modelpegawai->where('nip', $this->request->getPost('nip'))
						->first()
						->idgolongan;
						$golonganSetelah = $dbPengangkatan->where('gol_saatini',$golonganSebelum)
												->first()
												->gol_naik;
						$selectBukaMenu = $dbBukaMenu->first();
						$idPeriode = '';
						$tanggal = date('Y-m-d');
						$jam = date('h:i:s');
						if ($selectBukaMenu) {
							$idPeriode = $selectBukaMenu->idperiode;
						} else {
							$json_response['code'] = '0';
							$json_response['message']['text'] = 'Tidak ada periode yang sedang dibuka.';
							return $this->response->setJSON($json_response);
						}
						$filePath = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "doc" . DIRECTORY_SEPARATOR;
						$file = $this->request->getFile('file');
						$fileName = '';
						if ($file) {
							$fileName = $file->getRandomName();
							$file->move($filePath,$fileName);
						}
						$dataInsertLPT = [
							'nip' => $this->request->getPost('nip'),
							'idpengajuan' => $idPengajuan,
							'status' => "3",
							'sk' => $this->request->getPost('sk'),
							'tmt' => $this->request->getPost('tmt'),
							'gol_sebelum' => $golonganSebelum,
							'gol_setelah' => $golonganSetelah,
							'tgl_submit' => $tanggal,
							'jam_submit' => $jam,
							'idperiode' => $idPeriode,
							'path_dokumen' => $fileName,
						];

						$datariwayatbaru = $riwayatbaru->datalast($this->request->getPost('nip'))->getResultArray();
						if(count($datariwayatbaru) == 1){
							$tampungriwayatbaru = [];
							foreach($datariwayatbaru as $key => $drb){
								$tampungriwayatbaru['nip'] = $drb['nip'];
								$tampungriwayatbaru['idpengajuan'] = $drb['idpengajuan'];
								$tampungriwayatbaru['status'] = $drb['status'];
								$tampungriwayatbaru['sk'] = $drb['sk'];
								$tampungriwayatbaru['tmt'] = $drb['tmt'];
								$tampungriwayatbaru['gol_sebelum'] = $drb['gol_sebelum'];
								$tampungriwayatbaru['gol_setelah'] = $drb['gol_setelah'];
								$tampungriwayatbaru['tgl_submit'] = $drb['tgl_submit'];
								$tampungriwayatbaru['jam_submit'] = $drb['jam_submit'];
								$tampungriwayatbaru['idperiode'] = $drb['idperiode'];
								$tampungriwayatbaru['path_dokumen'] = $drb['path_dokumen'];
							}
							$riwayatlama->insert($tampungriwayatbaru);
						}
						$riwayatbaru->insert($dataInsertLPT);

						$dataMTK = [
							'pendidikan_1' => $this->request->getPost('pendidikan_1'),
							'pendidikan_2' => $this->request->getPost('pendidikan_2'),
							'pendidikan_3' => $this->request->getPost('pendidikan_3'),
							'pemeriksaan' => $this->request->getPost('pemeriksaan'),
							'pengembangan' => $this->request->getPost('pengembangan'),
							'penunjang' => $this->request->getPost('penunjang'),
							'pen1' => $this->request->getPost('pen1'),
							'pen2' => $this->request->getPost('pen2'),
							'pen3' => $this->request->getPost('pen3'),
							'pem' => $this->request->getPost('pem'),
							'peng' => $this->request->getPost('peng'),
							'pen' => $this->request->getPost('pen'),
							'nip' => $this->request->getPost('nip'),
							'idpengajuan' => $this->request->getPost('idpengajuan'),
							'idperiode' => $this->request->getPost('idperiode'),
							'nip_penilai' => $this->request->getPost('nip_penilai'),
						];

						$modeltotalkredit->simpan($dataMTK);

						$dataUpdatePegawai = [
							'idpangkat' => $this->request->getPost('idpangkat'),
							'idgolongan' => $this->request->getPost('idgolongan'),
							'tmt_pegawai' => $this->request->getPost('tmt'),
						];
						//Eksekusi DB
						$dbLogPengajuanTerakhir->transStart();
						$dbLogPengajuanTerakhir->insert($dataInsertLPT);
						$dbPengajuan->update($idPengajuan,['status' => '3']);
						$dbRiwayatPengajuan->where('idpengajuan',$idPengajuan)
							->where('idstep','3')
							->set(['idstep' => '5'])
							->update();
						$dbPegawai->where('nip',$this->request->getPost('nip'))
							->set($dataUpdatePegawai)
							->update();
						if ($dbLogPengajuanTerakhir->transStatus() === TRUE) {
							$dbLogPengajuanTerakhir->transCommit();
							$json_response['code'] = '1';
							$json_response['message']['text'] = 'Data berhasil diperbarui';
						} else {
							$dbLogPengajuanTerakhir->transRollback();
							$json_response['code'] = '0';
							$json_response['message']['text'] = 'Terjadi kegagalan sistem: ' . $dbLogPengajuanTerakhir->errors();
						}
				} else {
						$json_response['code'] = '0';
						$json_response['message']['text'] = $this->validator->listErrors();
				}
			}
			return $this->response->setJSON($json_response);
		}
	}

	public function exportPDF($id = null){
		if($id !== null){
			$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetHeader('Menteri Hukum Dan Hak Asasi Manusia||Direktorat Jenderal Kekayaan Intelektual');
			$mpdf->WriteHTML('
			<h3 align="center">Penetapan Angka Kredit</h3>
			<h4 align="center">Masa Penilaian: Juli s/d Desember 2020</h4>
			<table border="1" id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse;">
			<tr>
				<th colspan="5" align="left">I. KETERANGAN PERORANGAN</th>
			</tr>
			<tr>
			<td class="tengah">1</td>
			<td colspan="1" align="left">N A M A</td>
			<td colspan="3">RADEN NURUL ANWAR, SE, MM</td>
			</tr>
			<tr>
			<td class="tengah">2</td>
			<td colspan="1" align="left">N I P</td>
			<td colspan="3">197610202006041001</td>
			</tr>
			<tr>
			<td class="tengah">3</td>
			<td colspan="1" align="left">Nomor Seri Kartu Pegawai</td>
			<td colspan="3">N.157961</td>
			</tr>
			<tr>
			<td class="tengah">4</td>
			<td colspan="1" align="left">Pangkat & Golongan Ruang / Terhitung Mulai Tanggal</td>
			<td colspan="3">Pembina (IV/b) / 1 April 2020</td>
			</tr>
			<tr>
			<td class="tengah">5</td>
			<td colspan="1" align="left">Tempat dan Tanggal Lahir</td>
			<td colspan="3">Jakarta, 20 Oktober 1976</td>
			</tr>
			<tr>
			<td class="tengah">6</td>
			<td colspan="1" align="left">Jenis Kelamin</td>
			<td colspan="3">Laki-laki</td>
			</tr>
			<tr>
			<td class="tengah">7</td>
			<td colspan="1" align="left">Pendidikan Tertinggi</td>
			<td colspan="3">Pasca Sarjana (S2)</td>
			</tr>
			<tr>
			<td class="tengah">8</td>
			<td colspan="1" align="left">Jabatan Pemeriksa Merek / Terhitung Mulai Tanggal</td>
			<td colspan="3">Pemeriksa Merek Madya / 1 Maret 2016</td>
			</tr>
			<tr>
			<td class="tengah">9</td>
			<td colspan="1" align="left">Unit Kerja</td>
			<td colspan="3">Direktorat Merek dan Indikasi Geografis</td>
			</tr>
			<tr>
				<th colspan="2" align="left">II. PENETAPAN ANGKA KREDIT</th>
				<th class="tengah">LAMA</th>
				<th class="tengah">BARU</th>
				<th class="tengah">JUMLAH</th>
				</tr>
				<tr>
				<td class="tengah">1</td>
				<th colspan="4" align="left">UNSUR UTAMA</th>
				</tr>
				<tr>
				<td></td>
				<td>a. (1) Pendidikan Formal dan mencapai gelar / ijasah</td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat surat Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;(3) Diklat Prajabatan</td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<td>b. Pemeriksaan</td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<th align="left">JUMLAH UNSUR UTAMA</th>
				<th class="tengah"></th>
				<th class="tengah"></th>
				<th class="tengah"></th>
				</tr>
				<tr>
				<td class="tengah">2</td>
				<th colspan="4" align="left">UNSUR PENUNJANG</th>
				</tr>
				<tr>
				<td></td>
				<td>Pendukung Kegiatan Pemeriksaan</td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<th align="left">JUMLAH UNSUR PENUNJANG</th>
				<th class="tengah"></th>
				<th class="tengah"></th>
				<td class="tengah"></td>
				</tr>
				<tr>
				<td></td>
				<th align="left">JUMLAH UNSUR UTAMA DAN PENUNJANG</th>
				<th class="tengah"></th>
				<th class="tengah"></th>
				<td class="tengah text-danger"></td>
				</tr>
				<tr>
				<th class="tengah">III</th>
				<td colspan="4"><span><span class="text-danger"><strong>*</strong></span></span></td>
				<tr>
				</tr>
			</table>');

			return redirect()->to($mpdf->Output('Penetapan_Angka_Kredit.pdf', 'I'));
		}else{
			return redirect()->to(base_url().'/pengajuan');
		}
	}
}
