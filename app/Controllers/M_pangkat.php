<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_pangkat_model;

class M_pangkat extends BaseController
{

	protected $M_pangkat_model;
	
	public function __construct() {

        $this->m_pangkat = new M_pangkat_model();
    }

	public function index()
	{

		$data['m_pangkat'] = $this->m_pangkat->getM_pangkat();
		$data['page']	 = 'page/m_pangkat';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$data['page'] = 'page/m_pangkat_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$idpangkat = $this->request->getPost('idpangkat');
    	$pangkat = $this->request->getPost('pangkat');

    	$data = [
      		'idpangkat' => $idpangkat,
      		'pangkat' => $pangkat
    	];

    $simpan = $this->m_pangkat->insert_m_pangkat($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_pangkat')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$data['m_pangkat'] = $this->m_pangkat->getM_pangkat($id);
    	$data['page'] = 'page/m_pangkat_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idpangkat = $this->request->getPost('idpangkat');
		$pangkat = $this->request->getPost('pangkat');

		$data = [
			'idpangkat' => $idpangkat,
			'pangkat' => $pangkat
		];

	$ubah = $this->m_pangkat->update_m_pangkat($data, $idpangkat);
	
	if($ubah)
	{
		return redirect()->to(base_url('m_pangkat'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_pangkat->delete_m_pangkat($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_pangkat'));
		}
	}


	//--------------------------------------------------------------------

}
