<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\List_pengusul_model;
use App\Models\Periode_model;
use App\Models\Pegawai_model;
use App\Models\Jenis_pemeriksa_model;
use App\Models\Pengajuan_model;
use App\Models\Pengajuan;

class List_pengusul extends BaseController
{

	protected $List_pengusul_model;
	
	public function __construct() {

        $this->list_pengusul = new List_pengusul_model();
    }

	public function index()
	{

		// $data['list_pengusul'] = $this->list_pengusul->getList_pengusul();
        $this->periode = new Periode_model();
    	$data['periode'] = $this->periode->getPeriode();
        $this->jenis_pemeriksa = new Jenis_pemeriksa_model();
    	$data['jenis_pemeriksa'] = $this->jenis_pemeriksa->getJenis_pemeriksa();

        $data['list_pengajuan'] = '';
        $data['list_penilai'] = '';
		$data['page']	 = 'page/list_pengusul';
		echo view('web', $data);
	}

    public function search()
    { 
 
        $this->periode = new Periode_model();
        $data['periode'] = $this->periode->getPeriode();
        $this->jenis_pemeriksa = new Jenis_pemeriksa_model();
        $data['jenis_pemeriksa'] = $this->jenis_pemeriksa->getJenis_pemeriksa();
        
        $idperiode = $this->request->getPost('idperiode');
        $idbidang = $this->request->getPost('idbidang');
        $idpenilai = $this->request->getPost('idpenilai');

        // var_dump($idperiode);
        // $idperiode = '6';
        // $idbidang = '2';
        // $this->pengajuan = new Pengajuan();
        // $data['list_pengajuan'] = $this->pengajuan->dataSearchByPeriodeAndBidang($idPeriode, $idPengusul);

        $data['list_pengajuan']  =  $this->list_pengusul->getList_pengusul($idperiode, $idbidang);
        $data['list_penilai'] = $this->list_pengusul->getList_penilai($idperiode, $idbidang);
        // var_dump($data['list_pengajuan']);
        $data['page']	 = 'page/list_pengusul';
		echo view('web', $data);
    }
	public function savepenilai($idpengajuan=null)
    {
        $idpenilai = $this->request->getPost('idpenilai');
        
        $data = [
            'idpenilai' => $idpenilai
        ];
        
        $ubah = $this->list_pengusul->update_penilai($data, $idpengajuan);
	
        if($ubah)
        {
            return redirect()->to(base_url('list_pengusul'));

        }
    }
// 	public function add()
//   	{
// 		//Proteksi
//     	$this->eselon1 = new Eselon1_model();
//     	$data['eselon1'] = $this->eselon1->getEselon1();
//     	$data['page'] = 'page/eselon2_add';
//     	return view('web', $data);
//   	}

// 	public function save()
// 	{
    
//     	$ideselon1 = $this->request->getPost('ideselon1');
//     	$n_unitsatuankerja2 = $this->request->getPost('n_unitsatuankerja2');

//     	$data = [
//       		'ideselon1' => $ideselon1,
//       		'n_unitsatuankerja2' => $n_unitsatuankerja2
//     	];

//     $simpan = $this->eselon2->insert_eselon2($data);

//     if($simpan)
//     {

//       return redirect()->to(base_url('eselon2')); 
//     }
//   }

//     public function edit($id)
//   	{
// 		//Proteksi

//     	$this->eselon1 = new Eselon1_model();
//     	$data['eselon1'] = $this->eselon1->getEselon1();
//     	$data['eselon2'] = $this->eselon2->getEselon2($id);
//     	$data['page'] = 'page/eselon2_edit';
//     	return view('web', $data);
//   	}

// 	public function update()
// 	{
// 		$ideselon1 = $this->request->getPost('ideselon1');
// 		$ideselon2 = $this->request->getPost('ideselon2');
// 		$n_unitsatuankerja2 = $this->request->getPost('n_unitsatuankerja2');

// 		$data = [
// 			'ideselon1' => $ideselon1,
// 			'ideselon2' => $ideselon2,
// 			'n_unitsatuankerja2' => $n_unitsatuankerja2
// 		];

// 	$ubah = $this->eselon2->update_eselon2($data, $ideselon2);
	
// 	if($ubah)
// 	{
// 		return redirect()->to(base_url('eselon2'));
// 	}

// }

// 	public function delete($id)
// 	{

// 		$hapus = $this->eselon2->delete_eselon2($id);

// 		if($hapus)
// 		{
// 			return redirect()->to(base_url('eselon2'));
// 		}
// 	}


	//--------------------------------------------------------------------

}
