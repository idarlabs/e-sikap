<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\User_model;
use App\Models\Pegawai_model;
use App\Models\M_pangkat_model;
use App\Models\M_golongan_model;
use App\Models\M_Pendidikan_model;


class User extends BaseController {

  public function __construct() {

    $this->user = new User_model();

  }

	public function index(){
		$model = new User_model();
		$user = $model->data()->getResult();
		$data = [
      'h1' => 'Form User',
      'bread' => 'Daftar User',
      'user' => $user,
    ];
    return view("admin/user", $data);
	}

  public function addUser(){
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($dataInput['submit']) {
        $rules = [
          'nip' => [
              'label' => 'Nomor Induk Pegawai (NIP)',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'username' => [
              'label' => 'Username',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'password' => [
              'label' => 'Password',
              'rules' => 'required|min_length[3]',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',
                'min_length' => 'Kolom {field} harus terdiri minimal {param} karakter',

              ],
          ],
          'no_karpeg' => [
              'label' => 'Nomor Kartu Pegawai',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'nama' => [
              'label' => 'Nama',
              'rules' => 'required|min_length[3]',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',
                'min_length' => 'Kolom {field} harus terdiri minimal {param} karakter',

              ],
          ],
          'idgolongan' => [
              'label' => 'Golongan',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'tmt' => [
              'label' => 'Terhitung Mulai Tanggal',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'tempat_lahir' => [
              'label' => 'Tempat Lahir',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'tgl_lahir' => [
              'label' => 'Tanggal Lahir',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'jk' => [
              'label' => 'Jenis Kelamin',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'idpendidikan' => [
              'label' => 'Pendidikan',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'alamat' => [
              'label' => 'Alamat',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'telepon' => [
              'label' => 'Telepon',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
          'email' => [
              'label' => 'Email',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',
              ],
          ],
          'idbidang' => [
              'label' => 'Bidang',
              'rules' => 'required',
              'errors' => [
                'required' => 'Kolom {field} tidak boleh kosong',

              ],
          ],
        ];
        if ($this->validate($rules)) {
            $dataGolongan = explode('|',$dataInput['idgolongan']);
            $idgolongan = $dataGolongan[0];
            $idpangkat = $dataGolongan[1];
            $dataInsertUser = [
              'username' => $dataInput['username'],
              'password'  => $dataInput['password'],
              'email'  => $dataInput['email'],
              'idlevel'  =>  '2',
              'nip'  =>  $dataInput['nip'],
              'status'  => '1',
            ];
            $dataInsertPegawai = [
              'nip' => $dataInput['nip'],
              'no_karpeg' => $dataInput['no_karpeg'],
              'nama' => $dataInput['nama'],
              'idpangkat' => $idpangkat,
              'idgolongan' => $idgolongan,
              'tmt_pegawai' => $dataInput['tmt'],
              'tempat_lahir' => $dataInput['tempat_lahir'],
              'tgl_lahir' => $dataInput['tgl_lahir'],
              'jk' => $dataInput['jk'],
              'idpendidikan' => $dataInput['idpendidikan'],
              'alamat' => $dataInput['alamat'],
              'telepon' => $dataInput['telepon'],
              'email' => $dataInput['email'],
              'idbidang' => $dataInput['idbidang'],
            ];
            $dbUser = new \App\Models\User_model();
            $dbPegawai = new \App\Models\Pegawai_model();
            $dbUser->transStart();
            $dbUser->insert($dataInsertUser);
            $dbPegawai->insert($dataInsertPegawai);
            if ($dbUser->transStatus === FALSE) {
              $dbUser->transRollback();
              $json_response['code'] = '0';
              $json_response['message']['text'] = 'Error transaction. Check the query';
            } else { $dbUser->transCommit(); }
            $json_response['code'] = '1';
            $json_response['message']['text'] = 'berhasil';
        } else {
            $json_response['code'] = '0';
            $json_response['message']['text'] = $this->validator->listErrors();
        }
      }
      return $this->response->setJSON($json_response);
    }
  }

  public function getData(){
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($dataInput['submit']) {
        switch ($dataInput['jenis']) {
          case 'pangkat':
            $dbPangkat = new \App\Models\M_Pangkat();
            $json_response['code'] = '1';
            $json_response['result']['data'] = $dbPangkat->findAll();
            $json_response['message']['text'] = 'berhasil';
            break;
          case 'golongan':
            $dbGolongan = new \App\Models\M_Golongan();
            $json_response['code'] = '1';
            $json_response['result']['data'] = $dbGolongan->findAll();
            $json_response['message']['text'] = 'berhasil';
            break;
          case 'pendidikan':
            $dbPendidikan = new \App\Models\M_Pendidikan();
            $json_response['code'] = '1';
            $json_response['result']['data'] = $dbPendidikan->findAll();
            $json_response['message']['text'] = 'berhasil';
            break;
          default:
            $json_response['code'] = '0';
            $json_response['message']['text'] = 'Tidak ada data';
            break;
        }
      }
      return $this->response->setJSON($json_response);
    }
  }

  public function ubah_password()
  {
    $data['page'] = 'page/ubah_password';
    return view('web', $data);
  }

  public function update_password()
  {

    $userid = session()->get('userid');
    $cek = $this->user->getUser($userid);
    $pass = $cek['password'];

    $pwd_lama = $this->request->getPost('password_lama');
    $password_lama = password_hash($pwd_lama, PASSWORD_BCRYPT);
    $pwd_baru = $this->request->getPost('password_baru');
    $password_baru = password_hash($pwd_baru, PASSWORD_BCRYPT);
    // print_r($pass);
    // return $pass;
    if(!password_verify($pwd_lama,$pass))
    {
      session()->setFlashData('gagal', 'Gagal merubah password..!! Password tidak cocok!!');
      return redirect()->to(base_url('user/ubah_password'));
    }else{
      $data = [
        'password' => $password_baru
      ];
      $ubah = $this->user->update_user($data, $userid);

      if($ubah)
      {
        session()->setFlashData('success', 'Password berhasil diubah');
        return redirect()->to(base_url('user/ubah_password'));
      }
    }
  }

  public function profil()
  {

    $db = db_connect();
    $userid = session()->get('userid');
    $data['user'] = $this->user->getUser($userid);
    $this->pegawai = new Pegawai_model();
    $data['pegawai'] = $db->query("SELECT user.nip, pegawai.no_karpeg, pegawai.nama, m_pangkat.pangkat, m_golongan.golongan,
    pegawai.tmt_pegawai, pegawai.tempat_lahir, pegawai.tgl_lahir, pegawai.jk, m_pendidikan.pendidikan,
    pegawai.alamat, pegawai.telepon, pegawai.email FROM pegawai,user,m_pangkat,m_golongan,m_pendidikan
    WHERE userid=$_SESSION[userid] AND pegawai.idpangkat=m_pangkat.idpangkat AND pegawai.nip=user.nip
    AND pegawai.idgolongan=m_golongan.idgolongan AND pegawai.idpendidikan=m_pendidikan.idpendidikan")
    ->getResultArray();
    // return print_r($data);
    $data['page'] = 'page/profil';
    return view('web', $data);
  }

}
