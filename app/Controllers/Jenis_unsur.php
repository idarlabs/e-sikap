<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Jenis_unsur_model;

class Jenis_unsur extends BaseController
{

	protected $Jenis_unsur;
	
	public function __construct() {

        $this->jenis_unsur = new Jenis_unsur_model();
    }

	public function index()
	{

		$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
		$data['page']	 = 'page/jenis_unsur';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$data['page'] = 'page/jenis_unsur_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$idjenisunsur = $this->request->getPost('idjenisunsur');
    	$jenis = $this->request->getPost('jenis');

    	$data = [
      		'idjenisunsur' => $idjenisunsur,
      		'jenis' => $jenis
    	];

    $simpan = $this->jenis_unsur->insert_jenis_unsur($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('jenis_unsur')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur($id);
    	$data['page'] = 'page/jenis_unsur_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idjenisunsur = $this->request->getPost('idjenisunsur');
		$jenis = $this->request->getPost('jenis');

		$data = [
			'jenis' => $jenis,
		];

	$ubah = $this->jenis_unsur->update_jenis_unsur($data, $idjenisunsur);
	
	if($ubah)
	{
		return redirect()->to(base_url('jenis_unsur'));
	}

}

	public function delete($id)
	{

		$hapus = $this->jenis_unsur->delete_jenis_unsur($id);

		if($hapus)
		{
			return redirect()->to(base_url('jenis_unsur'));
		}
	}


	//--------------------------------------------------------------------

}
