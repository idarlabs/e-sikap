<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_pendidikan_model;


class M_pendidikan extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new M_pendidikan_model();
		$data['m_pendidikan'] = $model->getM_pendidikan();
		// dd($data['m_pendidikan']);
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/m_pendidikan';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new M_pendidikan_model();
		$data = array(
			'pendidikan' => $this->request->getPost('pendidikan'),			
		);
		$model->saveM_pendidikan($data);
		return redirect()->to(base_url('m_pendidikan'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new M_pendidikan_model();
		$id = $this->request->getPost('idpendidikan');
		$data = array (
			'pendidikan' => $this->request->getPost('pendidikan'),	
		);

		$model->updateM_pendidikan($data, $id);
		return redirect()->to(base_url('m_pendidikan'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new M_pendidikan_model();
		$id = $this->request->getPost('idpendidikan');
		$model->deleteM_pendidikan($id);
		return redirect()->to(base_url('m_pendidikan'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
