<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_unit_kerja_model;


class M_unit_kerja extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new M_unit_kerja_model();
		$data['m_unit_kerja'] = $model->getM_unit_kerja()->getResult();
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/m_unitkerja';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new M_unit_kerja_model();
		$data = array(
			'unit_kerja' => $this->request->getPost('unit_kerja'),			
		);
		$model->saveM_unit_kerja($data);
		return redirect()->to(base_url('m_unit_kerja'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new M_unit_kerja_model();
		$id = $this->request->getPost('idunitkerja');
		$data = array (
			'unit_kerja' => $this->request->getPost('unit_kerja'),	
		);

		$model->updateM_unit_kerja($data, $id);
		return redirect()->to(base_url('m_unit_kerja'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new M_unit_kerja_model();
		$id = $this->request->getPost('idunitkerja');
		$model->deleteM_unit_kerja($id);
		return redirect()->to(base_url('m_unit_kerja'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
