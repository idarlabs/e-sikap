<?php
namespace App\Controllers;

use App\Models\ModelSubUnsur;
use App\Models\ModelUnsur;
use App\Models\ModelButir1;
use App\Models\ModelJenisBidang;
use App\Models\Pegawai_model;

class Tambahpenilai extends BaseController
{
	public function index(){
		$model = new ModelJenisBidang();
		$modelpenilai = new \App\Models\ModelTimPenilai();
		$bidangnya = $model->data()->getResult();
		$penilai = $modelpenilai->data()->getResult();
    $data = [
      'h1' => 'Daftar Penilai',
      'bread' => 'Tambah Penilai',
      'bidangnya' => $bidangnya,
      'penilai' => $penilai,
    ];

		return view('admin/tambahpenilai', $data);
	}

  public function ambileselon(){
    if($this->request->getPost('validasi') === "go"){
      $model = new Pegawai_model();
      $idbidang = $this->request->getPost('idbidang');
      $peg = $model->data1($idbidang)->getResult();
      return json_encode($peg);
    }
    return false;
  }

  public function proses(){
		if($this->request->getPost('validasi') === "go"){
			$query = $this->db->query("select * from buka_menu");
			$bukamenu = $query->getResult();
			if(count($bukamenu) > 0){
				$model = new \App\Models\ModelTimPenilai();
				$nip = $this->request->getPost('nip');
				$idbidang = $this->request->getPost('idbidang');
				$ideselon = $this->request->getPost('ideselon');
				$idperiode = $bukamenu[0]->idperiode;
				$data = [
					'nip' => $nip,
					'idperiode' => $idperiode,
					'ideselon' => $ideselon,
					'idjenispemeriksa' => $idbidang,
					'status' => 1
				];
				$model->simpan($data);
				return "oke";
			}else{
				return "tidak oke";
			}
		}
  }

	public function hapus(){
		if($this->request->getPost('validasi') === "go"){
			$model = new \App\Models\ModelTimPenilai();
			$model->hapus($this->request->getPost('id'));
		}
	}
}
