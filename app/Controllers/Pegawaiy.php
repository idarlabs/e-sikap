<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_pendidikan_model;
use App\Models\M_pangkat_model;
use App\Models\M_golongan_model;
use App\Models\Eselon2_model;
use App\Models\Pegawai_model;

class Pegawaiy extends BaseController
{

//	protected $Pegawai_model;

//	public function __construct() {

//		$this->pegawai = new Pegawai_model();
//	}

	public function index()
	{
		$model = new Pegawai_model();
		$data['pegawai'] = $model->getPegawai()->getResult();
		$data['m_pangkat'] = $model->getM_pangkat()->getResult();
		$data['m_golongan'] = $model->getM_golongan()->getResult();
		$data['m_pendidikan'] = $model->getM_pendidikan()->getResult();
		$data['eselon2'] = $model->getEselon2()->getResult();
		$data['page']	 = 'page/pegawai';
		echo view('web', $data);
	}


	public function save()
	{
		$model = new Pegawai_model();

		$data = array(
			'idpendidikan' => $this->request->getPost('idpendidikan'),
			'idpangkat' => $this->request->getPost('idpangkat'),
			'idgolongan' => $this->request->getPost('idgolongan'),
			'ideselon2' => $this->request->getPost('ideselon2'),
			'nip' => $this->request->getPost('nip'),
			'nama' => $this->request->getPost('nama'),
			'no_karpeg' => $this->request->getPost('no_karpeg'),
			'tmt_pegawai' => $this->request->getPost('tmt_pegawai'),
			'tempat_lahir' => $this->request->getPost('tempat_lahir'),
			'tgl_lahir' => $this->request->getPost('tgl_lahir'),
			'jk' => $this->request->getPost('jk'),
			'alamat' => $this->request->getPost('alamat'),
			'telepon' => $this->request->getPost('telepon'),
			'email' => $this->request->getPost('email'),
			'ktp' => $this->request->getPost('ktp'),
		);

		$model->insert_pegawai($data);
		return redirect()->to(base_url('pegawai'));
	}

	public function update()
	{
		$model = new Pegawai_model();
		$id = $this->request->getPost('idpegawai');
		$data = array(
			'idpendidikan' => $this->request->getPost('idpendidikan'),
			'idpangkat' => $this->request->getPost('idpangkat'),
			'idgolongan' => $this->request->getPost('idgolongan'),
			'ideselon2' => $this->request->getPost('ideselon2'),
			'nip' => $this->request->getPost('nip'),
			'nama' => $this->request->getPost('nama'),
			'no_karpeg' => $this->request->getPost('no_karpeg'),
			'tmt_pegawai' => $this->request->getPost('tmt_pegawai'),
			'tempat_lahir' => $this->request->getPost('tempat_lahir'),
			'tgl_lahir' => $this->request->getPost('tgl_lahir'),
			'jk' => $this->request->getPost('jk'),
			'alamat' => $this->request->getPost('alamat'),
			'telepon' => $this->request->getPost('telepon'),
			'email' => $this->request->getPost('email'),
			'ktp' => $this->request->getPost('ktp'),
		);

		$model->update_pegawai($data, $id);
		return redirect()->to(base_url('pegawai'));

	}

	public function delete()
	{

        $model = new Pegawai_model();
        $id = $this->request->getPost('idpegawai');
        $model->delete_pegawai($id);
		return redirect()->to(base_url('pegawai'));
	}



	//--------------------------------------------------------------------

}
