<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Datapegawai_model;
use App\Models\Ms_jabatan_model;
use App\Models\Ms_pangkatgol_model;
use App\Models\Ms_pendidikan_model;


class Datapegawai extends BaseController
{
	
	protected $Datapegawai_model;
	
	public function __construct() {
		helper('form');
        $this->pegawai = new Datapegawai_model();
    }
	
	public function index()
	{
		//Proteksi
		$data['pegawai'] = $this->pegawai->getPegawai();
        $data['page'] = 'page/datapegawai';
        echo view('web', $data);
	}

	public function add()
	{
		$this->ms_jabatan_model = new Ms_jabatan_model();
		$data['ms_jabatan_model'] = $this->ms_jabatan_model->getMs_jabatan_model();
		$this->ms_pangkatgol_model = new Ms_pangkatgol_model();
		$data['ms_pangkatgol_model'] = $this->ms_pangkatgol_model->getMs_pangkatgol_model();
		$this->ms_pendidikan_model = new Ms_pendidikan_model();
		$data['ms_pendidikan_model'] = $this->ms_pendidikan_model->getMs_pangkatgol_model();

		$data['page'] = 'page/add_datapegawai';
		return view('web', $data);
	}
	
	public function save()
	{
		$id_jabatan = $this->request->getPost('id_jabatan');
		$id_pangkatgol = $this->request->getPost('id_pangkatgol');
		$id_pendidikan = $this->request->getPost('id_pendidikan');
		$nip = $this->request->getPost('nip');
		$username = $this->request->getPost('username');
		$pwd = $this->request->getPost('password');
		$password = md5($pwd);
		$level = $this->request->getPost('level');
		$status_user = $this->request->getPost('status_user');
		$foto = 
		$nama_pegawai = $this->request->getPost('nama_pegawai');
		$nomor_serikp = $this->request->getPost('nomor_serikp');
		$tt_pangkatgol = date('Y-m-d');
		$tempat_lahir = $this->request->getPost('tempat_lahir');
		$tanggal_lahir = date('Y-m-d');
		$jk = $this->request->getPost('jk');
		$tt_jabatan date('Y-m-d');
		$unit_kerja = $this->request->getPost('unit_kerja');
	}

	//--------------------------------------------------------------------

}
