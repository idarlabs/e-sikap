<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Eselon1_model;

class Eselon1 extends BaseController
{

	protected $Eselon1_model;
	
	public function __construct() {

        $this->eselon1 = new Eselon1_model();
    }

	public function index()
	{

		$data['eselon1'] = $this->eselon1->getEselon1();
		$data['page']	 = 'page/eselon1';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$data['page'] = 'page/eselon1_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$ideselon1 = $this->request->getPost('ideselon1');
    	$n_unitsatuankerja1 = $this->request->getPost('n_unitsatuankerja1');

    	$data = [
      		'ideselon1' => $ideselon1,
      		'n_unitsatuankerja1' => $n_unitsatuankerja1
    	];

    $simpan = $this->eselon1->insert_eselon1($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('eselon1')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$data['eselon1'] = $this->eselon1->getEselon1($id);
    	$data['page'] = 'page/eselon1_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$ideselon1 = $this->request->getPost('ideselon1');
		$n_unitsatuankerja1 = $this->request->getPost('n_unitsatuankerja1');

		$data = [
			'ideselon1' => $ideselon1,
			'n_unitsatuankerja1' => $n_unitsatuankerja1,
		];

	$ubah = $this->eselon1->update_eselon1($data, $ideselon1);
	
	if($ubah)
	{
		return redirect()->to(base_url('eselon1'));
	}

}

	public function delete($id)
	{

		$hapus = $this->eselon1->delete_eselon1($id);

		if($hapus)
		{
			return redirect()->to(base_url('eselon1'));
		}
	}


	//--------------------------------------------------------------------

}
