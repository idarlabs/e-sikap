<?php
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Pegawai_model;

class Pegawai extends Controller {

  public function index(){
    $model = new Pegawai_model();
    $pegawai = $model->data()->getResult();
    $data = [
      'h1' => 'Form Pegawai',
      'bread' => 'Daftar Pegawai',
      'pegawai' => $pegawai,
    ];
    return view("admin/pegawai", $data);
  }

}
