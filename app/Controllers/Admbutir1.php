<?php
namespace App\Controllers;

use App\Models\ModelSubUnsur;
use App\Models\ModelUnsur;
use App\Models\ModelButir1;

class Admbutir1 extends BaseController
{
	public function index(){
		$model = new ModelSubUnsur();
		$model1 = new ModelUnsur();
		$model2 = new ModelButir1();
		$m_sub_unsur = $model->data()->getResult();
		$m_unsur = $model1->data()->getResult();
		$butir1 = $model2->data()->getResult();
    $data = [
      'h1' => 'Form Butir Kegiatan',
      'bread' => 'Tambah Sub Unsur',
			'm_unsur' => $m_unsur,
			'm_sub_unsur' => $m_sub_unsur,
			'butir1' => $butir1,
    ];

		return view('kegiatan/admbutir1', $data);
	}

	public function simpan(){
		$model = new ModelButir1();
		$data = [
			'keterangan' => $this->request->getPost('keterangan'),
			'idunsur' => $this->request->getPost('unsur'),
			'idsubunsur' => $this->request->getPost('subunsur')
		];
		$model->simpan($data);
		return redirect()->to(base_url('admbutir1'));
	}

	public function edit(){
		$model = new ModelButir1();
		$id = $this->request->getPost('idbutir1e');
		$keterangan = $this->request->getPost('keterangane');
		$unsur = $this->request->getPost('unsure');
		$subunsur = $this->request->getPost('subunsure');
		$data = array (
			'keterangan' => $keterangan,
			'idunsur' => $unsur,
			'idsubunsur' => $subunsur,
		);

		$model->edit($data, $id);
		return redirect()->to(base_url('admbutir1'));
	}
	//
	//
	// public function hapus(){
	// 	$model = new ModelUnsur();
	// 	$id = $this->request->getPost('idunsurx');
	// 	$model->hapus($id);
	// 	return redirect()->to(base_url('admunsur'));
	// }
}
