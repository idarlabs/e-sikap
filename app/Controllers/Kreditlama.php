<?php
namespace App\Controllers;

use App\Models\Precutoff;

class Kreditlama extends BaseController {

	public function index(){
    if(session()->get('username')==''){
			return redirect()->to(base_url('login'));
		}else{
			$data = [
				'h1' => 'Daftar Pegawai',
				'bread' => 'Kredit Pegawai',
			];
      if(session()->get('level') == "Admin"){
        $model = new Precutoff();
        $data['precutoff'] = $model->data()->getResult();
				return view('admin/kreditlama', $data);
			}else{
        return redirect()->to(base_url('home'));
      }
    }
	}

  public function tambah(){
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($dataInput['submit']) {
        $model = new Precutoff();

        $data = [
          'nip' => $dataInput['nip'],
          'pendidikan_1' => $dataInput['pendidikan_1'],
          'pendidikan_2' => $dataInput['pendidikan_2'],
          'pendidikan_3' => $dataInput['pendidikan_3'],
          'pemeriksaan' => $dataInput['pemeriksaan'],
          'pengembangan' => $dataInput['pengembangan'],
          'penunjang' => $dataInput['penunjang']
        ];
        if($model->simpan($data)){
          $json_response['code'] = '1';
          $json_response['message']['text'] = 'Berhasil';
        }else{
          $json_response['code'] = '0';
          $json_response['message']['text'] = 'Gagal';
        }
      }
      return $this->response->setJSON($json_response);
    }
  }

}
