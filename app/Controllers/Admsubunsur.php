<?php
namespace App\Controllers;

use App\Models\ModelSubUnsur;
use App\Models\ModelUnsur;

class Admsubunsur extends BaseController
{
	public function index(){
		$model = new ModelSubUnsur();
		$model1 = new ModelUnsur();
		$m_sub_unsur = $model->data()->getResult();
		$m_unsur = $model1->data()->getResult();
    $data = [
      'h1' => 'Form Sub Unsur',
      'bread' => 'Tambah Sub Unsur',
			'm_unsur' => $m_unsur,
			'm_sub_unsur' => $m_sub_unsur,
    ];

		return view('kegiatan/admsubunsur', $data);
	}

	public function simpan(){
		$model = new ModelSubUnsur();
		$data = [
			'keterangan' => $this->request->getPost('keterangan'),
			'idunsur' => $this->request->getPost('unsur')
		];
		$model->simpan($data);
		return redirect()->to(base_url('admsubunsur'));
	}

	public function edit(){
		$model = new ModelSubUnsur();
		$id = $this->request->getPost('idsubunsure');
		$keterangan = $this->request->getPost('keterangane');
		$unsur = $this->request->getPost('unsure');
		$data = array (
			'keterangan' => $keterangan,
			'idunsur' => $unsur
		);

		$model->edit($data, $id);
		return redirect()->to(base_url('admsubunsur'));
	}
	//
	// public function hapus(){
	// 	$model = new ModelUnsur();
	// 	$id = $this->request->getPost('idunsurx');
	// 	$model->hapus($id);
	// 	return redirect()->to(base_url('admunsur'));
	// }
}
