<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_jabatan_model;

class M_jabatan extends BaseController
{

	protected $M_jabatan_model;
	
	public function __construct() {

        $this->m_jabatan = new M_jabatan_model();
    }

	public function index()
	{

		$data['m_jabatan'] = $this->m_jabatan->getM_jabatan();
		$data['page']	 = 'page/m_jabatan';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$data['page'] = 'page/m_jabatan_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$idjabatan = $this->request->getPost('idjabatan');
    	$jabatan = $this->request->getPost('jabatan');

    	$data = [
      		'idjabatan' => $idjabatan,
      		'jabatan' => $jabatan
    	];

    $simpan = $this->m_jabatan->insert_m_jabatan($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_jabatan')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$data['m_jabatan'] = $this->m_jabatan->getM_jabatan($id);
    	$data['page'] = 'page/m_jabatan_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idjabatan = $this->request->getPost('idjabatan');
		$jabatan = $this->request->getPost('jabatan');

		$data = [
			'idjabatan' => $idjabatan,
			'jabatan' => $jabatan,
		];

	$ubah = $this->m_jabatan->update_m_jabatan($data, $idjabatan);
	
	if($ubah)
	{
		return redirect()->to(base_url('m_jabatan'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_jabatan->delete_m_jabatan($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_jabatan'));
		}
	}


	//--------------------------------------------------------------------

}
