<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Ms_kelompok_model;


class Ms_kelompok extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new Ms_kelompok_model();
		$data['ms_kelompok'] = $model->getMs_kelompok()->getResult();
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/ms_kelompok';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new Ms_kelompok_model();
		$data = array(
			'nama_kelompok' => $this->request->getPost('nama_kelompok'),			
		);
		$model->saveMs_kelompok($data);
		return redirect()->to(base_url('ms_kelompok'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new Ms_kelompok_model();
		$id = $this->request->getPost('id_kelompok');
		$data = array (
			'nama_kelompok' => $this->request->getPost('nama_kelompok'),		
		);

		$model->updateMs_kelompok($data, $id);
		return redirect()->to(base_url('ms_kelompok'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new Ms_kelompok_model();
		$id = $this->request->getPost('id_kelompok');
		$model->deleteMs_kelompok($id);
		return redirect()->to(base_url('ms_kelompok'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
