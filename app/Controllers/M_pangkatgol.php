<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_pangkatgol_model;


class M_pangkatgol extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new M_pangkatgol_model();
		$data['m_pangkatgol'] = $model->getM_pangkatgol()->getResult();
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/m_pangkatgol';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new M_pangkatgol_model();
		$data = array(
			'namapangkat' => $this->request->getPost('namapangkat'),
			'namagolongan' => $this->request->getPost('namagolongan'),						
		);
		$model->saveM_pangkatgol($data);
		return redirect()->to(base_url('m_pangkatgol'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new M_pangkatgol_model();
		$id = $this->request->getPost('idpangkatgol');
		$data = array (
			'namapangkat' => $this->request->getPost('namapangkat'),
			'namagolongan' => $this->request->getPost('namagolongan'),					
		);

		$model->updateM_pangkatgol($data, $id);
		return redirect()->to(base_url('m_pangkatgol'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new M_pangkatgol_model();
		$id = $this->request->getPost('idpangkatgol');
		$model->deleteM_pangkatgol($id);
		return redirect()->to(base_url('m_pangkatgol'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
