<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_sub_unsur_model;
use App\Models\M_unsur_model;
use App\Models\Jenis_unsur_model;

class M_sub_unsur extends BaseController
{

	protected $M_sub_unsur;

	public function __construct() {

        $this->m_sub_unsur = new M_sub_unsur_model();
    }

	public function index()
	{

		$data['m_sub_unsur'] = $this->m_sub_unsur->getM_sub_unsur();
		$data['page']	 = 'page/m_sub_unsur';
		echo view('web', $data);
	}

	public function tampilkanunsur(){
		if($this->request->getPost('validasi') === "go"){
			$modelunsur = new \App\Models\ModelUnsur();
			$idjenisunsur = $this->request->getPost('idjenisunsur');
			$hasil = $modelunsur->dataid($idjenisunsur,session()->get('idbidang'))->getResult();
			return json_encode($hasil);
		}
		return false;
	}

	public function tampilkanunsur2(){
		if($this->request->getPost('validasi') === "go"){
			$modelunsur = new \App\Models\ModelUnsur();
			$idbidang = $this->request->getPost('idbidang');
			$idjenisunsur = $this->request->getPost('idjenisunsur');
			$hasil = $modelunsur->data2($idjenisunsur, $idbidang)->getResult();
			return json_encode($hasil);
		}
		return false;
	}

	public function add()
  	{
		//Proteksi
    	$this->m_unsur = new M_unsur_model();
    	$data['m_unsur'] = $this->m_unsur->getM_unsur();
		$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['page'] = 'page/m_sub_unsur_add';
    	return view('web', $data);
  	}

	public function save()
	{
			$modelunsur = new \App\Models\ModelSubUnsur();
    	$idunsur = $this->request->getPost('idunsur');
			$idjenisunsur = $this->request->getPost('idjenisunsur');
			$idbidang = $this->request->getPost('idbidang');
    	$keterangan = $this->request->getPost('keterangan');

    	$data = [
					'idjenisunsur' => $idjenisunsur,
      		'idunsur' => $idunsur,
      		'idbidang' => $idbidang,
      		'keterangan' => $keterangan
    	];

    if($modelunsur->simpan($data))
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_sub_unsur'));
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->m_unsur = new M_unsur_model();
    	$data['m_unsur'] = $this->m_unsur->getM_unsur();
    	$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['m_sub_unsur'] = $this->m_sub_unsur->getM_sub_unsur($id);
    	$data['page'] = 'page/m_sub_unsur_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idunsur = $this->request->getPost('idunsur');
		$idjenisunsur = $this->request->getPost('idjenisunsur');
		$idsubunsur = $this->request->getPost('idsubunsur');
		$keterangan = $this->request->getPost('keterangan');

		$data = [
			'idunsur' => $idunsur,
			'idjenisunsur' => $idjenisunsur,
			'idsubunsur' => $idsubunsur,
			'keterangan' => $keterangan
		];

	$ubah = $this->m_sub_unsur->update_m_sub_unsur($data, $idsubunsur);

	if($ubah)
	{
		return redirect()->to(base_url('m_sub_unsur'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_sub_unsur->delete_m_sub_unsur($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_sub_unsur'));
		}
	}


	//--------------------------------------------------------------------

}
