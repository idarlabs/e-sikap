<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_pendidikan_model;
use App\Models\M_pangkat_model;
use App\Models\M_golongan_model;
use App\Models\Eselon2_model;
use App\Models\Tim_penilai_model;
use App\Models\Jenis_pemeriksa_model;

class Tim_penilai extends BaseController
{

	public function index()
	{
		$model = new \App\Models\ModelTimPenilai();
		$data = [
      'h1' => 'Form Tim Penilai',
      'bread' => 'Daftar Tim Penilai',
    ];
		return view('admin/timpenilai', $data);
	}


	public function save()
	{
		$model = new Tim_penilai_model();

		$data = array(
			'idpendidikan' => $this->request->getPost('idpendidikan'),
			'idpangkat' => $this->request->getPost('idpangkat'),
			'idgolongan' => $this->request->getPost('idgolongan'),
			'ideselon2' => $this->request->getPost('ideselon2'),
			'nip' => $this->request->getPost('nip'),
			'nama' => $this->request->getPost('nama'),
			'tmt_pegawai' => $this->request->getPost('tmt_pegawai'),
			'tempat_lahir' => $this->request->getPost('tempat_lahir'),
			'tgl_lahir' => $this->request->getPost('tgl_lahir'),
			'jk' => $this->request->getPost('jk'),
			'alamat' => $this->request->getPost('alamat'),
			'telepon' => $this->request->getPost('telepon'),
			'email' => $this->request->getPost('email'),
			'ktp' => $this->request->getPost('ktp'),
			'idjp' => $this->request->getPost('idjp'),
		);

		$model->insert_pegawai($data);
		return redirect()->to(base_url('tim_penilai'));
	}

	public function update()
	{
		$model = new Tim_penilai_model();
		$id = $this->request->getPost('idtp');
		$data = array(
			'idpendidikan' => $this->request->getPost('idpendidikan'),
			'idpangkat' => $this->request->getPost('idpangkat'),
			'idgolongan' => $this->request->getPost('idgolongan'),
			'ideselon2' => $this->request->getPost('ideselon2'),
			'nip' => $this->request->getPost('nip'),
			'nama' => $this->request->getPost('nama'),
			'tmt_pegawai' => $this->request->getPost('tmt_pegawai'),
			'tempat_lahir' => $this->request->getPost('tempat_lahir'),
			'tgl_lahir' => $this->request->getPost('tgl_lahir'),
			'jk' => $this->request->getPost('jk'),
			'alamat' => $this->request->getPost('alamat'),
			'telepon' => $this->request->getPost('telepon'),
			'email' => $this->request->getPost('email'),
			'ktp' => $this->request->getPost('ktp'),
			'idjp' => $this->request->getPost('idjp'),
		);

		$model->update_tim_penilai($data, $id);
		return redirect()->to(base_url('tim_penilai'));

	}

	public function delete()
	{

        $model = new Tim_penilai_model();
        $id = $this->request->getPost('idtp');
        $model->delete_tim_penilai($id);
		return redirect()->to(base_url('tim_penilai'));
	}



	//--------------------------------------------------------------------

}
