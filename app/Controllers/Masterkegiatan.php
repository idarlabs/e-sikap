<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_Jenisunsur;
use App\Models\ModelUnsur;
use App\Models\ModelSubUnsur;
use App\Models\ModelMKegiatan;
use App\Models\ModelKegiatan;
use App\Models\ModelJabatan;
use App\Models\ModelJenisPemeriksaKerja;

class Masterkegiatan extends Controller {

  public function index(){
    $model = new M_Jenisunsur();
    $model1 = new ModelJenisPemeriksaKerja();
    $model2 = new ModelJabatan();
    $modelkegiatan = new ModelKegiatan();
    $jenis_unsur = $model->data()->getResult();
    $m_jpk = $model1->data()->getResult();
    $m_jabatan = $model2->data()->getResult();
    $mk = $modelkegiatan->data2()->getResult();
    $data = [
      'h1' => 'Form Master Kegiatan',
      'bread' => 'Tambah Master Kegiatan',
      'jenis_unsur' => $jenis_unsur,
      'm_jpk' => $m_jpk,
      'm_jabatan' => $m_jabatan,
      'mk' => $mk,
    ];
    return view('kegiatan/masterkegiatan', $data);
  }

  public function jpk(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelJenisPemeriksaKerja();
      $id = $this->request->getPost('id');
      $m_unsur = $model->dataid($id)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function unsur(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelUnsur();
      $id = $this->request->getPost('id');
      $idbidang = $this->request->getPost('idbidang');
      $m_unsur = $model->dataid($id, $idbidang)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function subunsur(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelSubUnsur();
      $id = $this->request->getPost('id');
      $idbidang = $this->request->getPost('idbidang');
      $m_unsur = $model->dataid3($id, $idbidang)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function mkegiatan(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelMKegiatan();
      $id = $this->request->getPost('id');
      $m_unsur = $model->dataid($id)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function simpan(){
    $model = new ModelKegiatan();
		$data = [
      'idjenispemeriksakerja' => $this->request->getPost('jpk'),
      'idjenisunsur' => $this->request->getPost('jenis_unsur'),
      'idunsur' => $this->request->getPost('unsur'),
      'idsubunsur' => $this->request->getPost('subunsur'),
      'idmasterkegiatan' => $this->request->getPost('mkegiatan'),
      'butirkegiatan' => $this->request->getPost('butirkegiatan'),
      'idjabatan' => $this->request->getPost('jabatan'),
      'kredit' => $this->request->getPost('kredit')
		];
		$model->simpan($data);
		return "true";
  }

  public function hapus(){
    if($this->request->getPost('validasi') == "go"){
      $model = new ModelKegiatan();
      $idkegiatan = $this->request->getPost('idkegiatan');
      $model->hapus($idkegiatan);
    }
  }
}
