<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Periode_model;
use App\Models\Periode as PR;
use App\Models\Buka_menu;

class Periode extends BaseController
{

	protected $Periode_model;
	protected $bukamenu;
	
	public function __construct() {

        $this->periode = new Periode_model();
        $this->bukamenu = new Buka_menu();
    }

	public function index()
	{
		$data['bukamenu']= $this->bukamenu->jPeriode2();
		$data['periode'] = $this->periode->getPeriode();
		$data['page']	 = 'page/periode';
		echo view('web', $data);
	}

	public function tutup(){
		if($this->request->isAjax()){
			$this->response->setContentType('application/json; charset=utf-8');
			$json_response=array();
			$dataInput = json_decode(file_get_contents('php://input'),true);
			if ($dataInput['submit']) {
				$dbBukaMenu = new \App\Models\Buka_menu();
				$dbRiwayatPengajuan = new \App\Models\Riwayat_pengajuan();
				$dbPengajuan = new \App\Models\Pengajuan();
				$idPeriode = $dbBukaMenu->jPeriode()
								->idperiode;
				$dbRiwayatPengajuan->transStart();
				$dbRiwayatPengajuan->whereNotIn('idstep',['3','4','5'])
					->set(['idstep' => '4'])
					->update();
				$dbBukaMenu->delete('1');
				if ($dbRiwayatPengajuan->transStatus() === TRUE) {
					$dbRiwayatPengajuan->transCommit();
					$json_response['code'] = '1';
					$json_response['message']['text'] = 'berhasil';
				} else {
					$dbRiwayatPengajuan->transRollback();
					$json_response['code'] = '0';
					$json_response['message']['text'] = 'gagal';
				}
			}
			return $this->response->setJSON($json_response);
		}
	}
	
	public function add()
  	{
		//Proteksi
		helper('text');
		$data['kd'] = random_string('alnum',6);
    	$data['page'] = 'page/periode_add';
    	return view('web', $data);
  	}

	public function save(){
    	$idperiode = $this->request->getPost('idperiode');
    	$kode = $this->request->getPost('kode');
    	$tgl_buka = $this->request->getPost('tgl_buka');
    	$tgl_tutup = $this->request->getPost('tgl_tutup');

    	$data = [
      		'kode' => $kode,
      		'tgl_buka' => $tgl_buka,
      		'tgl_tutup' => $tgl_tutup
    	];
		$dbPeriode = new \App\Models\Periode();
		$dbBukaMenu = new \App\Models\Buka_menu();
		$cek = $dbBukaMenu->data()->getResultArray();
		if(count($cek) == 0){
			$dbPeriode->transStart();
			$dbPeriode->insert($data);
			$idPeriode = $dbPeriode->getInsertID();
			$dataInsertBukaMenu = [
				'idbuka_menu' => '1',
				'tgl_admin_buka' => date('Y-m-d'),
				'idperiode' => $idPeriode,
				'status' => '1'
			];
			$dbBukaMenu->simpan($dataInsertBukaMenu);

			if($dbPeriode->transStatus === FALSE){
				$dbPeriode->transRollback();
			} else {
				$dbPeriode->transCommit();
				return redirect()->to(base_url('periode')); 
			}
		} else {
			echo "<script>
					alert('tidak boleh');
					window.location = '" . base_url("periode/add") . "'
				</script>";
		}
    // $simpan = $this->periode->insert_periode($data);

    // if($simpan)
    // {
    //   //session()->setFlashdata('success', 'Created product successfully');
    // }
  }

    public function edit($id)
  	{
		//Proteksi

    	$data['periode'] = $this->periode->getPeriode($id);
    	$data['page'] = 'page/periode_edit';
    	return view('web', $data);
  	}

	public function update()
	{

    	$idperiode = $this->request->getPost('idperiode');
    	$kode = $this->request->getPost('kode');
    	$tgl_buka = date('Y/m/d');
    	$tgl_tutup = date('Y/m/d');

    	$data = [
      		'idperiode' => $idperiode,
      		'kode' => $kode,
      		'tgl_buka' => $tgl_buka,
      		'tgl_tutup' => $tgl_tutup
    	];

	$ubah = $this->periode->update_periode($data, $idperiode);
	
	if($ubah)
	{
		return redirect()->to(base_url('periode'));
	}

}

	public function delete($id)
	{

		$hapus = $this->periode->delete_periode($id);

		if($hapus)
		{
			return redirect()->to(base_url('periode'));
		}
	}


	//--------------------------------------------------------------------



}
