<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_golongan_model;
use App\Models\M_jabatan_model;

class M_golongan extends BaseController
{

	protected $M_golongan_model;
	
	public function __construct() {

        $this->m_golongan = new M_golongan_model();
    }

	public function index()
	{

		$data['m_golongan'] = $this->m_golongan->getM_golongan();
		$data['page']	 = 'page/m_golongan';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$this->m_jabatan = new M_jabatan_model();
    	$data['m_jabatan'] = $this->m_jabatan->getM_jabatan();		
    	$data['page'] = 'page/m_golongan_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$idjabatan = $this->request->getPost('idjabatan');
    	$golongan = $this->request->getPost('golongan');

    	$data = [
      		'idjabatan' => $idjabatan,
      		'golongan' => $golongan
    	];

    $simpan = $this->m_golongan->insert_m_golongan($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_golongan')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->m_jabatan = new M_jabatan_model();
    	$data['m_jabatan'] = $this->m_jabatan->getM_jabatan();
    	$data['m_golongan'] = $this->m_golongan->getM_golongan($id);
    	$data['page'] = 'page/m_golongan_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idgolongan = $this->request->getPost('idgolongan');
		$golongan = $this->request->getPost('golongan');
    	$idjabatan = $this->request->getPost('idjabatan');		

		$data = [
			'idgolongan' => $idgolongan,
			'golongan' => $golongan,
			'idjabatan' => $idjabatan
		];

	$ubah = $this->m_golongan->update_m_golongan($data, $idgolongan);
	
	if($ubah)
	{
		return redirect()->to(base_url('m_golongan'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_golongan->delete_m_golongan($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_golongan'));
		}
	}


	//--------------------------------------------------------------------

}
