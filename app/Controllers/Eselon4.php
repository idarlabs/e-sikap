<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Eselon1_model;
use App\Models\Eselon2_model;
use App\Models\Eselon3_model;
use App\Models\Eselon4_model;

class Eselon4 extends BaseController
{

	protected $Eselon4_model;
	
	public function __construct() {

        $this->eselon4 = new Eselon4_model();
    }

	public function index()
	{

		$data['eselon4'] = $this->eselon4->getEselon4();
		$data['page']	 = 'page/eselon4';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$this->eselon2 = new Eselon2_model();
    	$data['eselon2'] = $this->eselon2->getEselon2();
    	$this->eselon3 = new Eselon3_model();
    	$data['eselon3'] = $this->eselon3->getEselon3();    	
    	$data['page'] = 'page/eselon4_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$ideselon1 = $this->request->getPost('ideselon1');
		$ideselon2 = $this->request->getPost('ideselon2');
		$ideselon3 = $this->request->getPost('ideselon3');		
    	$n_unitsatuankerja4 = $this->request->getPost('n_unitsatuankerja4');

    	$data = [
      		'ideselon1' => $ideselon1,
      		'ideselon2' => $ideselon2,
      		'ideselon3' => $ideselon3,      		      		
      		'n_unitsatuankerja4' => $n_unitsatuankerja4
    	];

    $simpan = $this->eselon4->insert_eselon4($data);

    if($simpan)
    {

      return redirect()->to(base_url('eselon4')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$this->eselon2 = new Eselon2_model();
    	$data['eselon2'] = $this->eselon2->getEselon2();
    	$this->eselon3 = new Eselon3_model();
    	$data['eselon3'] = $this->eselon3->getEselon3();    	    	
    	$data['eselon4'] = $this->eselon4->getEselon4($id);
    	$data['page'] = 'page/eselon4_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$ideselon1 = $this->request->getPost('ideselon1');
		$ideselon2 = $this->request->getPost('ideselon2');		
		$ideselon3 = $this->request->getPost('ideselon3');
		$ideselon4 = $this->request->getPost('ideselon4');		
		$n_unitsatuankerja4 = $this->request->getPost('n_unitsatuankerja4');

		$data = [
			'ideselon1' => $ideselon1,
			'ideselon2' => $ideselon2,
			'ideselon3' => $ideselon3,						
			'n_unitsatuankerja4' => $n_unitsatuankerja4
		];

	$ubah = $this->eselon4->update_eselon4($data, $ideselon4);
	
	if($ubah)
	{
		return redirect()->to(base_url('eselon4'));
	}

}

	public function delete($id)
	{

		$hapus = $this->eselon4->delete_eselon4($id);

		if($hapus)
		{
			return redirect()->to(base_url('eselon4'));
		}
	}


	//--------------------------------------------------------------------

}
