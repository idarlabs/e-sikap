<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_Jenisunsur;
use App\Models\ModelUnsur;
use App\Models\ModelSubUnsur;
use App\Models\ModelMKegiatan;
use App\Models\ModelKegiatan;
use phpDocumentor\Reflection\Types\Null_;

class Bidang extends BaseController {

  public function index(){
    return redirect()->to(base_url('home'));
  }

  public function addDesainIndustri(){
    $uploadedDocumentPath = __DIR__ .DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "doc";
    $webURIImage = "doc";
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($this->request->getPost('submit')) {
        $file = $this->request->getFile('fileUpload');
        $rules = [
          'fileUpload' => [
              'label' => 'Dokumen Upload',
              'rules' => 'uploaded[fileUpload]',
              'errors' => [
                'uploaded' => 'Anda harus menyematkan dokumen untuk diunggah (err. {field} | {value} {param} | uploaded)',
              ],
          ],
        ];
        if ($this->validate($rules)) {
          $dbBukaMenu = new \App\Models\Buka_menu();
          $dbPengajuan = new \App\Models\Pengajuan();
          $dbKegiatan = new \App\Models\Kegiatan();
          $dbRiwayatPengajuan = new \App\Models\Riwayat_pengajuan();
          $dbRiwayatDokumen = new \App\Models\Riwayat_dokumen();
          $dbLogPT = new \App\Models\Log_pengajuan_terakhir();
          $selectBukaMenu = $dbBukaMenu->find('1');
          $idPeriode = $selectBukaMenu->idperiode;
          $selectLog = $dbLogPT->where('idperiode',$idPeriode)
            ->where('nip',session()->get('nip'))
            ->first();
          if ($selectLog) {
            $json_response['message']['text'] = 'Anda sudah melakukan permohonan';
          } else {
            if ($file) {
              $fileName = $file->getRandomName();
              $file->move($uploadedDocumentPath, $webURIImage . $fileName);
            }
            $selectKegiatan = $dbKegiatan->find($this->request->getPost('idkegiatan'));
            $tanggalSubmit = date('Y-m-d');
            $jamSubmit = date('h:i:s');
            $dbPengajuan->transStart();
            if ($selectBukaMenu) {
              $selectPengajuan = $dbPengajuan->where('nip',session()->get('nip'))
                                    ->where('idperiode',$selectBukaMenu->idperiode)
                                    ->first();
              if ($selectPengajuan) {
                $idPengajuan = $selectPengajuan->idpengajuan;
                $selectRiwayatPengajuan = $dbRiwayatPengajuan->where('nip',session()->get('nip'))
                                            ->where('idkegiatan',$this->request->getPost('idkegiatan'))
                                            ->where('idpengajuan',$idPengajuan)
                                            ->first();
                //cek kalo Riwayat pengajuan sudah
                $idRiwayatPengajuan = '';
                if ($selectRiwayatPengajuan) {
                  $idRiwayatPengajuan = $selectRiwayatPengajuan->idriwayatpengajuan;
                  $dataUpdateRiwayatPengajuan = [
                    'idriwayatpengajuan' => $idRiwayatPengajuan,
                    'tgl_submit' => $tanggalSubmit,
                    'jam_submit' => $jamSubmit,
                  ];
                  $dbRiwayatPengajuan->save($dataUpdateRiwayatPengajuan);

                  // sekalian update dokumen
                  $id = $idRiwayatPengajuan;
                  $data = array(
                    'path' => $webURIImage . $fileName,
                    'tgl_upload' => $tanggalSubmit,
                    'jam_upload' => $jamSubmit
                  );

                  $dbRiwayatDokumen->updatedata($data, $id);

                } else {
                  $dataInsertRiwayatPengajuan = [
                    'idpengajuan' => $idPengajuan,
                    'tgl_submit' => $tanggalSubmit,
                    'jam_submit' => $jamSubmit,
                    'kredit' => $selectKegiatan->kredit,
                    'idkegiatan' => $this->request->getPost('idkegiatan'),
                    'idstep' => '1',
                    'nip' => session()->get('nip'),
                  ];
                  $dbRiwayatPengajuan->insert($dataInsertRiwayatPengajuan);
                  $idRiwayatPengajuan = $dbRiwayatPengajuan->getInsertID();

                  $dataInsertRiwayatDokumen = [
                    'path' => $webURIImage . $fileName,
                    'idriwayatpengajuan' => $idRiwayatPengajuan,
                    'tgl_upload' => $tanggalSubmit,
                    'jam_upload' => $jamSubmit
                  ];
                  // $dbRiwayatDokumen->insert($dataInsertRiwayatDokumen);
                  $dbRiwayatDokumen->simpan($dataInsertRiwayatDokumen);
                }
              } else {
                $selectBukaMenu = $dbBukaMenu->find('1');
                $idPeriode = $selectBukaMenu->idperiode;
                // //insert Pengajuan
                // $dataInsertPengajuan = [
                //   'nip' => session()->get('nip'),
                //   'idperiode' => $idPeriode,
                //   'ideselon' => '0',
                //   'status' => '1',
                // ];
                // $dbPengajuan->insert($dataInsertPengajuan);
                // $idPengajuan = $dbPengajuan->getInsertID();

                //insert Riwayat_pengajuan
                $dataInsertRiwayatPengajuan = [
                  'idpengajuan' => $idPengajuan,
                  'tgl_submit' => $tanggalSubmit,
                  'jam_submit' => $jamSubmit,
                  'kredit' => $selectKegiatan->kredit,
                  'idkegiatan' => $this->request->getPost('idkegiatan'),
                  'idstep' => '1',
                  'nip' => session()->get('nip'),
                ];
                $dbRiwayatPengajuan->insert($dataInsertRiwayatPengajuan);
                $idRiwayatPengajuan = $dbRiwayatPengajuan->getInsertID();
                //insert Riwayat_dokumen
                $dataInsertRiwayatDokumen = [
                  'path' => 'doc' . $fileName,
                  'idriwayatpengajuan' => $idRiwayatPengajuan,
                  'tgl_upload' => $tanggalSubmit,
                  'jam_upload' => $jamSubmit
                ];
                // $dbRiwayatDokumen->insert($dataInsertRiwayatDokumen);
                $dbRiwayatDokumen->simpan($dataInsertRiwayatDokumen);
              }
              if ($dbPengajuan->transStatus === FALSE) {
                $dbPengajuan->transRollback();
              } else {
                $dbPengajuan->transCommit();
                $json_response['code'] = '1';
                $json_response['message']['text'] = 'berhasil';
              }
            } else {
              $json_response['code'] = '0';
              $json_response['message']['text'] = 'Tidak ada periode yang aktif';
            }
          }
        } else {
            $json_response['code'] = '0';
            $json_response['message']['text'] = $this->validator->listErrors();
        }
      }
      return $this->response->setJSON($json_response);
    }
  }

  public function tambahAwal(){
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($dataInput['submit']) {
        $dbPengajuan = new \App\Models\Pengajuan();
        $eselon = '0';
        $golku = session()->get('golru');
        if($golku == "IV/d" || $golku == "IV/c" || $golku == "IV/b"){
          $eselon = '1';
        }else if($golku == "IV/a" || $golku == "III/d" || $golku == "III/c" || $golku == "III/b" || $golku == "III/a"){
          $eselon = '2';
        }
        $dataInsertPengajuan = [
          'nip' => session()->get('nip'),
          'idperiode' => $dataInput['idperiode'],
          'ideselon' => $eselon,
          'status' => '1',
          'idbidang' => session()->get('idbidang'),
          'idpenilai' => "kosong",
        ];
        $dbPengajuan->insert($dataInsertPengajuan);
        $idPengajuan = $dbPengajuan->getInsertID();
        $json_response['code'] = '1';
        $json_response['result']['data'] = $idPengajuan;
        $json_response['message']['text'] = 'berhasil';
      }
      return $this->response->setJSON($json_response);
    }

  }

  public function desainindustri(){
    $dbBukaMenu = new \App\Models\Buka_menu();
    $dbPengajuan = new \App\Models\Pengajuan();
    $kegiatanditolak = new \App\Models\PengajuanDitolak();
    $selectBukaMenu = $dbBukaMenu->find('1');
    $idPeriode = $selectBukaMenu == NULL ? "0" : $selectBukaMenu->idperiode;
    $nip = session()->get('nip');
    $selectPengajuan = $dbPengajuan->where('idperiode',$idPeriode)
                          ->where('nip',$nip)
                          ->first();
    $draft = NULL;
    $usulan = NULL;
    $proses = NULL;
    $ditolakx = NULL;

    $dblog = new \App\Models\ModelLogTerakhir();
    $sudahmengajukan = false;
    if(count($dblog->cek($nip, $idPeriode)->getResultArray()) > 0){
      $sudahmengajukan = true;
    }

    if($selectPengajuan != NULL){
      $draft = $dbPengajuan->draft($selectPengajuan->idpengajuan, '1')->getResult();
      $usulan = $dbPengajuan->draft($selectPengajuan->idpengajuan, '2')->getResult();
      $proses = $dbPengajuan->draft($selectPengajuan->idpengajuan, '3')->getResult();
      $ditolakx = $kegiatanditolak->draft($selectPengajuan->idpengajuan, '4')->getResult();
    }
    // return var_dump($ditolakx);die;

    $isipengajuan = $selectPengajuan  == NULL ? "0" : "1";
    $model = new M_Jenisunsur();
    $jenis_unsur = $model->data()->getResult();

    $data = [
      'h1' => 'Bidang Desain Industri',
      'bread' => 'Desain Industri',
      'jenis_unsur' => $jenis_unsur,
      'isipengajuan' => $isipengajuan,
      'idperiode' => $idPeriode,
      'draft' => $draft,
      'usulan' => $usulan,
      'proses' => $proses,
      'ditolakx' => $ditolakx,
      'sudahmengajukan' => $sudahmengajukan,
    ];
    $data['dataTerima'] = $dbPengajuan->jPeriode(session()->get('nip'),'3');
    $data['dataTolak'] = $dbPengajuan->jPeriode(session()->get('nip'),'2');
    return view("bidang/desainindustri", $data);
  }

  public function paten(){
    $dbBukaMenu = new \App\Models\Buka_menu();
    $dbPengajuan = new \App\Models\Pengajuan();
    $kegiatanditolak = new \App\Models\PengajuanDitolak();
    $selectBukaMenu = $dbBukaMenu->find('1');
    $idPeriode = $selectBukaMenu == NULL ? "0" : $selectBukaMenu->idperiode;
    $nip = session()->get('nip');
    $selectPengajuan = $dbPengajuan->where('idperiode',$idPeriode)
                          ->where('nip',$nip)
                          ->first();
    $draft = NULL;
    $usulan = NULL;
    $proses = NULL;
    $ditolakx = NULL;

    $dblog = new \App\Models\ModelLogTerakhir();
    $sudahmengajukan = false;
    if(count($dblog->cek($nip, $idPeriode)->getResultArray()) > 0){
      $sudahmengajukan = true;
    }

    if($selectPengajuan != NULL){
      $draft = $dbPengajuan->draft($selectPengajuan->idpengajuan, '1')->getResult();
      $usulan = $dbPengajuan->draft($selectPengajuan->idpengajuan, '2')->getResult();
      $proses = $dbPengajuan->draft($selectPengajuan->idpengajuan, '3')->getResult();
      $ditolakx = $kegiatanditolak->draft($selectPengajuan->idpengajuan, '4')->getResult();
    }

    $isipengajuan = $selectPengajuan  == NULL ? "0" : "1";
    $model = new M_Jenisunsur();
    $jenis_unsur = $model->data()->getResult();

    $data = [
      'h1' => 'Bidang Paten',
      'bread' => 'Paten',
      'jenis_unsur' => $jenis_unsur,
      'isipengajuan' => $isipengajuan,
      'idperiode' => $idPeriode,
      'draft' => $draft,
      'usulan' => $usulan,
      'proses' => $proses,
      'ditolakx' => $ditolakx,
      'sudahmengajukan' => $sudahmengajukan,
    ];
    $data['dataTerima'] = $dbPengajuan->jPeriode(session()->get('nip'),'3');
    $data['dataTolak'] = $dbPengajuan->jPeriode(session()->get('nip'),'2');
    return view("bidang/paten", $data);
  }

  public function merek(){
    $dbBukaMenu = new \App\Models\Buka_menu();
    $dbPengajuan = new \App\Models\Pengajuan();
    $kegiatanditolak = new \App\Models\PengajuanDitolak();
    $selectBukaMenu = $dbBukaMenu->find('1');
    $idPeriode = $selectBukaMenu == NULL ? "0" : $selectBukaMenu->idperiode;
    $nip = session()->get('nip');
    $selectPengajuan = $dbPengajuan->where('idperiode',$idPeriode)
                          ->where('nip',$nip)
                          ->first();
    $draft = NULL;
    $usulan = NULL;
    $proses = NULL;
    $ditolakx = NULL;

    $dblog = new \App\Models\ModelLogTerakhir();
    $sudahmengajukan = false;
    if(count($dblog->cek($nip, $idPeriode)->getResultArray()) > 0){
      $sudahmengajukan = true;
    }

    if($selectPengajuan != NULL){
      $draft = $dbPengajuan->draft($selectPengajuan->idpengajuan, '1')->getResult();
      $usulan = $dbPengajuan->draft($selectPengajuan->idpengajuan, '2')->getResult();
      $proses = $dbPengajuan->draft($selectPengajuan->idpengajuan, '3')->getResult();
      $ditolakx = $kegiatanditolak->draft($selectPengajuan->idpengajuan, '4')->getResult();
    }

    $isipengajuan = $selectPengajuan  == NULL ? "0" : "1";
    $model = new M_Jenisunsur();
    $jenis_unsur = $model->data()->getResult();

    $data = [
      'h1' => 'Bidang Merek',
      'bread' => 'Merek',
      'jenis_unsur' => $jenis_unsur,
      'isipengajuan' => $isipengajuan,
      'idperiode' => $idPeriode,
      'draft' => $draft,
      'usulan' => $usulan,
      'proses' => $proses,
      'ditolakx' => $ditolakx,
      'sudahmengajukan' => $sudahmengajukan,
    ];
    $data['dataTerima'] = $dbPengajuan->jPeriode(session()->get('nip'),'3');
    $data['dataTolak'] = $dbPengajuan->jPeriode(session()->get('nip'),'2');
    return view("bidang/merek", $data);
  }

  public function usulkandesainindustri(){
    if($this->request->getPost('validasi') === "go"){
      $model = new \App\Models\Riwayat_pengajuan();
      $id = $this->request->getPost('idriwayatpengajuan');
      $model->updatedata(['idstep' => '2'], $id);
    }
    return true;
  }

  public function kegiatan($idunsur = ""){
    if($idunsur === ""){
      return redirect()->to(base_url('home'));
    }else{
      $model = new ModelUnsur();
      $unsur = $model->dataid($idunsur,session()->get('idbidang'))->getResult();
      $data = [
        'h1' => 'Form Kegiatan',
        'bread' => 'Form Kegiatan',
        'jenu' => $idunsur,
        'unsur' => $unsur,
        'kode_jpk' => session()->get('idbidang'),
      ];
      return view("bidang/kegiatan", $data);
    }
  }

  public function subunsur(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelSubUnsur();
      $id = $this->request->getPost('id');
      $m_unsur = $model->dataid($id)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function mkegiatan(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelMKegiatan();
      $id = $this->request->getPost('id');
      $m_unsur = $model->dataid($id)->getResultArray();
      return json_encode($m_unsur);
    }
    return false;
  }

  public function butirkegiatan(){
    if($this->request->getPost('validasi') === "go"){
      $model = new ModelKegiatan();
      $idjenispemeriksakerja = $this->request->getPost('idjenispemeriksakerja');
      $idjenisunsur = $this->request->getPost('idjenisunsur');
      $idunsur = $this->request->getPost('idunsur');
      $idsubunsur = $this->request->getPost('idsubunsur');
      $idmasterkegiatan = $this->request->getPost('idmasterkegiatan');
      $kegiatan = $model->getdata($idjenispemeriksakerja, $idjenisunsur, $idunsur, $idsubunsur, $idmasterkegiatan)->getResultArray();
      return json_encode($kegiatan);
    }
    return false;
  }

  public function hapusKegiatan(){
    if($this->request->isAjax()){
      $this->response->setContentType('application/json; charset=utf-8');
      $json_response=array();
      $dataInput = json_decode(file_get_contents('php://input'),true);
      if ($dataInput['submit']) {
          $dbRiwayatPengajuan = new \App\Models\Riwayat_pengajuan();
          $dbRiwayatDokumen = new \App\Models\Riwayat_dokumen();
          $dbRiwayatDokumen->where('idriwayatpengajuan',$dataInput['idriwayatpengajuan'])
              ->delete();
          $dbRiwayatPengajuan->where('idriwayatpengajuan',$dataInput['idriwayatpengajuan'])
              ->delete();
          $json_response['code'] = '1';
          $json_response['message']['text'] = 'berhasil';
      }
      return $this->response->setJSON($json_response);
    }
  }

  public function detailnilai($id=null,$idperiode=null){
    if ($id == null && $idperiode == null) {
      return redirect()->to("/home");
    } else {
      $modeltotalkredit = new \App\Models\ModelTotalKredit();
      $model = new \App\Models\Riwayat_pengajuan();
      $riwayatbaru = new \App\Models\ModelRiwayatBaru();
      $riwayatlama = new \App\Models\ModelRiwayatLama();
      $dbPengajuan = new \App\Models\Pengajuan();

      $pengajuanbaru = $riwayatbaru->datalast(session()->get('nip'))->getResultArray();
      $pengajuanlama = $riwayatlama->datalast(session()->get('nip'))->getResultArray();

      $pengajuan = $model->data($id)->getResultArray();
      $modelpengajuan = new \App\Models\Pengajuan();
      $datapengaju = $modelpengajuan->datapengajuan($id)->getResult()[0];

      $pengajuansebelumnya = $this->db->table('log_pengajuan_terakhir')
      ->where(['nip' => $datapengaju->nip])
      ->orderBy('idlog','DESC')
      ->limit(1)
      ->get()
      ->getResult();

      $pendidikan_1 = 0;
      $pendidikan_2 = 0;
      $pendidikan_3 = 0;
      $pemeriksaan = 0;
      $pengembangan = 0;
      $penunjang = 0; //penunjang kalo di form
      $jmlunsurutama = 0;
      $jmlkeduanya = 0;

      $pen1 = 0;
      $pen2 = 0;
      $pen3 = 0;
      $pem = 0;
      $peng = 0;
      $pen = 0;
      $jmlunsurutama2 = 0;
      $jmlkeduanya2 = 0;

      $pen1x = 0;
      $pen2x = 0;
      $pen3x = 0;
      $pemx = 0;
      $pengx = 0;
      $penx = 0;

      $jmlunsurutama2x = 0;
      $jmlkeduanya2x = 0;

      $dataku = $modeltotalkredit->dataku(session()->get('nip'), $id)->getResultArray();

      if(count($dataku) === 1){
        foreach($dataku as $d){
          $pendidikan_1 = $d['pendidikan_1'];
          $pendidikan_2 = $d['pendidikan_2'];
          $pendidikan_3 = $d['pendidikan_3'];
          $pemeriksaan = $d['pemeriksaan'];
          $pengembangan = $d['pengembangan'];
          $penunjang = $d['penunjang'];
          $pen1 = $d['pen1'];
          $pen2 = $d['pen2'];
          $pen3 = $d['pen3'];
          $pem = $d['pem'];
          $peng = $d['peng'];
          $pen = $d['pen'];
        }
      }

      $jmlunsurutama = $pendidikan_1 + $pendidikan_2 + $pendidikan_3 + $pemeriksaan + $pengembangan;
      $jmlkeduanya = $jmlunsurutama + $penunjang;

      //tambah dengan nilai kredit awal
      $modelprecutoff = new \App\Models\Precutoff();
      $nilaiawal = $modelprecutoff->nilaiku(session()->get('nip'))->getResult();
      if($nilaiawal !== NULL){
        foreach($nilaiawal as $na){
          $pen1x = $na->pendidikan_1;
          $pen2x = $na->pendidikan_2;
          $pen3x = $na->pendidikan_3;
          $pemx = $na->pemeriksaan;
          $pengx = $na->pengembangan;
          $penx = $na->penunjang;
        }
      }
      $pen1 += $pen1x; $pen2 += $pen2x; $pen3 += $pen3x; $pem += $pemx; $peng += $pengx; $pen += $penx;
      $jmlunsurutama2 = $pen1 + $pen2 + $pen3 + $pem + $peng;
      $jmlkeduanya2 = $jmlunsurutama2 + $pen;

      /* =================================================================================== */

      $modelpengajuan = new \App\Models\ModelPengangkatan();
      $pengangkatan = $modelpengajuan->dataid($datapengaju->idgolongan)->getResult();

				$data =  [
					'bread' => 'Detail Pengajuan',
					'h1' => 'Detail Pengajuan',
					'pendidikan_1' => $pendidikan_1,
					'pendidikan_2' => $pendidikan_2,
					'pendidikan_3' => $pendidikan_3,
					'pengembangan' => $pengembangan,
					'pemeriksaan' => $pemeriksaan,
					'penunjang' => $penunjang,
					'jmlunsurutama' => $jmlunsurutama,
					'jmlkeduanya' => $jmlkeduanya,

					'pen1' => $pen1,
					'pen2' => $pen2,
					'pen3' => $pen3,
					'peng' => $peng,
					'pem' => $pem,
					'pen' => $pen,
					'jmlunsurutama2' => $jmlunsurutama2,
					'jmlkeduanya2' => $jmlkeduanya2,

					'datapengaju' => $datapengaju,
					'pengangkatan' => $pengangkatan,
					'idpeng' => $id,
					'idper' => $idperiode,
				];
      return view("bidang/detailnilai",$data);
    }
  }

  // public function pdf($id = null, $nip = null, $idperiode = null){
	// 	if($id !== null && $nip !== null && $idperiode !== null){
  //     $modelpegawai = new \App\Models\ModelPegawai();
  //     $modeltotalkredit = new \App\Models\ModelTotalKredit();
  //     $modelperiode = new \App\Models\Periode_model();
  //
  //     $data_akhir = $modeltotalkredit->dataku($nip, $id)->getResult()[0];
  //     $dp = $modelpegawai->datalengkap($nip)->getResult()[0];
  //     $dpe = $modelpegawai->datalengkap($data_akhir->nip_penilai)->getResult()[0];
  //     $periode = $modelperiode->dataperiode($idperiode)[0];
  //     $index_bulan_awal = (substr($periode['tgl_buka'], 5, 2) * 1) -1;
  //     $index_bulan_akir = (substr($periode['tgl_tutup'], 5, 2) * 1) -1;
  //
  //     $total_pendidikan1 = $data_akhir->pendidikan_1;
  //     $total_pendidikan2 = $data_akhir->pendidikan_2;
  //     $total_pendidikan3 = $data_akhir->pendidikan_3;
  //     $total_pemeriksaan = $data_akhir->pemeriksaan;
  //     $total_pengembangan = $data_akhir->pengembangan;
  //
  //     $total_pen1 = $data_akhir->pen1;
  //     $total_pen2 = $data_akhir->pen2;
  //     $total_pen3 = $data_akhir->pen3;
  //     $total_pem = $data_akhir->pem;
  //     $total_peng = $data_akhir->peng;
  //
  //     $total_baru_utama = $total_pendidikan1 + $total_pendidikan2 + $total_pendidikan3 + $total_pemeriksaan + $total_pengembangan;
  //     $total_lama_utama = $total_pen1 + $total_pen2 + $total_pen3 + $total_pem + $total_peng;
  //     $total_utama = $total_baru_utama + $total_lama_utama;
  //
	// 		$mpdf = new \Mpdf\Mpdf();
	// 		$mpdf->SetHeader('Menteri Hukum Dan Hak Asasi Manusia||Direktorat Jenderal Kekayaan Intelektual');
	// 		$mpdf->WriteHTML('
  //     <style>.tengah{text-align:center;}</style>
	// 		<h3 align="center">Penetapan Angka Kredit</h3>
	// 		<h4 align="center">Masa Penilaian: '.$this->bln_indo($index_bulan_awal).' s/d '.$this->bln_indo($index_bulan_akir, $periode['tgl_tutup']).'</h4>
	// 		<table border="1" id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse;">
	// 		<tr>
	// 			<th colspan="5" align="left">I. KETERANGAN PERORANGAN</th>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">1</td>
	// 		<td colspan="1" align="left">N A M A</td>
	// 		<td colspan="3">'.$dp->nama.'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">2</td>
	// 		<td colspan="1" align="left">N I P</td>
	// 		<td colspan="3">'.$dp->nip.'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">3</td>
	// 		<td colspan="1" align="left">Nomor Seri Kartu Pegawai</td>
	// 		<td colspan="3">'.$dp->no_karpeg.'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">4</td>
	// 		<td colspan="1" align="left">Pangkat & Golongan Ruang / Terhitung Mulai Tanggal</td>
	// 		<td colspan="3">'.$dp->pangkat.' ('.$dp->golongan.') / '.$this->tgl_indo($dp->tmt_pegawai).'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">5</td>
	// 		<td colspan="1" align="left">Tempat dan Tanggal Lahir</td>
	// 		<td colspan="3">'.$dp->tempat_lahir.', '.$this->tgl_indo($dp->tgl_lahir).'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">6</td>
	// 		<td colspan="1" align="left">Jenis Kelamin</td>
	// 		<td colspan="3">'.$dp->jekel.'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">7</td>
	// 		<td colspan="1" align="left">Pendidikan Tertinggi</td>
	// 		<td colspan="3">'.$dp->pendidikan.'</td>
	// 		</tr>
	// 		<tr>
	// 		<td class="tengah">8</td>
	// 		<td colspan="1" align="left">Jabatan Pemeriksa Merek / Terhitung Mulai Tanggal</td>
	// 		<td colspan="3">Pemeriksa '.$dp->jenispemeriksa.' '.$dpe->jabatan.' / '.$this->tgl_indo($dpe->tmt_pegawai).'</td>
	// 		</tr>
	// 		<tr>
	// 			<th colspan="2" align="left">II. PENETAPAN ANGKA KREDIT</th>
	// 			<th class="tengah">LAMA</th>
	// 			<th class="tengah">BARU</th>
	// 			<th class="tengah">JUMLAH</th>
	// 			</tr>
	// 			<tr>
	// 			<td class="tengah">1</td>
	// 			<th colspan="4" align="left">UNSUR UTAMA</th>
	// 			</tr>
				// <tr>
				// <td></td>
				// <td>a. (1) Pendidikan Formal dan mencapai gelar / ijasah</td>
        // <td class="tengah">'.$data_akhir->pen1.'</td>
				// <td class="tengah">'.$data_akhir->pendidikan_1.'</td>
				// <td class="tengah">'.($data_akhir->pendidikan_1 + $data_akhir->pen1).'</td>
				// </tr>
				// <tr>
				// <td></td>
				// <td>&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat surat Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
        // <td class="tengah">'.$data_akhir->pen2.'</td>
        // <td class="tengah">'.$data_akhir->pendidikan_2.'</td>
				// <td class="tengah">'.($data_akhir->pendidikan_2 + $data_akhir->pen2).'</td>
				// </tr>
				// <tr>
				// <td></td>
				// <td>&nbsp;&nbsp;&nbsp;&nbsp;(3) Diklat Prajabatan</td>
        // <td class="tengah">'.$data_akhir->pen3.'</td>
        // <td class="tengah">'.$data_akhir->pendidikan_3.'</td>
				// <td class="tengah">'.($data_akhir->pendidikan_3 + $data_akhir->pen3).'</td>
				// </tr>
        // <tr>
				// <td></td>
				// <td>b. Pemeriksaan</td>
        // <td class="tengah">'.$data_akhir->pem.'</td>
        // <td class="tengah">'.$data_akhir->pemeriksaan.'</td>
				// <td class="tengah">'.($data_akhir->pemeriksaan + $data_akhir->pem).'</td>
				// </tr>
        // <tr>
				// <td></td>
				// <td>c. Pengembangan Profesi</td>
        // <td class="tengah">'.$data_akhir->peng.'</td>
        // <td class="tengah">'.$data_akhir->pengembangan.'</td>
				// <td class="tengah">'.($data_akhir->pengembangan + $data_akhir->peng).'</td>
				// </tr>
				// <tr>
				// <td></td>
				// <th align="left">JUMLAH UNSUR UTAMA</th>
        // <th class="tengah">'.($data_akhir->pen1 + $data_akhir->pen2 + $data_akhir->pen3 + $data_akhir->pem + $data_akhir->peng).'</th>
				// <th class="tengah">'.($data_akhir->pendidikan_1 + $data_akhir->pendidikan_2 + $data_akhir->pendidikan_3 + $data_akhir->pemeriksaan + $data_akhir->pengembangan).'</th>
				// <th class="tengah">'.$total_utama.'</th>
				// </tr>
				// <tr>
				// <td class="tengah">2</td>
				// <th colspan="4" align="left">UNSUR PENUNJANG</th>
				// </tr>
				// <tr>
				// <td></td>
				// <td>Pendukung Kegiatan Pemeriksaan</td>
				// <td class="tengah">'.$data_akhir->pen.'</td>
				// <td class="tengah">'.$data_akhir->penunjang.'</td>
				// <td class="tengah">'.($data_akhir->pen + $data_akhir->penunjang).'</td>
				// </tr>
				// <tr>
				// <td></td>
				// <th align="left">JUMLAH UNSUR PENUNJANG</th>
				// <td class="tengah">'.$data_akhir->pen.'</td>
				// <td class="tengah">'.$data_akhir->penunjang.'</td>
				// <td class="tengah">'.($data_akhir->pen + $data_akhir->penunjang).'</td>
				// </tr>
				// <tr>
				// <td></td>
				// <th align="left">JUMLAH UNSUR UTAMA DAN PENUNJANG</th>
				// <th class="tengah">'.($total_lama_utama + $data_akhir->pen).'</th>
				// <th class="tengah">'.($total_baru_utama + $data_akhir->penunjang).'</th>
				// <th class="tengah text-danger">'.($total_utama + ($data_akhir->pen + $data_akhir->penunjang)).'</th>
				// </tr>
	// 		</table>');
  //
	// 		return redirect()->to($mpdf->Output('Penetapan_Angka_Kredit.pdf', 'I'));
	// 	}else{
	// 		return redirect()->to(base_url().'/pengajuan');
	// 	}
	// }

  public function pdf($id = null, $nip = null, $idperiode = null){
      $modelpegawai = new \App\Models\ModelPegawai();
      $modeltotalkredit = new \App\Models\ModelTotalKredit();
      $modelperiode = new \App\Models\Periode_model();

      $data_akhir = $modeltotalkredit->dataku($nip, $id)->getResult()[0];
      // var_dump($data_akhir);die;
      $dp = $modelpegawai->datalengkap($nip)->getResult()[0];
      $dpe = $modelpegawai->datalengkap($data_akhir->nip_penilai)->getResult()[0];
      $periode = $modelperiode->dataperiode($idperiode)[0];
      $index_bulan_awal = (substr($periode['tgl_buka'], 5, 2) * 1) -1;
      $index_bulan_akir = (substr($periode['tgl_tutup'], 5, 2) * 1) -1;

      $total_pendidikan1 = $data_akhir->pendidikan_1;
      $total_pendidikan2 = $data_akhir->pendidikan_2;
      $total_pendidikan3 = $data_akhir->pendidikan_3;
      $total_pemeriksaan = $data_akhir->pemeriksaan;
      $total_pengembangan = $data_akhir->pengembangan;

      $total_pen1 = $data_akhir->pen1;
      $total_pen2 = $data_akhir->pen2;
      $total_pen3 = $data_akhir->pen3;
      $total_pem = $data_akhir->pem;
      $total_peng = $data_akhir->peng;

      $total_baru_utama = $total_pendidikan1 + $total_pendidikan2 + $total_pendidikan3 + $total_pemeriksaan + $total_pengembangan;
      $total_lama_utama = $total_pen1 + $total_pen2 + $total_pen3 + $total_pem + $total_peng;
      $total_utama = $total_baru_utama + $total_lama_utama;
		// $bidang = "";
      	// if(session()->get('idbidang') == "1"){
        // $bidang = "Desain Industri";
		// 	}else if(session()->get('idbidang') == "2"){
		// $bidang = "Paten";
		// 	}else if(session()->get('idbidang') == "3"){
		// $bidang = "Merek";
		// }

		// $model = new \App\Models\Pengajuan();

		if($id <> null){
			$direktorat ='';
			$name ='';
			$nip ='';
			$nsk ='';
			$pangkat ='';
			$golongan ='';
			$tmt ='';
			$tempat ='';
			$ttgl ='';
			$jk ='';
			$study ='';
			$uk ='';
      $dir ='';
      $nipdir ='';
			// $row = $query->getRowArray();
			// return var_dump($row);

			// foreach($query->getResult('array') as $row)
			// {
			// }

      $dbpengajux = \Config\Database::connect();
      $dbperiode = \Config\Database::connect();
      $dataperiodenya = $dbperiode->table('periode')
      ->where(['idperiode' => $idperiode])
      ->get()->getRowArray();
      $dbpegawai = $dbpengajux->table('pegawai p')
      ->join('jenis_pemeriksa jp', 'jp.idjp=p.idbidang')
      ->join('m_pangkat mpang', 'mpang.idpangkat=p.idpangkat')
      ->join('m_golongan mg', 'mg.idgolongan=p.idgolongan')
      ->join('m_jekel mj', 'mj.idjekel=p.jk')
      ->join('m_pendidikan mp', 'mp.idpendidikan=p.idpendidikan')
      ->join('m_direktur md', 'md.idbidang=p.idbidang')
      ->where(['nip' => session()->get('nip')])
      ->get()->getRowArray();

      $direktorat .=''.$dbpegawai['jenispemeriksa'].'';
			$name .=''.$dbpegawai['nama'].'';
			$nip .=''.$dbpegawai['nip'].'';
			$nsk .=''.$dbpegawai['no_karpeg'].'';
			$pangkat .=''.$dbpegawai['pangkat'].'';
			$golongan .=''.$dbpegawai['golongan'].'';
			$tmt .=''.$dbpegawai['tmt_pegawai'].'';
			$tempat .=''.$dbpegawai['tempat_lahir'].'';
			$ttgl .=''.$dbpegawai['tgl_lahir'].'';
			$jk .=''.$dbpegawai['jekel'].'';;
			$study .=''.$dbpegawai['pendidikan'].'';
			$uk .=''.$dbpegawai['jenispemeriksa'].'';
			$dir .=''.$dbpegawai['nama_direktur'].'';
			$nipdir .=''.$dbpegawai['nipdir'].'';

			$mpdf = new \Mpdf\Mpdf();
			$mpdf->SetHeader('Menteri Hukum Dan Hak Asasi Manusia||Direktorat Jenderal Kekayaan Intelektual');
			$mpdf->WriteHTML('
			<table style="width:100%;">
        <tr>
          <td style="text-align:center;font-size:15px;font-weight:bold;">PENETAPAN ANGKA KREDIT</td>
        </tr>
        <tr>
          <td style="text-align:center;font-size:15px;font-weight:bold;"><br></td>
        </tr>
        <tr>
          <td style="text-align:center;font-size:14px;font-weight:bold;">Nomor : . . . . . . . . . . . . . . . . . .</td>
        </tr>
        <tr>
          <td style="text-align:center;font-size:14px;font-weight:bold;">Masa Penilaian : '.$this->ambilbulan($dataperiodenya["tgl_buka"]).' s/d '.$this->ambilbulan($dataperiodenya["tgl_tutup"]).' '.$this->ambiltahun($dataperiodenya["tgl_tutup"]).'</td>
        </tr>
        <tr>
          <td style="text-align:center;font-size:15px;font-weight:bold;"><br></td>
        </tr>
      </table>
			<table border="1" id="datatable" class="table table-striped table-bordered" style="border-collapse: collapse;font-size:13px;">
			<tr>
				<th colspan="5" align="left">I. KETERANGAN PERORANGAN</th>
			</tr>
			<tr>
			<td class="tengah">1</td>
			<td colspan="1" align="left">N A M A</td>
			<td colspan="3">'.$name.'</td>
			</tr>
			<tr>
			<td class="tengah">2</td>
			<td colspan="1" align="left">N I P</td>
			<td colspan="3">'.$nip.'</td>
			</tr>
			<tr>
			<td class="tengah">3</td>
			<td colspan="1" align="left">Nomor Seri Kartu Pegawai</td>
			<td colspan="3">'.$nsk.'</td>
			</tr>
			<tr>
			<td class="tengah">4</td>
			<td colspan="1" align="left">Pangkat & Golongan Ruang / Terhitung Mulai Tanggal</td>
			<td colspan="3">'.$pangkat.' ('.$golongan.') / '.$this->tglindonesia($tmt).'</td>
			</tr>
			<tr>
			<td class="tengah">5</td>
			<td colspan="1" align="left">Tempat dan Tanggal Lahir</td>
			<td colspan="3">'.$tempat.', '.$this->tglindonesia($ttgl).'</td>
			</tr>
			<tr>
			<td class="tengah">6</td>
			<td colspan="1" align="left">Jenis Kelamin</td>
			<td colspan="3">'.$jk.'</td>
			</tr>
			<tr>
			<td class="tengah">7</td>
			<td colspan="1" align="left">Pendidikan Tertinggi</td>
			<td colspan="3">'.$study.'</td>
			</tr>
			<tr>
			<td class="tengah">8</td>
			<td colspan="1" align="left">Jabatan Pemeriksa / Terhitung Mulai Tanggal</td>
			<td colspan="3">-</td>
			</tr>
			<tr>
			<td class="tengah">9</td>
			<td colspan="1" align="left">Unit Kerja</td>
			<td colspan="3">Direktorat '.$uk.'</td>
			</tr>
			<tr>
				<th colspan="2" align="left">II. PENETAPAN ANGKA KREDIT</th>
				<th class="tengah">LAMA</th>
				<th class="tengah">BARU</th>
				<th class="tengah">JUMLAH</th>
				</tr>
				<tr>
				<td class="tengah">1</td>
				<th colspan="4" align="left">UNSUR UTAMA</th>
        <tr>
        <td></td>
        <td>a. (1) Pendidikan Formal dan mencapai gelar / ijasah</td>
        <td class="tengah"><center>'.$data_akhir->pen1.'</center></td>
        <td class="tengah"><center>'.$data_akhir->pendidikan_1.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pendidikan_1 + $data_akhir->pen1).'</center></td>
        </tr>
        <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat surat Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
        <td class="tengah"><center>'.$data_akhir->pen2.'</center></td>
        <td class="tengah"><center>'.$data_akhir->pendidikan_2.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pendidikan_2 + $data_akhir->pen2).'</center></td>
        </tr>
        <tr>
        <td></td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;(3) Diklat Prajabatan</td>
        <td class="tengah"><center>'.$data_akhir->pen3.'</center></td>
        <td class="tengah"><center>'.$data_akhir->pendidikan_3.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pendidikan_3 + $data_akhir->pen3).'</center></td>
        </tr>
        <tr>
        <td></td>
        <td>b. Pemeriksaan</td>
        <td class="tengah"><center>'.$data_akhir->pem.'</center></td>
        <td class="tengah"><center>'.$data_akhir->pemeriksaan.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pemeriksaan + $data_akhir->pem).'</center></td>
        </tr>
        <tr>
        <td></td>
        <td>c. Pengembangan Profesi</td>
        <td class="tengah"><center>'.$data_akhir->peng.'</center></td>
        <td class="tengah"><center>'.$data_akhir->pengembangan.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pengembangan + $data_akhir->peng).'</center></td>
        </tr>
        <tr>
        <td></td>
        <th align="left">JUMLAH UNSUR UTAMA</th>
        <th class="tengah">'.($data_akhir->pen1 + $data_akhir->pen2 + $data_akhir->pen3 + $data_akhir->pem + $data_akhir->peng).'</th>
        <th class="tengah">'.($data_akhir->pendidikan_1 + $data_akhir->pendidikan_2 + $data_akhir->pendidikan_3 + $data_akhir->pemeriksaan + $data_akhir->pengembangan).'</th>
        <th class="tengah">'.$total_utama.'</th>
        </tr>
        <tr>
        <td class="tengah">2</td>
        <th colspan="4" align="left">UNSUR PENUNJANG</th>
        </tr>
        <tr>
        <td></td>
        <td>Pendukung Kegiatan Pemeriksaan</td>
        <td class="tengah"><center>'.$data_akhir->pen.'</center></td>
        <td class="tengah"><center>'.$data_akhir->penunjang.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pen + $data_akhir->penunjang).'</center></td>
        </tr>
        <tr>
        <td></td>
        <th align="left">JUMLAH UNSUR PENUNJANG</th>
        <td class="tengah"><center>'.$data_akhir->pen.'</center></td>
        <td class="tengah"><center>'.$data_akhir->penunjang.'</center></td>
        <td class="tengah"><center>'.($data_akhir->pen + $data_akhir->penunjang).'</center></td>
        </tr>
        <tr>
        <td></td>
        <th align="left">JUMLAH UNSUR UTAMA DAN PENUNJANG</th>
        <th class="tengah">'.($total_lama_utama + $data_akhir->pen).'</th>
        <th class="tengah">'.($total_baru_utama + $data_akhir->penunjang).'</th>
        <th class="tengah text-danger">'.($total_utama + ($data_akhir->pen + $data_akhir->penunjang)).'</th>
        </tr>
				</table>
				<br>
				<small>Asli : disampaikan dengan hormat kepada :</small>
				<br>
				<small>Kepala BKN Up. Deputi Bidang Informasi Kepegawaian BKN</small>
				<br>
				<br>
        <table style="width:100%;">
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            <small>Ditetapkan di Jakarta</small>
          </td>
        </tr>
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            <small>Pada tanggal, '.$this->tglindonesia($tmt).'</small>
          </td>
        </tr>
        <tr>
          <td style="width:60%;color:transparent;"><br></td>
          <td style="text-align:center;"></td>
        </tr>
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            <small>Direktur '.$direktorat.' dan Indikasi Geografis</small>
          </td>
        </tr>
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            <small>Direktorat Jenderal Kekayaan Intelektual</small>
          </td>
        </tr>
        <tr>
          <td style="width:60%;color:transparent;"><br></td>
          <td style="text-align:center;"></td>
        </tr>
        <tr>
          <td style="width:60%;color:transparent;"><br></td>
          <td style="text-align:center;"></td>
        </tr>
        <tr>
          <td style="width:60%;color:transparent;"><br></td>
          <td style="text-align:center;"></td>
        </tr>
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            <small>'.$dir.'</small>
          </td>
        </tr>
        <tr>
          <td style="width:60%;"></td>
          <td style="text-align:center;">
            	<small>'.$nipdir.'</small>
          </td>
        </tr>
        </table>
				<br>
        <br>
				<small>TEMBUSAN disampaikan kepada :</small>
				<br>
				<small>1. Pemeriksa '.$uk.' yang bersangkutan;</small>
				<br>
				<small>2. Ketua Tim Penilai yang bersangkutan;</small>
				<br>
				<small>3. Kepala Biro / Bagian Kepegawaian Ditjen K.I; dan</small>
				<br>
				<small>4. Pejabat yang berwenang menetapkan angka kredit.</small>

				');

				return redirect()->to($mpdf->Output('Penetapan_Angka_Kredit.pdf', 'I'));
		}else{
			return redirect()->to(base_url().'/pengajuan');
		}
	}

  private function tglindonesia($tanggal){
  $bulan = array(
  'Januari','Februari','Maret','April',
  'Mei','Juni','Juli','Agustus',
  'September','Oktober','November','Desember'
  );

  $tgl = substr($tanggal, 8, 2);
  $bln = $bulan[(substr($tanggal, 5, 2)+0)-1];
  $thn = substr($tanggal, 0, 4);

  return $tgl." ".$bln." ".$thn;
}

  private function ambilbulan($tanggal){
    $bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    $bln = (substr($tanggal, 5, 2) * 1) -1;
    return $bulan[$bln];
  }

  private function ambiltahun($tanggal){
    return substr($tanggal, 0, 4);;
  }

  private function tgl_indo($tanggal){
    $tgl = substr($tanggal, 8);
    $bln = (substr($tanggal, 5, 2) * 1) -1;
    $thn = substr($tanggal, 0, 4);

    return $tgl.' '.$this->bln_indo($bln).' '.$thn;
  }

  private function bln_indo($index, $tgl = null){
    $tahun = "";
    $bulan = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
    if($tgl !== null){
      $tahun = " ".substr($tgl, 0, 4);
    }
    return $bulan[$index]."".$tahun;
  }
}
