<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Jenis_unsur_model;
use App\Models\M_unsur_model;
use App\Models\M_sub_unsur_model;
use App\Models\M_kegiatan_model;

class M_kegiatan extends BaseController
{

	protected $M_kegiatan;

	public function __construct() {

        $this->m_kegiatan = new M_kegiatan_model();
    }

	public function index()
	{

		$data['m_kegiatan'] = $this->m_kegiatan->getM_kegiatan();
		$data['page']	 = 'page/m_kegiatan';
		echo view('web', $data);
	}

	public function tampilkansubunsur(){
		if($this->request->getPost('validasi') === "go"){
			$model = new \App\Models\ModelSubUnsur();
			$idunsur = $this->request->getPost('idunsur');
			$idjenisunsur = $this->request->getPost('idjenisunsur');
			$idbidang = $this->request->getPost('idbidang');
			return json_encode($model->dataid($idunsur)->getResult());
		}
		return false;
	}

	public function tampilkansubunsur2(){
		if($this->request->getPost('validasi') === "go"){
			$model = new \App\Models\ModelSubUnsur();
			$idunsur = $this->request->getPost('idunsur');
			$idjenisunsur = $this->request->getPost('idjenisunsur');
			$idbidang = $this->request->getPost('idbidang');
			return json_encode($model->dataid2($idunsur, $idjenisunsur, $idbidang)->getResult());
		}
		return false;
	}

	public function add()
  	{
		//Proteksi
    	$this->m_sub_unsur = new M_sub_unsur_model();
    	$data['m_sub_unsur'] = $this->m_sub_unsur->getM_sub_unsur();
    	$this->m_unsur = new M_unsur_model();
    	$data['m_unsur'] = $this->m_unsur->getM_unsur();
    	$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['page'] = 'page/m_kegiatan_add';
    	return view('web', $data);
  	}

	public function save()
	{

    	$idsubunsur = $this->request->getPost('idsubunsur');
    	$idunsur = $this->request->getPost('idunsur');
    	$idjenisunsur = $this->request->getPost('idjenisunsur');
    	$idbidang = $this->request->getPost('idbidang');
    	$masterkegiatan = $this->request->getPost('masterkegiatan');

    	$data = [
      		'idsubunsur' => $idsubunsur,
      		'idunsur' => $idunsur,
      		'idjenisunsur' => $idjenisunsur,
      		'idbidang' => $idbidang,
      		'masterkegiatan' => $masterkegiatan
    	];

    $simpan = $this->m_kegiatan->insert_m_kegiatan($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_kegiatan'));
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->m_sub_unsur = new M_sub_unsur_model();
    	$data['m_sub_unsur'] = $this->m_sub_unsur->getM_sub_unsur();
    	$this->m_unsur = new M_unsur_model();
    	$data['m_unsur'] = $this->m_unsur->getM_unsur();
    	$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['m_kegiatan'] = $this->m_kegiatan->getM_kegiatan($id);
    	$data['page'] = 'page/m_kegiatan_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idsubunsur = $this->request->getPost('idsubunsur');
		$idunsur = $this->request->getPost('idunsur');
		$idjenisunsur = $this->request->getPost('idjenisunsur');
		$idmasterkegiatan = $this->request->getPost('idmasterkegiatan');
		$masterkegiatan = $this->request->getPost('masterkegiatan');

		$data = [
			'idsubunsur' => $idsubunsur,
			'idunsur' => $idunsur,
			'idjenisunsur' => $idjenisunsur,
			'idmasterkegiatan' => $idmasterkegiatan,
			'masterkegiatan' => $masterkegiatan
		];

	$ubah = $this->m_kegiatan->update_m_kegiatan($data, $idmasterkegiatan);

	if($ubah)
	{
		return redirect()->to(base_url('m_kegiatan'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_kegiatan->delete_m_kegiatan($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_kegiatan'));
		}
	}


	//--------------------------------------------------------------------

}
