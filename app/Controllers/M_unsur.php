<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Jenis_unsur_model;
use App\Models\M_unsur_model;

class M_unsur extends BaseController
{

	protected $M_unsur;

	public function __construct() {

        $this->m_unsur = new M_unsur_model();
    }

	public function index()
	{

		$data['m_unsur'] = $this->m_unsur->getM_unsur();
		$data['page']	 = 'page/m_unsur';
		echo view('web', $data);
	}

	public function add()
  	{
		//Proteksi
    	$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['page'] = 'page/m_unsur_add';
    	return view('web', $data);
  	}

	public function save()
	{

    	$idjenisunsur = $this->request->getPost('idjenisunsur');
    	$unsur = $this->request->getPost('unsur');
			$idbidang = $this->request->getPost('idbidang');
			$idkategori = $this->request->getPost('idkategori');

    	$data = [
      		'idjenisunsur' => $idjenisunsur,
      		'unsur' => $unsur,
					'idbidang' => $idbidang,
					'idkategori' => $idkategori
    	];

    $simpan = $this->m_unsur->insert_m_unsur($data);

    if($simpan)
    {
      //session()->setFlashdata('success', 'Created product successfully');
      return redirect()->to(base_url('m_unsur'));
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->jenis_unsur = new Jenis_unsur_model();
    	$data['jenis_unsur'] = $this->jenis_unsur->getJenis_unsur();
    	$data['m_unsur'] = $this->m_unsur->getM_unsur($id);
    	$data['page'] = 'page/m_unsur_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$idjenisunsur = $this->request->getPost('idjenisunsur');
		$idunsur = $this->request->getPost('idunsur');
		$unsur = $this->request->getPost('unsur');

		$data = [
			'idjenisunsur' => $idjenisunsur,
			'idunsur' => $idunsur,
			'unsur' => $unsur
		];

	$ubah = $this->m_unsur->update_m_unsur($data, $idunsur);

	if($ubah)
	{
		return redirect()->to(base_url('m_unsur'));
	}

}

	public function delete($id)
	{

		$hapus = $this->m_unsur->delete_m_unsur($id);

		if($hapus)
		{
			return redirect()->to(base_url('m_unsur'));
		}
	}


	//--------------------------------------------------------------------

}
