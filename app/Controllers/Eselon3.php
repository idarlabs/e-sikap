<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Eselon1_model;
use App\Models\Eselon2_model;
use App\Models\Eselon3_model;

class Eselon3 extends BaseController
{

	protected $Eselon3_model;
	
	public function __construct() {

        $this->eselon3 = new Eselon3_model();
    }

	public function index()
	{

		$data['eselon3'] = $this->eselon3->getEselon3();
		$data['page']	 = 'page/eselon3';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$this->eselon2 = new Eselon2_model();
    	$data['eselon2'] = $this->eselon2->getEselon2();    	
    	$data['page'] = 'page/eselon3_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$ideselon1 = $this->request->getPost('ideselon1');
		$ideselon2 = $this->request->getPost('ideselon2');
    	$n_unitsatuankerja3 = $this->request->getPost('n_unitsatuankerja3');

    	$data = [
      		'ideselon1' => $ideselon1,
      		'ideselon2' => $ideselon2,      		
      		'n_unitsatuankerja3' => $n_unitsatuankerja3
    	];

    $simpan = $this->eselon3->insert_eselon3($data);

    if($simpan)
    {

      return redirect()->to(base_url('eselon3')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$this->eselon2 = new Eselon2_model();
    	$data['eselon2'] = $this->eselon2->getEselon2();    	
    	$data['eselon3'] = $this->eselon3->getEselon3($id);
    	$data['page'] = 'page/eselon3_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$ideselon1 = $this->request->getPost('ideselon1');
		$ideselon2 = $this->request->getPost('ideselon2');		
		$ideselon3 = $this->request->getPost('ideselon3');
		$n_unitsatuankerja3 = $this->request->getPost('n_unitsatuankerja3');

		$data = [
			'ideselon1' => $ideselon1,
			'ideselon2' => $ideselon2,			
			'n_unitsatuankerja3' => $n_unitsatuankerja3
		];

	$ubah = $this->eselon3->update_eselon3($data, $ideselon3);
	
	if($ubah)
	{
		return redirect()->to(base_url('eselon3'));
	}

}

	public function delete($id)
	{

		$hapus = $this->eselon3->delete_eselon3($id);

		if($hapus)
		{
			return redirect()->to(base_url('eselon3'));
		}
	}


	//--------------------------------------------------------------------

}
