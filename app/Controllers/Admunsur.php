<?php
namespace App\Controllers;

use App\Models\ModelUnsur;

class Admunsur extends BaseController
{
	public function index(){
		$model = new ModelUnsur();
		$m_unsur = $model->data()->getResult();
    $data = [
      'h1' => 'Form Unsur',
      'bread' => 'Tambah Unsur',
			'm_unsur' => $m_unsur
    ];

		return view('kegiatan/admunsur', $data);
	}

	public function simpan(){
		$model = new ModelUnsur();
		$data = [
			'unsur' => $this->request->getPost('unsur')
		];
		$model->simpan($data);
		return redirect()->to(base_url('admunsur'));
	}

	public function edit(){
		$model = new ModelUnsur();
		$id = $this->request->getPost('idunsur');
		$data = array (
			'unsur' => $this->request->getPost('unsur')
		);

		$model->edit($data, $id);
		return redirect()->to(base_url('admunsur'));
	}

	public function hapus(){
		$model = new ModelUnsur();
		$id = $this->request->getPost('idunsurx');
		$model->hapus($id);
		return redirect()->to(base_url('admunsur'));
	}
}
