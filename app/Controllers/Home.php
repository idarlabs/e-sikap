<?php namespace App\Controllers;

use App\Models\Pengajuan;

class Home extends BaseController
{
	public function index()
	{
		if(session()->get('username')==''){
			return redirect()->to(base_url('login'));
		}else{
			$data = [
				'h1' => 'Dashboard',
				'bread' => 'Dashboard',
			];

			$db = db_connect();
			$data['pending'] = $db->table('pengajuan')->where('status',1)->get()->getNumRows();
			$data['ditolak'] = $db->table('pengajuan')->where('status',2)->get()->getNumRows();
			$data['diterima'] = $db->table('pengajuan')->where('status',3)->get()->getNumRows();
			// $this->pengajuan = new Pengajuan();
			// $data['pengajuan'] = $db->query("Select idbidang,status, count(idbidang) as total from pengajuan group by idbidang")->getResultArray();
			$data['industripending'] = $db->table('pengajuan')->where('status',1)->where('idbidang',1)->get()->getNumRows();
			$data['industriditolak'] = $db->table('pengajuan')->where('status',2)->where('idbidang',1)->get()->getNumRows();
			$data['industriselesai'] = $db->table('pengajuan')->where('status',3)->where('idbidang',1)->get()->getNumRows();
			
			$data['patenpending'] = $db->table('pengajuan')->where('status',1)->where('idbidang',2)->get()->getNumRows();
			$data['patenditolak'] = $db->table('pengajuan')->where('status',2)->where('idbidang',2)->get()->getNumRows();
			$data['patenselesai'] = $db->table('pengajuan')->where('status',3)->where('idbidang',2)->get()->getNumRows();

			$data['merekpending'] = $db->table('pengajuan')->where('status',1)->where('idbidang',3)->get()->getNumRows();
			$data['merekditolak'] = $db->table('pengajuan')->where('status',2)->where('idbidang',3)->get()->getNumRows();
			$data['merekselesai'] = $db->table('pengajuan')->where('status',3)->where('idbidang',3)->get()->getNumRows();
			
			// return print_r($industri);
			if(session()->get('level') == "Admin"){
				return view('admin/index', $data);
			}else if(session()->get('level') == "Pejabat Fungsional"){
				return view('user/index', $data);
			}else if(session()->get('level') == "Tim Penilai"){
				return view('user/penilaiuser');
			}
		}
	}


	//--------------------------------------------------------------------

}
