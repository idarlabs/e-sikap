<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Ms_jabatan_model;


class Ms_jabatan extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new Ms_jabatan_model();
		$data['ms_jabatan'] = $model->getMs_jabatan()->getResult();
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/ms_jabatan';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new Ms_jabatan_model();
		$data = array(
			'nama_jabatan' => $this->request->getPost('nama_jabatan'),
		);
		$model->saveMs_jabatan($data);
		return redirect()->to(base_url('ms_jabatan'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new Ms_jabatan_model();
		$id = $this->request->getPost('id_jabatan');
		$data = array (
			'nama_jabatan' => $this->request->getPost('nama_jabatan'),
		);

		$model->updateMs_jabatan($data, $id);
		return redirect()->to(base_url('ms_jabatan'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new Ms_jabatan_model();
		$id = $this->request->getPost('id_jabatan');
		$model->deleteMs_jabatan($id);
		return redirect()->to(base_url('ms_jabatan'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
