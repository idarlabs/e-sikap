<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Eselon1_model;
use App\Models\Eselon2_model;

class Eselon2 extends BaseController
{

	protected $Eselon2_model;
	
	public function __construct() {

        $this->eselon2 = new Eselon2_model();
    }

	public function index()
	{

		$data['eselon2'] = $this->eselon2->getEselon2();
		$data['page']	 = 'page/eselon2';
		echo view('web', $data);
	}
	
	public function add()
  	{
		//Proteksi
    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$data['page'] = 'page/eselon2_add';
    	return view('web', $data);
  	}

	public function save()
	{
    
    	$ideselon1 = $this->request->getPost('ideselon1');
    	$n_unitsatuankerja2 = $this->request->getPost('n_unitsatuankerja2');

    	$data = [
      		'ideselon1' => $ideselon1,
      		'n_unitsatuankerja2' => $n_unitsatuankerja2
    	];

    $simpan = $this->eselon2->insert_eselon2($data);

    if($simpan)
    {

      return redirect()->to(base_url('eselon2')); 
    }
  }

    public function edit($id)
  	{
		//Proteksi

    	$this->eselon1 = new Eselon1_model();
    	$data['eselon1'] = $this->eselon1->getEselon1();
    	$data['eselon2'] = $this->eselon2->getEselon2($id);
    	$data['page'] = 'page/eselon2_edit';
    	return view('web', $data);
  	}

	public function update()
	{
		$ideselon1 = $this->request->getPost('ideselon1');
		$ideselon2 = $this->request->getPost('ideselon2');
		$n_unitsatuankerja2 = $this->request->getPost('n_unitsatuankerja2');

		$data = [
			'ideselon1' => $ideselon1,
			'ideselon2' => $ideselon2,
			'n_unitsatuankerja2' => $n_unitsatuankerja2
		];

	$ubah = $this->eselon2->update_eselon2($data, $ideselon2);
	
	if($ubah)
	{
		return redirect()->to(base_url('eselon2'));
	}

}

	public function delete($id)
	{

		$hapus = $this->eselon2->delete_eselon2($id);

		if($hapus)
		{
			return redirect()->to(base_url('eselon2'));
		}
	}


	//--------------------------------------------------------------------

}
