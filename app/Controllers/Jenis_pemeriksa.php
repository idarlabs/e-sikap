<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Jenis_pemeriksa_model;


class Jenis_pemeriksa extends Controller
{

//	protected $Ms_jabatan_model;
	
//	public function __construct() {
		
//        $this->ms_jabatan = new Ms_jabatan_model();
//    }
	
	public function index()
	{
		$model = new Jenis_pemeriksa_model();
		$data['jenis_pemeriksa'] = $model->getJenis_pemeriksa();
		// dd($data['m_pendidikan']);
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/jenis_pemeriksa';
        echo view('web', $data);
	}
	
	public function save()
	{
		$model = new Jenis_pemeriksa_model();
		$data = array(
			'jenispemeriksa' => $this->request->getPost('jenispemeriksa'),			
		);
		$model->saveJenis_pemeriksa($data);
		return redirect()->to(base_url('jenis_pemeriksa'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new Jenis_pemeriksa_model();
		$id = $this->request->getPost('idjp');
		$data = array (
			'jenispemeriksa' => $this->request->getPost('jenispemeriksa'),	
		);

		$model->updateJenis_pemeriksa($data, $id);
		return redirect()->to(base_url('jenis_pemeriksa'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new Jenis_pemeriksa_model();
		$id = $this->request->getPost('idjp');
		$model->deleteJenis_pemeriksa($id);
		return redirect()->to(base_url('jenis_pemeriksa'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
