<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\M_direktur_model;

class M_direktur extends Controller
{

//	protected $Ms_jabatan_model;

//	public function __construct() {

//        $this->ms_jabatan = new Ms_jabatan_model();
//    }

	public function index()
	{
		$model = new M_direktur_model();
		$data['m_direktur'] = $model->getM_direktur();
		// dd($data['m_direktur']);
		//$data['ms_jabatan']	= $this->ms_jabatan->getMs_jabatan();
        $data['page']		= 'page/m_direktur';
        echo view('web', $data);
	}

	public function save()
	{
		$model = new M_direktur_model();
		$data = [
			'nama_direktur' => htmlspecialchars($this->request->getVar('nama_direktur')),
			'nipdir' => htmlspecialchars($this->request->getVar('nipdir')),
			'idbidang' => htmlspecialchars($this->request->getVar('idbidang')),
		];
		$model->saveM_direktur($data);
		return redirect()->to(base_url('m_direktur'));
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [
//			'nama_jabatan'	=> $nama_jabatan,
//		];

//		$simpan = $this->ms_jabatan->insert_ms_jabatan($data);

//		if($simpan)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function update()
	{
		$model = new M_direktur_model();
		$id = $this->request->getPost('iddirektur');
		$data = array (
			'nama_direktur' => htmlspecialchars($this->request->getVar('nama_direktur')),
			'nipdir' => htmlspecialchars($this->request->getVar('nipdir')),
			'idbidang' => htmlspecialchars($this->request->getVar('idbidang')),
		);

		$model->updateM_direktur($data, $id);
		return redirect()->to(base_url('m_direktur'));
//		$id

//		$id_jabatan = $this->request->getPost('id_jabatan');
//		$nama_jabatan = $this->request->getPost('nama_jabatan');

//		$data = [

//			'nama_jabatan' => $nama_jabatan,
//		];

//		$ubah = $this->ms_jabatan->update_ms_jabatan($data, $id_jabatan);

//		if($ubah)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}

	public function delete()
	{
		$model = new M_direktur_model();
		$id = $this->request->getPost('iddirektur');
		$model->deleteM_direktur($id);
		return redirect()->to(base_url('m_direktur'));
//		$hapus = $this->ms_jabatan->delete_ms_jabatan($id);

//		if($hapus)
//		{
//			return redirect()->to(base_url('ms_jabatan'));
//		}
	}


	//--------------------------------------------------------------------

}
