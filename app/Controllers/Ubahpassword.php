<?php namespace App\Controllers;

use CodeIgniter\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class Ubahpassword extends BaseController {

  public function index(){
    return view("lupapassword");
  }

  public function proses(){
    if($this->p('validasi') === "go"){
      helper('text');
      $mail = new PHPMailer(true);

      try {
        //Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host       = getenv("email.host");                     //Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
        $mail->Username   = getenv("email.username");                     //SMTP username
        $mail->Password   = getenv("email.password");                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port       = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        //Recipients
        $mail->setFrom('mail@sikap.dgip.go.id', 'Ubah Password');
        $mail->addAddress($this->p("mailjarjit"), 'User Sikap DGIP');     //Add a recipient

        //generate PASSWORD
        $modeluser = new \App\Models\User_model();
        $acak = random_string();
        //================================================
        $tampung = $modeluser->cariuser($this->p("mailjarjit"));
        if($tampung != null){
          $modeluser->update_user(['password' => password_hash($acak, PASSWORD_BCRYPT)], $tampung->userid);
          //Content
          $mail->isHTML(true);                                  //Set email format to HTML
          $mail->Subject = 'Mail System DJKI';
          $mail->Body    = 'Password Baru Anda adalah <strong>'.$acak.'</strong>';

          $mail->send();
        }
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
    }
  }

  private function p($arr){
    return $this->request->getPost($arr);
  }

}
