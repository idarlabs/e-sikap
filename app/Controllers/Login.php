<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Login_model;

class Login extends BaseController
{

	protected $Login_model;

	public function __construct() {

		helper('form');

        $this->login = new Login_model();
    }

	public function index()
	{
        echo view('page/login');
	}

	public function cek_login()
	{

		helper('text');
        $username = $this->request->getPost('username');
        $pwd = $this->request->getPost('password');
		$password = md5($pwd);

		$dbLogin  = new \App\Models\Login_model();
		$cek = $this->login->cekLogin($username, $password);
		//dd($cek);
		//if(!empty($cek) && ($cek['username'] == $username) && ($cek['password'] == $password)){
		if($cek > 0){
			$ceklogin = $this->login->getLogin($username, $password);
			session()->set('userid',$ceklogin['userid']);
			session()->set('username',$ceklogin['username']);
			session()->set('level',$ceklogin['level']);
			session()->set('nip',$ceklogin['nip']);

			return redirect()->to(base_url('home'));
		}else{
			session()->setFlashdata('gagal','Username atau Password Salah..!!');
			return redirect()->to(base_url('login'));
		}

	}

	public function auth(){
		if($this->request->isAjax()){
            $this->response->setContentType('application/json; charset=utf-8');
            $json_response=[];
            $dataInput = json_decode(file_get_contents('php://input'),true);
            if ($dataInput['submit']) {
                $rules = [
                    'username' => [
                        'label' => 'Username',
                        'rules' => 'required|validateUser[username]|validateUserActive[username]',
                        'errors' => [
                            'required' => 'Kolom {field} tidak boleh kosong',
                            'validateUser' => 'Akun dengan {field} {value} tidak tersedia',
                            'validateUserActive' => "Akun dengan {field} {value} tidak aktif.",
                        ],
                    ],
                    'password' => [
                        'label' => 'Password',
                        'rules' => 'required|validatePassword[username,password]',
                        'errors' => [
                            'required' => 'Kolom {field} tidak boleh kosong',
                            'validatePassword' => 'Kombinasi Username dan {field} tidak sesuai',
                        ],
                    ],
                ];
                if ($this->validate($rules)) {
                    $dbUser = new \App\Models\User_model();
                    $selectUser = $dbUser->jLevel($dataInput['username']);
					$session=array();
					switch ($selectUser->idlevel) {
						case '1':
							$session = [
								'userid' => $selectUser->userid,
								'username' => $selectUser->username,
								'idlevel' => $selectUser->idlevel,
								'level' => $selectUser->level,
								'nip' => $selectUser->nip,
								'status' => $selectUser->status,
								'isLoggedIn' => true,
							];
							break;
						case '2':
							$dbPegawai = new \App\Models\Pegawai_model();
							$dbpenilai = new \App\Models\ModelTimPenilai();

							$selectPenilai = $this->db->query("select * from penilai where nip = '".$selectUser->nip."'");
							$cekpenilai = count($selectPenilai->getResult());

							$selectPegawai = $dbPegawai->jGolonganByNip($selectUser->nip);

							$session = [
								'nama'  => $selectPegawai->nama,
								'golongan'  => $selectPegawai->idgolongan,
								'golru'  => $selectPegawai->golongan,
								'statuspenilai'  => $cekpenilai,
								'idbidang'  => $selectPegawai->idbidang,
								'pangkat' => $selectPegawai->idpangkat,
								'jabatan' => $selectPegawai->idjabatan,
								'userid' => $selectUser->userid,
								'username' => $selectUser->username,
								'idlevel' => $selectUser->idlevel,
								'level' => $selectUser->level,
								'nip' => $selectUser->nip,
								'status' => $selectUser->status,
								'isLoggedIn' => true,
							];
							break;

						default:
							# code...
							break;
					}
					session()->set($session);
                    $json_response['code']='1';
                    $json_response['message']['redirect']=base_url('home');
                } else {
                    $json_response['code']='0';
                    $json_response['message']['text']='<strong>Login Gagal</strong><hr>' . $this->validator->listErrors();
                }
            }
            return $this->response->setJSON($json_response);
        }
	}

	public function logout()
	{
		session()->destroy();
        return redirect()->to(base_url('login'));
	}

	//--------------------------------------------------------------------

}
