<?php
function ajaxCallback($log=true)
{
    $data='';
    $onsuccess='';
    $onerror = '';
    $timeout = 60000;
    if ($log) {
        $data = <<<DATA
console.log(arguments[1]);
    console.log(arguments[2]);
DATA
;
        $onsuccess = <<<ONSUCCESS
console.log('success');
            console.log(data);
            console.log(textStatus);
            console.log(jqXHR);
ONSUCCESS;

    $onerror = <<<ONERROR
console.log('error');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
ONERROR;
    }

    $ajaxCallback = <<<AJAXCALLBACK
function ajaxCallback(callback){
    $data
    \$.ajax({
        url: arguments[1],
        type: 'POST',
        headers: {'X-Requested-With' : 'XMLHTTPRequest'},
        contentType: 'application/json; charset=UTF-8',
        dataType: 'json',
        data: JSON.stringify(arguments[2]),
        timeout: $timeout,
        success: function (data,textStatus,jqXHR){
            $onsuccess
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $onerror
            let data = {
                code : '0',
                message: {text : "Terjadi error sintaks (err. " + jqXHR.statusText + " | " + jqXHR.responseText + ")"}
            };
            callback(data);
        }
    });
}

AJAXCALLBACK;
return $ajaxCallback;
}

// ajaxCallback form data 

function ajaxCallbackFormData($log=true)
{
    $data='';
    $onsuccess='';
    $onerror = '';
    $timeout = 60000;
    if ($log) {
        $data = <<<DATA
console.log(arguments[1]);
    console.log(arguments[2]);
DATA
;
        $onsuccess = <<<ONSUCCESS
console.log('success');
        console.log(data);
        console.log(textStatus);
        console.log(jqXHR);
ONSUCCESS;

    $onerror = <<<ONERROR
console.log('error');
        console.log(jqXHR);
        console.log(textStatus);
        console.log(errorThrown);
ONERROR;
    }

    $ajaxCallbackFormData = <<<AJAXCALLBACK
function ajaxCallbackFormData(callback){
    $data
    \$.ajax({
        url: arguments[1],
        type: 'POST',
        headers: {'X-Requested-With' : 'XMLHTTPRequest'},
        contentType: false,
        processData: false,
        data: arguments[2],
        timeout: $timeout,
        success: function (data,textStatus,jqXHR){
            $onsuccess
            callback(data);
        },
        error: function (jqXHR, textStatus, errorThrown){
            $onerror
            let data = {
                code : '0',
                message: {text : "Terjadi error sintaks (err. " + jqXHR.responseText+")"}
            };
            callback(data);
        }
    });
}

AJAXCALLBACK;
return $ajaxCallbackFormData;
}