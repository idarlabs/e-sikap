<?php
namespace App\Validation;
class UserRules{
    public function validateUser(string $str, string $fields, array $data)
    {
        $dbUser = new \App\Models\User_model();
        $selectUser = $dbUser->where('username',$data['username'])
                                ->first();
        if ($selectUser) {
            return true;
        } 
        return false;
    }

    public function validateUserActive(string $str, string $fields, array $data)
    {
        $dbUser = new \App\Models\User_model();
        $selectUser = $dbUser->where('username', $data['username'])
                                ->where('status', '1')
                                ->first();
        if ($selectUser) {
            return true;
        }
        return false;
    }

    public function validatePassword(string $str, string $fields, array $data)
    {
        $dbUser = new \App\Models\User_model();
        $selectUser = $dbUser->where('username', $data['username'])
                                ->where('status', '1')
                                ->first();
        if ($selectUser) {
            return password_verify($data['password'],$selectUser->password);
        }
        return false;
    }
}