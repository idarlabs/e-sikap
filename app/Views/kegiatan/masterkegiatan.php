<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>

<div class="card">
  <div class="card-body">
    <div class="col-sm-12 table-responsive">
      <table id="example1" class="table table-bordered table-striped teks15">
        <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</button>
        <p></p>
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th class="">Direktorat</th>
            <th>Jenis Unsur</th>
            <th>Unsur</th>
            <th>Sub Unsur</th>
            <th>Kegiatan</th>
            <th>Butir Kegiatan</th>
            <th>Ditetapkan Kepada</th>
            <th class="tengah">Kredit</th>
            <th class="tengah">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $nonya = 1; foreach($mk as $m): $kredit = ($m->kredit % 1 != 0) ? $m->kredit : floor($m->kredit); ?>
            <tr>
              <td class="tengah"><?=$nonya;?></td>
              <td class=""><?=$m->jpkjenis;?></td>
              <td><?=$m->jenis;?></td>
              <td><?=$m->unsur;?></td>
              <td><?=$m->keterangan;?></td>
              <td><?=$m->masterkegiatan;?></td>
              <td><?=$m->butirkegiatan;?></td>
              <td><?=$m->jabatan;?></td>
              <td class="tengah"><?=$m->kredit;?></td>
              <td class="tengah sembarang">

                <!-- <button class="btn btn-sm btn-success"><i class="fas fa-edit"></i>Ubah&nbsp;</button> -->
                <button class="btn btn-sm btn-danger" onclick="hapus('<?=$m->idkegiatan;?>')"><i class="fas fa-trash-alt"></i></button>
              </td>
            </tr>
          <?php $nonya++; endforeach;?>
        </tbody>
    </table>
    </div>
  </div>
</div>


<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Master Kegiatan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-sm-6">
            <label>Jenis Pemeriksa Kerja</label>
            <select class="form-control" name="jpk" id="jpk" onchange="aktifkan_jenis_unsur(this.value)">
              <?php foreach($m_jpk as $ju): ?>
                <option value="<?=$ju->idjenispemeriksakerja;?>"><?=$ju->jenis;?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="col-sm-6">
            <label>Jenis Unsur</label>
            <select class="form-control" name="jenis_unsur" id="jenis_unsur" onchange="aktifkan_unsur(this.value)">
              <option value="">-- Pilih Jenis Unsur --</option>
              <?php foreach($jenis_unsur as $ju): ?>
                <option value="<?=$ju->idjenisunsur;?>"><?=$ju->jenis;?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="col-sm-6">
            <label>Unsur</label>
            <select class="form-control" name="unsur" id="unsur" onchange="aktifkan_subunsur(this.value)" disabled placeholder="Unsur"></select>
          </div>
          <div class="col-sm-6">
            <label>Sub Unsur</label>
            <select class="form-control" name="subunsur" id="subunsur" onchange="aktifkan_mkegiatan(this.value)" disabled placeholder="Sub Unsur"></select>
          </div>
          <div class="col-sm-6">
            <label>Kegiatan</label>
            <select class="form-control" name="mkegiatan" id="mkegiatan" onchange="aktifkan_butirkegiatan(this.value)" disabled placeholder="Poin Kegiatan"></select>
          </div>
          <div class="col-sm-6">
            <label>Butir Kegiatan</label>
            <input type="text" name="butirkegiatan" id="butirkegiatan" class="form-control" disabled placeholder="Butir Kegiatan">
          </div>
          <div class="col-sm-6">
            <label>Ditetapkan kepada</label>
            <select class="form-control" name="jabatan" id="jabatan" disabled>
              <?php foreach($m_jabatan as $ju): ?>
                <option value="<?=$ju->idjabatan;?>"><?=$ju->jabatan;?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="col-sm-6 kanan">
            <label>Kredit</label>
            <input type="number" name="kredit" id="kredit" class="form-control kanan" disabled placeholder="Kredit">
          </div>
          <div class="col-sm-12 kanan">
            <button type="button" id="simpankegiatan" class="btn btn-primary">Simpan Kegiatan</button>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- Modal Delete Product-->
        <form action="<?= base_url() ?>/masterkegiatan/hapus" method="post">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Master Kegiatan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                   <h8>Apakah anda yakin ingin menghapus data ini ?</h8>

                 </div>
                 <div class="modal-footer">
                  <input type="hidden" name="idkegiatan" class="idkegiatan">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                  <button type="submit" class="btn btn-danger">Yes</button>
                </div>
              </div>
            </div>
          </div>
        </form>
<!-- End Modal Delete Product-->


<script type="text/javascript">
  function hapus(idkegiatan){
    if(confirm('Anda yakin akan menghapus data ini?')){
      $.ajax({
  			url: '<?php echo base_url('Masterkegiatan/hapus') ?>',
  			type: "POST",
  			data: {
  				validasi: 'go',
  				idkegiatan: idkegiatan
  			},
  			success: function(hasil) {
          location.reload()
  			},
  			error: function(error) {
  				alert("error");butirkegiatan
  				console.error(error);
  			}
  		})
    }
  }

  $('#simpankegiatan').on('click', function(){
    var jpk = $('#jpk').val()
    var jenis_unsur = $('#jenis_unsur').val()
    var unsur = $('#unsur').val()
    var subunsur = $('#subunsur').val()
    var mkegiatan = $('#mkegiatan').val()
    var butirkegiatan = $('#butirkegiatan').val()
    var jabatan = $('#jabatan').val()
    var kredit = $('#kredit').val()

    if(jenis_unsur !== "" && unsur !== "" && subunsur !== "" && mkegiatan !== "" && butirkegiatan !== "" && kredit !== ""){
      if(confirm("Anda yakin akan menyimpan kegiatan ini?")){
        $.ajax({
    			url: '<?php echo base_url('Masterkegiatan/simpan') ?>',
    			type: "POST",
    			data: {
    				validasi: 'go',
    				jpk: jpk,
    				jenis_unsur: jenis_unsur,
    				unsur: unsur,
    				subunsur: subunsur,
    				mkegiatan: mkegiatan,
    				butirkegiatan: butirkegiatan,
    				jabatan: jabatan,
    				kredit: kredit
    			},
    			success: function(hasil) {
            location.reload()
    			},
    			error: function(error) {
    				alert("error");
    			}
    		})
      }
    }else{
      alert('Pastikan Anda sudah mengisi form dengan benar.')
    }
  })

  function aktifkan_butirkegiatan(id){
    if(id !== ""){
      $('#butirkegiatan').prop('disabled', false)
      $('#kredit').prop('disabled', false)
      $('#jabatan').prop('disabled', false)
    }else{
      $('#butirkegiatan').prop('disabled', true)
      $('#kredit').prop('disabled', true)
      $('#jabatan').prop('disabled', true)
    }
  }

  function tampilkan_mkegiatan(id){
    $.ajax({
			url: '<?php echo base_url('Masterkegiatan/mkegiatan') ?>',
			type: "POST",
			data: {
				validasi: 'go',
				id: id
			},
			success: function(hasil) {
        var obj = JSON.parse(hasil);
        $('#mkegiatan').empty()
        $('#mkegiatan').append('<option value="">-- Pilih Kegiatan --</option>')
        obj.forEach(data => {
          $('#mkegiatan').append('<option value="'+data.idmasterkegiatan+'">'+data.masterkegiatan+'</option>')
        })
			},
			error: function(error) {
				alert("error");butirkegiatan
				console.error(error);
			}
		})
  }

  function aktifkan_mkegiatan(id){
    if(id !== ""){
      $('#mkegiatan').prop('disabled', false)
      tampilkan_mkegiatan(id)
    }else{
      $('#mkegiatan').empty()
      $('#mkegiatan').prop('disabled', true)
      $('#butirkegiatan').prop('disabled', true)
      $('#kredit').prop('disabled', true)
      $('#jabatan').prop('disabled', true)
    }
  }

  function aktifkan_jenis_unsur(idbidang){
    if(idbidang != ""){
      $('#subunsur').val('')
      $('#unsur').val('')
      $('#mkegiatan').val('')
      $('#jenis_unsur').val('')
    }
  }

  function aktifkan_unsur(id){
    var unsur = $('#unsur')
    $('#subunsur').empty()
    $('#unsur').empty()
    $('#mkegiatan').empty()

    $('#subunsur').val('')
    $('#unsur').val('')
    $('#mkegiatan').val('')

    if(id !== ""){
      unsur.prop('disabled', false)
      tampilkanUnsur(id)
    }else{
      $('#subunsur').prop('disabled', true)
      $('#mkegiatan').prop('disabled', true)
      $('#butirkegiatan').prop('disabled', true)
      $('#kredit').prop('disabled', true)
      $('#jabatan').prop('disabled', true)
      $('#butirkegiatan').val("")
      $('#kredit').val("")
      unsur.prop('disabled', true)
    }
  }

  function tampilkanUnsur(id) {
		$.ajax({
			url: '<?php echo base_url('Masterkegiatan/unsur') ?>',
			type: "POST",
			data: {
				validasi: 'go',
				id: id,
        idbidang: $('#jpk').val()
			},
			success: function(hasil) {
        var obj = JSON.parse(hasil);
        $('#unsur').empty()
        $('#unsur').append('<option value="">-- Pilih Unsur --</option>')
        obj.forEach(data => {
          $('#unsur').append('<option value="'+data.idunsur+'">'+data.unsur+'</option>')
        })
			},
			error: function(error) {
				alert("error");
				console.error(error);
			}
		})
	}

  function aktifkan_subunsur(id){
    var subunsur = $('#subunsur')
    $('#subunsur').empty()
    $('#mkegiatan').empty()
    if(id !== ""){
      subunsur.prop('disabled', false)
      $('#mkegiatan').val('')
      tampilkanSubUnsur(id)
    }else{
      $('#mkegiatan').prop('disabled', true)
      $('#butirkegiatan').prop('disabled', true)
      $('#kredit').prop('disabled', true)
      $('#jabatan').prop('disabled', true)
      $('#butirkegiatan').val("")
      $('#kredit').val("")
      subunsur.prop('disabled', true)
    }
  }

  function tampilkanSubUnsur(id) {
		$.ajax({
			url: '<?php echo base_url('Masterkegiatan/subunsur') ?>',
			type: "POST",
			data: {
				validasi: 'go',
				id: id,
        idbidang: $('#jpk').val()
			},
			success: function(hasil) {
        var obj = JSON.parse(hasil);
        $('#subunsur').empty()
        $('#subunsur').append('<option value="">-- Pilih Sub Unsur --</option>')
        obj.forEach(data => {
          $('#subunsur').append('<option value="'+data.idsubunsur+'">'+data.keterangan+'</option>')
        })
			},
			error: function(error) {
				alert("error");
				console.error(error);
			}
		})
	}

        // get Delete Product
        $('.btn-delete').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            // Set data to Form Edit
            $('.idkegiatan').val(id);
            // Call Modal Edit
            $('#deleteModal').modal('show');
          });

</script>
<?php echo $this->endSection();?>
