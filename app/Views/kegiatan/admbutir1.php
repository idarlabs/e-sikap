<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>
<div class="card">
  <div class="card-body">
    <div class="col-sm-12">
      <table id="example1" class="table table-bordered table-striped">
        <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</button>
        <p></p>
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th class="tengah width75">Keterangan</th>
            <th class="tengah">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach($butir1 as $d){ ?>
            <tr>
              <td class="tengah"><?=$no;?></td>
              <td><?=$d->keterangan;?></td>
              <td class="tengah">
                <button type="button" class="btn btn-warning btn_edit" data-toggle="modal" data-id="<?= $d->idbutir1;?>" data-keterangan="<?= $d->keterangan;?>" data-idunsur="<?= $d->idunsur;?>" data-idsubunsur="<?= $d->idsubunsur;?>" data-target="#editModal">Ubah</button>
                <button type="button" class="btn btn-danger btn_hapus" data-toggle="modal" data-id="<?= $d->idbutir1;?>" data-keterangan="<?= $d->keterangan;?>" data-idunsur="<?= $d->idunsur;?>" data-idsubunsur="<?= $d->idsubunsur;?>" data-target="#hapusModal">Hapus</button>
              </td>
            </tr>
          <?php } ?>
      </tbody>
    </table>
    </div>
  </div>
</div>

<!-- form tambah -->
<form action="<?= base_url() ?>/admbutir1/simpan" method="POST" role="form">
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Unsur</label>
            <select class="form-control" name="unsur" onchange="cek(this.value)" required>
              <option value="">-- Pilih Unsur --</option>
              <?php foreach($m_unsur as $m):?>
                <option value="<?=$m->idunsur;?>"><?=$m->unsur;?></option>
              <?php endforeach; ?>
            </select>
            <select class="form-control" name="subunsur" onchange="cek(this.value)" required>
              <option value="">-- Pilih Sub Unsur --</option>
              <?php foreach($m_sub_unsur as $m):?>
                <option value="<?=$m->idsubunsur;?>"><?=$m->keterangan;?></option>
              <?php endforeach; ?>
            </select>
            <label>Keterangan</label>
            <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" required disabled>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- form edit -->
<form action="<?= base_url() ?>/admbutir1/edit" method="POST" role="form">
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Unsur</label>
            <select class="form-control" id="unsure" name="unsure" required>
              <option value="">-- Pilih Unsur --</option>
              <?php foreach($m_unsur as $m):?>
                <option value="<?=$m->idunsur;?>"><?=$m->unsur;?></option>
              <?php endforeach; ?>
            </select>
            <select class="form-control" id="subunsure" name="subunsure" onchdisabledange="ceke1(this.value)" required>
              <option value="">-- Pilih Sub Unsur --</option>
              <?php foreach($m_sub_unsur as $m):?>
                <option value="<?=$m->idsubunsur;?>"><?=$m->keterangan;?></option>
              <?php endforeach; ?>
            </select>
            <label>Keterangan</label>
            <input type="text" class="form-control" id="keterangane" name="keterangane" placeholder="Keterangan" required>
            <input type="hidden" name="idbutir1e" id="idbutir1e">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- form hapus -->
<form action="<?= base_url() ?>/admbutir1/hapus" method="POST" role="form">
  <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h8>Apakah anda yakin ingin menghapus data ini ?</h8>
        </div>
        <div class="modal-footer">
          <input type="hidden" id="idsubunsurx" name="idsubunsurx">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Yakin</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">

function cek(id){
  if(id != ""){
    $('#keterangan').prop('disabled', false)
  }else{
    $('#keterangan').prop('disabled', true)
  }
}

function ceke1(id){
  if(id != ""){
    $('#keterangane').prop('disabled', false)
  }else{
    $('#keterangane').prop('disabled', true)
  }
}

$(document).ready(function(){
  $('.btn_edit').on('click',function(){
    const id = $(this).data('id');
    const keterangan = $(this).data('keterangan');
    const idunsur = $(this).data('idunsur');
    const idsubunsur = $(this).data('idsubunsur');
    $('#idbutir1e').val(id);
    $('#keterangane').val(keterangan);
    $('#unsure').val(idunsur);
    $('#subunsure').val(idsubunsur);
    console.log(idunsur);
    $('#editModal').modal('show');
  });

  $('.btn_hapus').on('click',function(){
    const id = $(this).data('id');
    $('#idunsurx').val(id);
    $('#hapusModal').modal('show');
  });
});
</script>
<?php echo $this->endSection();?>
