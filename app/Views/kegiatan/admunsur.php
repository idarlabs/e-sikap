<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>
<div class="card">
  <div class="card-body">
    <div class="col-sm-12">
      <table id="example1" class="table table-bordered table-striped">
        <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</button>
        <p></p>
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th class="tengah width75">Keterangan</th>
            <th class="tengah">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach($m_unsur as $d){ ?>
            <tr>
              <td class="tengah"><?=$no;?></td>
              <td><?=$d->unsur;?></td>
              <td class="tengah">
                <button type="button" class="btn btn-warning btn_edit" data-toggle="modal" data-id="<?= $d->idunsur;?>" data-unsur="<?= $d->unsur;?>" data-target="#editModal">Ubah</button>
                <button type="button" class="btn btn-danger btn_hapus" data-toggle="modal" data-id="<?= $d->idunsur;?>" data-unsur="<?= $d->unsur;?>" data-target="#hapusModal">Hapus</button>
              </td>
            </tr>
          <?php } ?>
      </tbody>
    </table>
    </div>
  </div>
</div>

<!-- form edit -->
<form action="<?= base_url() ?>/admunsur/edit" method="POST" role="form">
  <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Unsur</label>
            <input type="text" class="form-control" id="unsur" name="unsur" placeholder="Unsur" required>
            <input type="hidden" name="idunsur" id="idunsur">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- form tambah -->
<form action="<?= base_url() ?>/admunsur/simpan" method="POST" role="form">
  <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label>Unsur</label>
            <input type="text" class="form-control" name="unsur" placeholder="Unsur" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- form hapus -->
<form action="<?= base_url() ?>/admunsur/hapus" method="POST" role="form">
  <div class="modal fade" id="hapusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah Data Pangkat & Golongan</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h8>Apakah anda yakin ingin menghapus data ini ?</h8>
        </div>
        <div class="modal-footer">
          <input type="hidden" id="idunsurx" name="idunsurx">
          <button type="button" class="btn btn-warning" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Yakin</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
$(document).ready(function(){
  $('.btn_edit').on('click',function(){
    const id = $(this).data('id');
    const unsur = $(this).data('unsur');
    $('#idunsur').val(id);
    $('#unsur').val(unsur);
    $('#editModal').modal('show');
  });

  $('.btn_hapus').on('click',function(){
    const id = $(this).data('id');
    $('#idunsurx').val(id);
    $('#hapusModal').modal('show');
  });
});
</script>
<?php echo $this->endSection();?>
