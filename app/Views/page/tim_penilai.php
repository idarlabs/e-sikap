  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Tim Penilai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Data Tim Penilai </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

    <div class="row">
      <div class="col-md-12">
       <section class="content">
        <div class="row">
          <div class="col-12">
            <!-- /.card -->

            <!-- Modal Add Product-->
            <form action="<?= base_url() ?>/tim_penilai/save" method="POST" role="form">
              <div class="modal fade" style="overflow-y: scroll; max-height:80%; margin-top: 50px; margin-bottom:50px;" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Tambah Data Tim Penilai</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label>NIP</label>
                        <input type="text" class="form-control" required="" name="nip" placeholder="NIP">
                      </div>
                      <div class="form-group">
                        <label>Nama Tim Penilai</label>
                        <input type="text" class="form-control" required="" name="nama" placeholder="Nama Tim Penilai">
                      </div>                      
                      <div class="form-group">
                        <label>Pangkat</label>
                        <select class="form-control" name="idpangkat" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_pangkat as $row) :?>
                            <option value="<?= $row->idpangkat;?>"><?= $row->pangkat;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Golongan</label>
                        <select class="form-control" name="idgolongan" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_golongan as $row) :?>
                            <option value="<?= $row->idgolongan;?>"><?= $row->golongan;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Terhitung Mulai Tanggal</label>
                        <input type="date" name="tmt_pegawai" class="form-control" placeholder="" required>
                      </div>                          
                      <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" required>
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control" placeholder="" required>
                      </div>                                          
                      <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class="form-control" name="jk" required>
                          <option>--Pilih--</option>
                          <option>Laki-laki</option>
                          <option>Perempuan</option>                        
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Pendidikan Tertinggi</label>
                        <select class="form-control" name="idpendidikan" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_pendidikan as $row) :?>
                            <option value="<?= $row->idpendidikan;?>"><?= $row->pendidikan;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>                                 
                      <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" placeholder="Alamat" required> </textarea>
                      </div>
                      <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="telepon" class="form-control" placeholder="Telepon" required>
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="Email" name="email" class="form-control" placeholder="Email" required>
                      </div>
                      <div class="form-group">
                        <label>KTP</label>
                        <input type="text" name="ktp" class="form-control" placeholder="KTP" required>
                      </div>  
                      <div class="form-group">
                        <label>Eselon Unit Kerja</label>
                        <select class="form-control" name="ideselon2" required>
                          <option>--Pilih--</option>
                          <?php foreach($eselon2 as $row) :?>
                            <option value="<?= $row->ideselon2;?>"><?= $row->n_unitsatuankerja2 ;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Jenis Pemeriksa</label>
                        <select class="form-control" name="idjp" required>
                          <option>--Pilih--</option>
                          <?php foreach($jenis_pemeriksa as $row) :?>
                            <option value="<?= $row->idjp;?>"><?= $row->jenispemeriksa ;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>                                                                                                                    
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-danger">Save</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <!-- End Modal Add Product-->

            <!-- Modal Edit Product-->
            <form action="<?= base_url() ?>/tim_penilai/update" method="post">
              <div class="modal fade" style="overflow-y: scroll; max-height:80%; margin-top: 50px; margin-bottom:50px;" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Ubah Data Tim Penilai</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <div class="form-group">
                        <label>NIP</label>
                        <input type="text" class="form-control nip" required="" name="nip" placeholder="NIP">
                      </div>
                      <div class="form-group">
                        <label>Nama Pejabat</label>
                        <input type="text" class="form-control nama" required="" name="nama" placeholder="Nama Pejabat">
                      </div>                      
                      <div class="form-group">
                        <label>Pangkat</label>
                        <select class="form-control idpangkat" name="idpangkat" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_pangkat as $row) :?>
                            <option value="<?= $row->idpangkat;?>"><?= $row->pangkat;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Golongan</label>
                        <select class="form-control idgolongan" name="idgolongan" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_golongan as $row) :?>
                            <option value="<?= $row->idgolongan;?>"><?= $row->golongan;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Terhitung Mulai Tanggal</label>
                        <input type="date" name="tmt_pegawai" class="form-control tmt_pegawai" placeholder="" required>
                      </div>                          
                      <div class="form-group">
                        <label>Tempat Lahir</label>
                        <input type="text" name="tempat_lahir" class="form-control tempat_lahir" placeholder="Tempat Lahir" required>
                      </div>
                      <div class="form-group">
                        <label>Tanggal Lahir</label>
                        <input type="date" name="tgl_lahir" class="form-control tgl_lahir" placeholder="" required>
                      </div>                                          
                      <div class="form-group">
                        <label>Jenis Kelamin</label>
                        <select class="form-control jk" name="jk" required>
                          <option>--Pilih--</option>
                          <option>Laki-laki</option>
                          <option>Perempuan</option>                        
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Pendidikan Tertinggi</label>
                        <select class="form-control idpendidikan" name="idpendidikan" required>
                          <option>--Pilih--</option>
                          <?php foreach($m_pendidikan as $row) :?>
                            <option value="<?= $row->idpendidikan;?>"><?= $row->pendidikan;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>                                 
                      <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control alamat" placeholder="Alamat" required> </textarea>
                      </div>
                      <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" name="telepon" class="form-control telepon" placeholder="Telepon" required>
                      </div>
                      <div class="form-group">
                        <label>Email</label>
                        <input type="Email" name="email" class="form-control email" placeholder="Email" required>
                      </div>
                      <div class="form-group">
                        <label>KTP</label>
                        <input type="text" name="ktp" class="form-control ktp" placeholder="KTP" required>
                      </div>  
                      <div class="form-group">
                        <label>Eselon Unit Kerja</label>
                        <select class="form-control ideselon2" name="ideselon2" required>
                          <option>--Pilih--</option>
                          <?php foreach($eselon2 as $row) :?>
                            <option value="<?= $row->ideselon2;?>"><?= $row->n_unitsatuankerja2 ;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                      <div class="form-group">
                        <label>Jenis Pemeriksa</label>
                        <select class="form-control" name="idjp" required>
                          <option>--Pilih--</option>
                          <?php foreach($jenis_pemeriksa as $row) :?>
                            <option value="<?= $row->idjp;?>"><?= $row->jenispemeriksa ;?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>                        
                    </div>
                    <div class="modal-footer">
                      <input type="hidden" name="idtp" class="idtp">
                      <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-danger">Update</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <!-- End Modal Edit Product-->

            <!-- Modal Delete Product-->
            <form action="<?= base_url() ?>/tim_penilai/delete" method="post">
              <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">

                     <h8>Apakah anda yakin ingin menghapus data ini ?</h8>

                   </div>
                   <div class="modal-footer">
                    <input type="hidden" name="idtp" class="idtp">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-danger">Yes</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!-- End Modal Delete Product-->  



          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
               <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg   mr-2"></i>Tambah</button>
               <p></p>
               <thead>
                <tr>
                  <th><center>No</center></th>
                  <th><center>NIP</center></th>
                  <th><center>Nama</center></th>
                  <th><center>Pangkat</center></th>
                  <th><center>Mulai Tanggal</center></th>
                  <th><center>Tanggal Lahir</center></th>
                  <th><center>Jenis Kelamin</center></th>
                  <th><center>Email</center></th>
                  <th><center>Action</center></th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $no =1;
                foreach($tim_penilai as $row) :?>
                    <tr>
                      <td><center><?php echo $no++; ?></center></td>                      
                      <td><center><?= $row->nip; ?></center></td>
                      <td><center><?= $row->nama; ?></center></td>
                      <td><center><?= $row->pangkat; ?></center></td>
                      <td><center><?= $row->tmt_pegawai; ?></center></td>                      
                      <td><center><?= $row->tgl_lahir; ?></center></td>
                      <td><center><?= $row->jk; ?></center></td>
                      <td><center><?= $row->email; ?></center></td>
                      <td>
                        <center>

                          <a href="#" class="btn btn-success btn-sm btn-edit" data-id="<?= $row->idpegawai; ?>" data-nipnip="<?= $row->nip; ?>" data-name="<?= $row->nama; ?>" data-pangkatid="<?= $row->idpangkat; ?>" data-gol="<?= $row->idgolongan; ?>" data-tmtpegawai="<?= $row->tmt_pegawai; ?>" data-tempatlahir="<?= $row->tempat_lahir; ?>" data-tgllahir="<?= $row->tgl_lahir; ?>" data-kj="<?= $row->jk; ?>" data-pendidikanid="<?= $row->idpendidikan; ?>" data-alamt="<?= $row->alamat; ?>" data-notelp="<?= $row->telepon; ?>" data-aemail="<?= $row->email; ?>" data-noktp="<?= $row->ktp; ?>" data-eselonid2="<?= $row->ideselon2; ?>" data-jp="<?= $row->jenispemeriksa; ?>" ><i class="fas fa-edit"></i></a>
                          <?php if(session()->get('level')=="Administrator"){ ?>
                            <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="<?= $row->idpegawai; ?>"><i class="fas fa-trash-alt"></i></a>
                          <?php } ?>
                        </center>
                      </div>
                    </td>
                  </tr>
                <?php endforeach;?>
              </tbody>
              <tfoot>
                <tr>
                  <th><center>No</center></th>
                  <th><center>NIP</center></th>
                  <th><center>Nama</center></th>
                  <th><center>Pangkat</center></th>
                  <th><center>Mulai Tanggal</center></th>
                  <th><center>Tanggal Lahir</center></th>
                  <th><center>Jenis Kelamin</center></th>
                  <th><center>Email</center></th>
                  <th><center>Action</center></th>
                </tr>
              </tfoot>
            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/js/bootstrap.bundle.min.js"></script>
<script>
    $(document).ready(function(){

        // get Edit Product
        $('.btn-edit').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            const nipnip = $(this).data('nipnip');
            const name = $(this).data('name');
            const pangkatid = $(this).data('pangkatid');
            const gol = $(this).data('gol');
            const tmtpegawai = $(this).data('tmtpegawai');
            const tempatlahir = $(this).data('tempatlahir');
            const tgllahir = $(this).data('tgllahir');
            const kj = $(this).data('kj');
            const pendidikanid = $(this).data('pendidikanid');
            const alamt = $(this).data('alamt');
            const notelp = $(this).data('notelp');
            const aemail = $(this).data('aemail');
            const noktp = $(this).data('noktp');
            const eselonid2 = $(this).data('eselonid2');
            const jp = $(this).data('jp');
            // Set data to Form Edit
            $('.idpegawai').val(id);
            $('.nip').val(nipnip);
            $('.no_karpeg').val(karpeg);
            $('.nama').val(name);            
            $('.idpangkat').val(pangkatid).trigger('change');
            $('.idgolongan').val(gol).trigger('change');
            $('.tmt_pegawai').val(tmtpegawai);
            $('.tempat_lahir').val(tempatlahir);
            $('.tgl_lahir').val(tgllahir);
            $('.jk').val(kj);
            $('.idpendidikan').val(pendidikanid).trigger('change');
            $('.alamat').val(alamt);
            $('.telepon').val(notelp);
            $('.email').val(aemail);
            $('.ktp').val(noktp);
            $('.ideselon2').val(eselonid2).trigger('change');
            $('.jenispemeriksa').val(jp).trigger('change');                         
            // Call Modal Edit
            $('#editModal').modal('show');
        });

        // get Delete Product
        $('.btn-delete').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            // Set data to Form Edit
            $('.idtp').val(id);
            // Call Modal Edit
            $('#deleteModal').modal('show');
        });
        
    });
</script>
</div>