  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Unit Satuan Kerja</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/eselon2">Form Data Unit Satuan Kerja</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url() ?>/eselon2/update" method="POST" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 1</label>
                    <input type="hidden" name="ideselon1" value="<?php echo $eselon2['ideselon1']; ?>" class="form-control" id="id">
                    <select class="form-control" name="ideselon1" readonly required>
                      <?php foreach($eselon1 as $es1) { ?>
                        <option value="<?php echo $es1['ideselon1']; ?>"><?php echo $es1['n_unitsatuankerja1']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 2</label>
                    <input type="text" name="n_unitsatuankerja2" value="<?php echo $eselon2['n_unitsatuankerja2']; ?>" class="form-control" placeholder="Unit Satuan Kerja" required>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

          </div>
          <!--/.col (left) -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>