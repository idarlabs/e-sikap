<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Form Data Direktur</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
            <li class="breadcrumb-item active">Data Direktur</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
     <div class="row">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <!-- /.card-header -->
        <!-- form start -->
        <!-- Modal Add Product-->
        <form action="<?= base_url() ?>/m_direktur/save" method="POST" role="form">
          <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Tambah Data Direktur</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <label>Pilih Bidang</label>
                  <select class="form-control" name="idbidang" id="pilihbidang">
                    <option value="" selected disabled>-- Pilih Bidang --</option>
                    <option value="1">Desain Industri</option>
                    <option value="2">Paten</option>
                    <option value="3">Merek</option>
                  </select><br>
                  <div class="form-group">
                    <label>NIP</label>
                    <input type="text" class="form-control" required="" name="nip">
                  </div>
                  <div class="form-group">
                    <label>Nama Direktur</label>
                    <input type="text" class="form-control" required="" name="nama_direktur">
                  </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!-- End Modal Add Product-->

        <!-- Modal Edit Product-->
        <form action="<?= base_url() ?>/m_direktur/update" method="post">
          <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Ubah Data Direktur</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="form-group">
                    <label>NIP</label>
                    <input type="text" class="form-control nip" required="" name="nip">
                  </div>
                  <div class="form-group">
                    <label>Nama Direktur</label>
                    <input type="text" class="form-control nama_direktur" required="" name="nama_direktur">
                  </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                      <input type="hidden" name="iddirektur" class="iddirektur">
                      <button type="submit" class="btn btn-primary">Update</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                </div>
              </div>
              </div>
            </div>
          </div>
        </form>
        <!-- End Modal Edit Product-->

        <!-- Modal Delete Product-->
        <form action="<?= base_url() ?>/m_direktur/delete" method="post">
          <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">

                 <h8>Apakah anda yakin ingin menghapus data ini ?</h8>

               </div>
               <div class="modal-footer d-flex justify-content-center">
                <input type="hidden" name="idpendidikan" class="idpendidikan">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>
      </form>
      <!-- End Modal Delete Product-->

      <!-- /.row -->
      <div class="row">
        <div class="col-md-12">
         <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- /.card -->

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg   mr-2"></i>Tambah</button>
                    <p></p>
                    <thead>
                      <tr>
                        <th><center>No</center></th>
                        <th><center>Nama Direktur</center></th>
                        <th><center>NIP</center></th>
                        <th><center>Action</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $no =1;
                      foreach($m_direktur as $row) :?>
                        <tr>
                          <td><center><?php echo $no++; ?></center></td>
                          <td><center><?= $row['nama_direktur']; ?></center></td>
                          <td><center><?= $row['nipdir']; ?></center></td>
                          <td><center>

                            <a href="#" class="btn btn-success btn-sm btn-edit" data-id="<?= $row['iddirektur'];?>" data-name="<?= $row['nama_direktur']; ?>"><i class="fas fa-edit"></i>&nbsp;&nbsp;&nbsp;Edit</a>&nbsp;&nbsp;
                            <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="<?= $row['iddirektur']; ?>"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;&nbsp;Hapus</a>
                          </center>
                        </td>
                      </tr>
                    <?php endforeach;?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <th><center>No</center></th>
                      <th><center>Nama Direktur</center></th>
                      <th><center>NIP</center></th>
                      <th><center>Action</center></th>
                    </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </section>
    </div>
  </div>
</div><!-- /.container-fluid -->
</section>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/js/bootstrap.bundle.min.js"></script>
<script>
$(document).ready(function(){

      // get Edit Product
      $('.btn-edit').on('click',function(){
          // get data from button edit
          const id = $(this).data('id');
          const name = $(this).data('name');
          const nipp = $(this).data('nipp');
          // Set data to Form Edit
          $('.iddirektur').val(id);
          $('.nama_direktur').val(name);
          $('.nip').val(nipp);
          // Call Modal Edit
          $('#editModal').modal('show');
        });

      // get Delete Product
      $('.btn-delete').on('click',function(){
          // get data from button edit
          const id = $(this).data('id');
          // Set data to Form Edit
          $('.iddirektur').val(id);
          // Call Modal Edit
          $('#deleteModal').modal('show');
        });

    });
  </script>
  <!-- /.content -->
</div>
