<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Manajemen User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Manajemen User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
             <a href="<?php echo base_url('user/add'); ?>" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i> Add</a>
             <p></p>
             <thead>
              <tr>
                <th>No</th>
                <th>Username</th>
                <th>Level</th>
                <th>NIP</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach($user as $key => $data) { ?>
                <tr>
                  <td><?php echo $key+1; ?></td>
                  <td><?php echo $data['username']; ?></td>
                  <td><?php echo $data['level']; ?></td>
                  <td><?php echo $data['nip']; ?></td>
                  <?php 
                  if($data['status']==0){
                   $status = "Tidak Aktif"; ?>
                   <td><span class="badge badge-danger"><?php echo $status?></span></td>
                   <?php
                 }else{
                   $status = "Aktif"; ?>
                   <td><span class="badge badge-success"><?php echo $status?></span></td>
                <?php
                 }
                 ?>
                 <td>
                  <div class="btn-group">
                    <a href="<?php echo base_url('user/edit/'.$data['userid']); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>&nbsp
                    <?php if(session()->get('level')=="Administrator"){ ?>
                      <a href="<?php echo base_url('user/delete/'.$data['userid']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $data['username']; ?> ini?')"><i class="fas fa-trash-alt"></i></a>
                    <?php } ?>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th>No</th>
              <th>Username</th>
              <th>Level</th>
              <th>NIP</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>