  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Jenis Unsur</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/jenis_unsur">Form Data Master Jenis Unsur</a></li>
              <li class="breadcrumb-item active">Ubah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Ubah Data Jenis Unsur</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            <div class="card-body">
              <section class="content">
                <div class="container-fluid">
                  <div class="form-group">
                    <!-- left column -->
                    <div class="col-md-6">
                      <!-- general form elements -->
                      <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?= base_url() ?>/jenis_unsur/update" method="POST" role="form">
                          <div class="card-body">
                            <div class="form-group">
                              <label>Nama Jenis Unsur</label>
                              <input type="hidden" name="idjenisunsur" value="<?php echo $jenis_unsur['idjenisunsur']; ?>" class="form-control" placeholder="">
                              <input type="text" name="jenis" class="form-control" value="<?php echo $jenis_unsur['jenis'];  ?>" placeholder="Nama Jenis Unsur" required="">
                            </div>
                          </div>
                          <!-- /.card-body -->
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Update</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!--/.col (left) -->

                    <!--/.col (right) -->
                  </div>
                  <!-- /.row -->
                </div><!-- /.container-fluid -->
              </section>
            </div>
        </div>
      </div>
    </div>
  </div>