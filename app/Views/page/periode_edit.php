  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Ubah Periode</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/periode">Form Data Ubah Periode</a></li>
              <li class="breadcrumb-item active">Ubah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url() ?>/periode/update" method="POST" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Kode</label>
                    <input type="hidden" name="id" value="<?php echo $periode['idperiode']; ?>" class="form-control" id="id">
                    <input type="text" readonly="" value="<?php echo $periode['kode']; ?>" name="kode" class="form-control" required>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Buka</label>
                    <input type="date" name="tgl_buka" value="<?php echo $periode['tgl_buka']; ?>" class="form-control" placeholder="Tanggal Buka" required>
                  </div>
                  <div class="form-group">
                    <label>Tanggal Tutup</label>
                    <input type="date" name="tgl_tutup" value="<?php echo $periode['tgl_tutup']; ?>" class="form-control" placeholder="Tanggal Tutup" required>
                  </div>                                    
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Update</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

          </div>
          <!--/.col (left) -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>