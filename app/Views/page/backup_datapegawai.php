  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Pegawai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Tambah Data Pegawai </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          
            <!-- /.card-header -->
            <!-- form start -->
            <form action="<?= base_url() ?>/stok/save" method="POST" role="form">
             
               
                  <div class="row">
                    <!--<div class="col-lg-7">
                      <label>Foto Pegawai</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="customFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-7">
                      <label>Nama Pegawai</label>
                      <input type="text" name="" class="form-control" placeholder=""  required>
                    </div>
                    <div class="col-lg-5">
                      <label>NIP</label>
                      <input type="text" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Nomor Seri Kartu Pegawai</label>
                  <input type="text" name="" class="form-control" placeholder="" required>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-5">
                      <label>Pangkat dan Golongan Ruang</label>
                      <select class="form-control" name="" required>
                        <option value="">--Pilih--</option>
                        <option value="">Penata Muda Tk. I (III/b)</option>
                        <option value="">Penata (III/c)</option>
                        <option value="">Penata Tk.I (III/d)</option>
                        <option value="">Pembina (IV/a)</option>
                        <option value="">Pembina Tk. I (IV/b)</option>
                        <option value="">Pembina Utama Muda (IV/c)</option>
                        <option value="">Pembina Utama Madya (IV/d)</option>                
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <label>Terhitung Mulai Tanggal</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-3">
                      <label>Tempat</label>
                      <input type="text" name="" class="form-control" placeholder="" required>
                    </div>
                    <div class="col-lg-3">
                      <label>Tanggal Lahir</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="" required>
                    <option value="">--Pilih--</option>
                    <option value="">Laki-laki</option>
                    <option value="">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Pendidikan Tertinggi</label>
                  <select class="form-control" name="" required>
                    <option value="">--Pilih--</option>
                    <option value="">SLTA</option>
                    <option value="">D-III</option>
                    <option value="">D-IV/S1</option>
                    <option value="">S2</option>
                    <option value="">S3</option>
                  </select>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-5">
                      <label>Jabatan Pemeriksa Merek</label>
                      <select class="form-control" name="" required>
                        <option value="">--Pilih--</option>
                        <option value="">Pemeriksa Merek P Lanjutan</option>
                        <option value="">Pemeriksa Merek Penyelia</option>
                        <option value="">Pemeriksa Merek Pertama</option>
                        <option value="">Pemeriksa Merek Muda</option>
                        <option value="">Pemeriksa Merek Madya</option>
                        <option value="">Pemeriksa Merek Utama</option>              
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <label>Terhitung Mulai Tanggal</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Unit Kerja</label>
                  <input type="text" name="" class="form-control" placeholder="" required>
                </div>
              </div>
               /.card-body 
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>-->
          <!-- /.card -->

          <!-- Form Element sizes -->

          <!-- /.card -->

          <!-- /.card -->

          <!-- /.card -->

          <!-- /.card -->
<div class="row">
        <div class="col-md-12">
         <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- /.card -->

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">

                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pangkat & Golongan Ruang</th>
                        <th>Mulai Tanggal</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>File Foto</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td>1</td>
                        <td>FIQIH IHROMI, S.H, M.H.</td>
                        <td><a href="<?= base_url() ?>/portofolio" class="nav-link active">198109212009011010</td>
                        <td>Penata Tk. I (III/d)</td>
                        <td>1 April 2019</td>
                        <td>21 September 1981</td>
                        <td>Laki-laki</td>
                        <td>DSC2396.jpg</td>
                        <td>
                          <div class="btn-group">
                            <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin data ini?')"><i class="fas fa-trash-alt"></i></a>&nbsp;
                            <a href="<?= base_url() ?>/portofolio" class="btn btn-primary btn-sm"><i class="fas fa-user"></i></a>
                          </div>
                        </td>
                      </tr>

                        <td>2</td>
                        <td>MARCHIENDA WERDANY, SH</td>
                        <td><a href="<?= base_url() ?>/portofolio" class="nav-link active">198103112003122001</td>
                        <td>Pembina Muda (IV/c)</td>
                        <td>1 Oktober 2020</td>
                        <td>11 Maret 1981</td>
                        <td>Perempuan</td>
                        <td>DSC0098.jpg</td>
                        <td>
                          <div class="btn-group">
                            <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin data ini?')"><i class="fas fa-trash-alt"></i></a>&nbsp;
                            <a href="<?= base_url() ?>/portofolio" class="btn btn-primary btn-sm"><i class="fas fa-user"></i></a>
                          </div>
                        </td>
                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pangkat & Golongan Ruang</th>
                        <th>Mulai Tanggal</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>File Foto</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
      </div>
    </div>
        </div>
        <!--/.col (left) -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
      
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>