  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ubah Password</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Ubah Password</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Form Ubah Password</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
            <div class="card-body">
              <section class="content">
                <div class="container-fluid">
                  <div class="form-group">
                    <!-- left column -->
                    <div class="col-md-6">
                      <!-- general form elements -->
                      <div class="card card-primary">
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="<?= base_url() ?>/user/update_password" method="POST" role="form">
                          <div class="card-body">
                         <?php  
                            $gagal = session()->getFlashdata('gagal');
                            $success = session()->getFlashData('success');
                            if(!empty($gagal))
                            { ?>
                                <div class="alert alert-danger">
                                <?php echo "Gagal merubah password..!! Password tidak cocok!!"; ?>
                                </div>
                            <?php } ?>
                            <?php if(!empty($success))
                            { ?>
                                <div class="alert alert-success">
                                <?php echo "Password berhasil diubah"; ?>
                                </div>
                            <?php } ?>
                          
                            <div class="form-group">
                                <label>Password Lama</label>
                                <input type="password" name="password_lama" class="form-control" placeholder="Masukkan Password Lama" required="">
                            </div>
                            <div class="form-group">
                                <label>Password Baru</label>
                                <input type="password" name="password_baru" class="form-control" placeholder="Masukkan Password Baru" required="">
                            </div>
                          </div>
                          <!-- /.card-body -->
                          <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Save</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <!--/.col (left) -->

                    <!--/.col (right) -->
                  </div>
                  <!-- /.row -->
                </div><!-- /.container-fluid -->
              </section>
            </div>
        </div>
      </div>
    </div>
  </div>