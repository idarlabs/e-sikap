<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>List Pengusul </h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
            <li class="breadcrumb-item active">List Pengusul</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

<!-- Modal Add Product-->
<?php
            if($list_pengajuan <> ''){
              foreach($list_pengajuan as $key => $data) { ?>

<form action="<?= base_url() ?>/list_pengusul/savepenilai/<?=$data['idpengajuan']?>" method="POST" role="form">
            <div class="modal fade" id="addModal<?=$data['idpengajuan']?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Tambah Data Penilai</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body ">
                  <table id="example1" class="table table-bordered table-striped">
             <p></p>
             <thead>
              <tr>
                <th><center>No</center></th>
                <th><center>Daftar Penilai</center></th>
                <th><center>Action</center></th>
              </tr>
            </thead>
            <tbody>

            <?php
            if($list_penilai <> '') {
                foreach($list_penilai as $key => $data) { ?>
                    <tr>
                    <td><center><?php echo $key+1; ?></center></td>
                    <td><center><?php echo $data['nama']; ?></center></td>
                    <td>
                    <center>

                     <input class="form-check-input"  type="radio" value="<?=$data['nip']?>" name="idpenilai">
                        <!-- onClick="sendToTextBox('< ?=$data['idpenilai']?>')"  <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i>&nbsp;Edit</a>&nbsp; -->
                        <!-- <?php if(session()->get('level')=="Administrator"){ ?>
                        <a href="<?php echo base_url('user/delete/'.$data['ideselon2']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $data['n_unitsatuankerja2']; ?> ini?')"><i class="fas fa-trash-alt"></i>&nbsp;Hapus</a>
                        <?php } ?> -->
                    </center>
                    </div>
                    </td>
                </tr>
                 <?php }
              } ?>
          </tbody>
        </table>
                  </div>
                  <div class="modal-footer d-flex justify-content-center">
                   <!-- <input type="text" value="< ?=$list_pengusul['idpengusul']; ?>" class="form-control" >
                  <input type="text" value="< ?=$list_pengusul['idpenilai']; ?>" class="form-control" >  -->
                  <!-- <input type="text" name="idpenilai" value="< ?= $data['idpenilai'];?>" class="form-control" >
                  <input type="text" name="idpengajuan" value="< ?= $data['pengajuan'];?>" class="form-control" > -->
                  <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <?php
          }
        }    ?>
<!-- End Modal Add Product-->

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->
        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
          <form action="<?= base_url() ?>/list_pengusul/search" method="POST" role="form">
          <div class="form-group col-4">
            <label>Pilih Periode</label>
                <select class="form-control" name="idperiode">
                    <option value="" selected disabled>--Pilih Periode--</option>
                    <?php foreach($periode as $p) { ?>
                        <option value="<?php echo $p['idperiode']; ?>" ><?php echo $p['tgl_buka']; ?> sd <?php echo $p['tgl_tutup']; ?></option>
                    <?php } ?>
                </select>
        </div>
        <div class="form-group col-4">
            <label>Pilih Direktorat</label>
                <select class="form-control" name="idbidang" id="idbidang">
                    <option value="" selected disabled>--Pilih Direktorat--</option>
                    <?php foreach($jenis_pemeriksa as $jp) { ?>
                        <option value="<?php echo $jp['idjp']; ?>" ><?php echo $jp['jenispemeriksa']; ?> </option>
                    <?php } ?>
                </select>
        </div>
        <div class="form-group col-4">
        <button type="submit" class="btn btn-primary"></i>Search</button>
        </div>
        </form>
            <table id="example2" class="zero-configuration table table-bordered table-striped">
             <!--  -->
             <p></p>
             <thead>
              <tr>
                <th><center>No</center></th>
                <th><center>Daftar Pengusul</center></th>
                <th><center>Action</center></th>
              </tr>
            </thead>
            <tbody>
            <?php
            if($list_pengajuan <> ''){
              foreach($list_pengajuan as $key => $data) { ?>
                <tr>
                  <td><center><?php echo $key+1; ?></center></td>
                  <td><center><?php echo $data['nama'];?></center></td>
                 <td>
                  <center>
                  <button type="button" data-toggle="modal" data-target="#addModal<?=$data['idpengajuan']?>" class="btn btn-success btn-sm"><i class="fas fa-eye"></i> Detail</button>
                    <!-- <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i>&nbsp;Edit</a>&nbsp; -->
                    <!-- <?php if(session()->get('level')=="Administrator"){ ?>
                      <a href="<?php echo base_url('user/delete/'.$data['ideselon2']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $data['n_unitsatuankerja2']; ?> ini?')"><i class="fas fa-trash-alt"></i>&nbsp;Hapus</a>
                    <?php } ?> -->
                  </center>
                </td>
              </tr>
              <?php
              }
            } ?>
          </tbody>
          <tfoot>
            <tr>
              <th><center>No</center></th>
              <th><center>Daftar Pengusul</center></th>
              <th><center>Action</center></th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</form>
</section>
<script>

</script>
<!-- /.content -->
</div>
