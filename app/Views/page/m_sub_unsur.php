<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Form Data Master Sub Unsur</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
            <li class="breadcrumb-item active">Form Data Master Sub Unsur</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
             <a href="<?php echo base_url('m_sub_unsur/add'); ?>" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</a>
             <p></p>
             <thead>
              <tr>
                <th><center>No</center></th>
                <th><center>Nama Jenis Unsur</center></th>
                <th><center>Nama Unsur</center></th>
                <th><center>Nama Sub Unsur</center></th>
                <th><center>Direktorat</center></th>
                <th><center>Action</center></th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach($m_sub_unsur as $key => $data) { ?>
                <tr>
                  <td><center><?php echo $key+1; ?></center></td>
                  <td><center><?php echo $data['jenis']; ?></center></td>
                  <td><center><?php echo $data['unsur']; ?></center></td>
                  <td><center><?php echo $data['keterangan']; ?></center></td>
                  <td><center><?php echo $data['jenispemeriksa']; ?></center></td>
                  <td>
                    <center>
                      <!-- <a href="<?php echo base_url('m_sub_unsur/edit/'.$data['idsubunsur']); ?>" class="btn btn-success btn-sm"><i class="fas fa-edit"></i>&nbsp;Edit&nbsp;&nbsp;&nbsp;</a> -->
                      <!--session-->
                        <a href="<?php echo base_url('m_sub_unsur/delete/'.$data['idsubunsur']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $data['keterangan']; ?> ini?')"><i class="fas fa-trash-alt"></i>&nbsp;Hapus</a>
                      <!--session-->
                    </center>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
            <tfoot>
              <tr>
                <th><center>No</center></th>
                <th><center>Nama Jenis Unsur</center></th>
                <th><center>Nama Unsur</center></th>
                <th><center>Nama Sub Unsur</center></th>
                <th><center>Direktorat</center></th>
                <th><center>Action</center></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
