  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form User Manajemen</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/user">User Manajemen</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url() ?>/user/update" method="POST" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Username</label>
                    <input type="hidden" name="userid" value="<?php echo $user['userid']; ?>" class="form-control" id="id" > 
                    <input type="text" name="username" value="<?php echo $user['username']; ?>" class="form-control" placeholder="Username" required>
                  </div>
                  <div class="form-group">
                    <label>NIP</label>
                    <input type="text" readonly="" name="nip" value="<?php echo $user['nip']; ?>" class="form-control" placeholder="NIP" required>
                  </div>
                  <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
                  </div>
                  <div class="form-group">
                    <label>Level</label>
                    <select class="form-control" name="level" required>
                      <option value="">--Pilih--</option>
                      <?php if ($user['level']=="Admin"){ ?>
                        <option value="Admin" selected="Admin">Admin</option>
                      <?php } else{ ?>
                        <option value="Admin">Admin</option>
                      <?php } ?>                      
                      <?php if(session()->get('level')=='Administrator'){ ?>

                        <?php if ($user['level']=="Administrator") { ?>
                          <option value="Administrator" selected="Administrator">Administrator</option>
                        <?php } else{ ?>
                          <option value="Administrator">Administrator</option>
                        <?php } ?>
                      <?php } ?>
                      <?php if ($user['level']=="Employee"){ ?>
                        <option value="Employee" selected="Employee">Employee</option>
                      <?php } else {?>
                        <option value="Employee">Employee</option>  
                      <?php } ?>
                      <?php if ($user['level']=="Penilai") { ?>
                        <option value="Penilai" selected="Penilai">Penilai</option>
                      <?php } else {?>
                        <option value="Penilai">Penilai</option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Status User</label>
                    <select class="form-control" name="status" required="">
                      <?php if($user['status']==1) { ?>
                        <option value="1" selected>Aktif</option>
                      <?php } else{ ?>
                        <option value="1">Aktif</option>
                      <?php } ?>
                      <?php if($user['status']==0) { ?>
                        <option value="0" selected>Tidak Aktif</option>
                      <?php } else{ ?>
                        <option value="0">Tidak Aktif</option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                  <!-- /.card-body -->
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                  </div>
                </form>
              </div>
              <!-- /.card -->

              <!-- Form Element sizes -->

              <!-- /.card -->

              <!-- /.card -->

              <!-- /.card -->

              <!-- /.card -->

            </div>
            <!--/.col (left) -->

            <!--/.col (right) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>