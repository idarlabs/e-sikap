  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Unit Satuan Kerja</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/eselon4">Form Data Unit Satuan Kerja</a></li>
              <li class="breadcrumb-item active">Edit</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url() ?>/eselon4/update" method="POST" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 1</label>
                    <input type="hidden" name="ideselon1" value="<?php echo $eselon4['ideselon1']; ?>" class="form-control" id="id">
                    <select class="form-control" name="ideselon1" readonly required>
                      <?php foreach($eselon1 as $es1) { ?>
                        <option value="<?php echo $es1['ideselon1']; ?>"><?php echo $es1['n_unitsatuankerja1']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 2</label>
                    <input type="hidden" name="ideselon2" value="<?php echo $eselon4['ideselon2']; ?>" class="form-control" id="id">
                    <select class="form-control" name="ideselon1" required>
                      <?php foreach($eselon2 as $es2) { ?>
                        <option value="<?php echo $es2['ideselon2']; ?>"><?php echo $es2['n_unitsatuankerja2']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 3</label>
                    <input type="hidden" name="ideselon3" value="<?php echo $eselon4['ideselon3']; ?>" class="form-control" id="id">
                    <select class="form-control" name="ideselon1" required>
                      <?php foreach($eselon3 as $es3) { ?>
                        <option value="<?php echo $es3['ideselon3']; ?>"><?php echo $es3['n_unitsatuankerja3']; ?></option>
                      <?php } ?>
                    </select>
                  </div>                                     
                  <div class="form-group">
                    <label>Satuan Kerja Eselon 4</label>
                    <input type="hidden" name="ideselon4" value="<?php echo $eselon4['ideselon4']; ?>" class="form-control" id="id">
                    <input type="text" name="n_unitsatuankerja4" value="<?php echo $eselon4['n_unitsatuankerja4']; ?>" class="form-control" placeholder="Unit Satuan Kerja" required>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

          </div>
          <!--/.col (left) -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>