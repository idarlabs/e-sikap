  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Jenis Pemeriksa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Data Jenis Pemeriksa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <!-- /.card-header -->
          <!-- form start -->
          <!-- Modal Add Product-->
          <form action="<?= base_url() ?>/jenis_pemeriksa/save" method="POST" role="form">
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data Jenis Pemeriksa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">
                      <label>Nama Jenis Pemeriksa</label>
                      <input type="text" class="form-control" required="" name="jenispemeriksa" placeholder="Nama Jenis Pemeriksa">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Save</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!-- End Modal Add Product-->

          <!-- Modal Edit Product-->
          <form action="<?= base_url() ?>/jenis_pemeriksa/update" method="post">
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Data Jenis Pemeriksa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div class="form-group">

                      <label>Nama Jenis Pemeriksa</label>
                      <input type="text" class="form-control jenispemeriksa" required="" name="jenispemeriksa" placeholder="Nama Jenis Pemeriksa">
                    </div>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="idjp" class="idjp">
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Update</button>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <!-- End Modal Edit Product-->

          <!-- Modal Delete Product-->
          <form action="<?= base_url() ?>/jenis_pemeriksa/delete" method="post">
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">

                   <h8>Apakah anda yakin ingin menghapus data ini ?</h8>

                 </div>
                 <div class="modal-footer">
                  <input type="hidden" name="idjp" class="idjp">
                  <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
                  <button type="submit" class="btn btn-danger">Yes</button>
                </div>
              </div>
            </div>
          </div>
        </form>
        <!-- End Modal Delete Product-->          

        <!-- /.row -->
        <div class="row">
          <div class="col-md-12">
           <section class="content">
            <div class="row">
              <div class="col-12">
                <!-- /.card -->

                <div class="card">
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                      <button type="button" data-toggle="modal" data-target="#addModal" class="btn btn-primary"><i class="fas fa-plus fa-lg   mr-2"></i>Tambah</button>
                      <p></p>
                      <thead>
                        <tr>
                          <th><center>No</center></th>
                          <th><center>Nama Jenis Pemeriksa</center></th>
                          <th><center>Action</center></th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $no =1;
                        foreach($jenis_pemeriksa as $row) :?>
                          <tr>
                            <td><center><?php echo $no++; ?></center></td>
                            <td><center><?= $row['jenispemeriksa']; ?></center></td>                        
                            <td><center>

                              <a href="#" class="btn btn-success btn-sm btn-edit" data-id="<?= $row['idjp'];?>" data-name="<?= $row['jenispemeriksa']; ?>"><i class="fas fa-edit"></i>&nbsp;&nbsp;&nbsp;Edit</a>&nbsp;&nbsp;
                              <a href="#" class="btn btn-danger btn-sm btn-delete" data-id="<?= $row['idjp']; ?>"><i class="fas fa-trash-alt"></i>&nbsp;&nbsp;&nbsp;Hapus</a>
                            </center>
                          </td>
                        </tr>
                      <?php endforeach;?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th><center>No</center></th>
                        <th><center>Nama Jenis Pemeriksa</center></th>
                        <th><center>Action</center></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/jquery.min.js"></script>
<script src="<?= base_url() ?>/assets/bootstrap/dist/js/js/bootstrap.bundle.min.js"></script>
<script>
  $(document).ready(function(){

        // get Edit Product
        $('.btn-edit').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            const name = $(this).data('name');
            // Set data to Form Edit
            $('.idjp').val(id);
            $('.jenispemeriksa').val(name);
            // Call Modal Edit
            $('#editModal').modal('show');
          });

        // get Delete Product
        $('.btn-delete').on('click',function(){
            // get data from button edit
            const id = $(this).data('id');
            // Set data to Form Edit
            $('.idjp').val(id);
            // Call Modal Edit
            $('#deleteModal').modal('show');
          });
        
      });
    </script>
    <!-- /.content -->
  </div>