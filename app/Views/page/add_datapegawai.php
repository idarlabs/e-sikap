  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Pegawai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Tambah Data Pegawai </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       <div class="row">
        <!-- left column -->
        <div class="col-md-9">
          <!-- general form elements -->
          <div class="card card-primary">
            <!-- /.card-header -->
            <!-- form start -->
            <form action="<?= base_url() ?>/stok/save" method="POST" role="form">
              <div class="card-body">
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-7">
                      <label>Foto Pegawai</label>
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="foto" id="customFile">
                        <label class="custom-file-label" for="customFile">Pilih foto</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-7">
                      <label>Nama Pegawai</label>
                      <input type="text" name="" class="form-control" placeholder="" required>
                    </div>
                    <div class="col-lg-5">
                      <label>NIP</label>
                      <input type="text" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Nomor Seri Kartu Pegawai</label>
                  <input type="text" name="" class="form-control" placeholder="" required>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-5">
                      <label>Pangkat dan Golongan Ruang</label>
                      <select class="form-control" name="" required>
                        <option value="">--Pilih--</option>
                        <option value="">Penata Muda Tk. I (III/b)</option>
                        <option value="">Penata (III/c)</option>
                        <option value="">Penata Tk.I (III/d)</option>
                        <option value="">Pembina (IV/a)</option>
                        <option value="">Pembina Tk. I (IV/b)</option>
                        <option value="">Pembina Utama Muda (IV/c)</option>
                        <option value="">Pembina Utama Madya (IV/d)</option>                
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <label>Terhitung Mulai Tanggal</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-3">
                      <label>Tempat</label>
                      <input type="text" name="" class="form-control" placeholder="" required>
                    </div>
                    <div class="col-lg-3">
                      <label>Tanggal Lahir</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control" name="" required>
                    <option value="">--Pilih--</option>
                    <option value="">Laki-laki</option>
                    <option value="">Perempuan</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Pendidikan Tertinggi</label>
                  <select class="form-control" name="" required>
                    <option value="">--Pilih--</option>
                    <option value="">SLTA</option>
                    <option value="">D-III</option>
                    <option value="">D-IV/S1</option>
                    <option value="">S2</option>
                    <option value="">S3</option>
                  </select>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-lg-5">
                      <label>Jabatan Pemeriksa Merek</label>
                      <select class="form-control" name="" required>
                        <option value="">--Pilih--</option>
                        <option value="">Pemeriksa Merek P Lanjutan</option>
                        <option value="">Pemeriksa Merek Penyelia</option>
                        <option value="">Pemeriksa Merek Pertama</option>
                        <option value="">Pemeriksa Merek Muda</option>
                        <option value="">Pemeriksa Merek Madya</option>
                        <option value="">Pemeriksa Merek Utama</option>              
                      </select>
                    </div>
                    <div class="col-lg-3">
                      <label>Terhitung Mulai Tanggal</label>
                      <input type="date" name="" class="form-control" placeholder="" required>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label>Unit Kerja</label>
                  <input type="text" name="" class="form-control" placeholder="" required>
                </div>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </form>
          </div>
          <!-- /.card -->

          <!-- Form Element sizes -->

          <!-- /.card -->

          <!-- /.card -->

          <!-- /.card -->

          <!-- /.card -->

        </div>
        <!--/.col (left) -->

        <!--/.col (right) -->
      </div>
      <!-- /.row -->
     
  </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>