<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Form Data Unit Satuan Kerja</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
            <li class="breadcrumb-item active">Form Data Unit Satuan Kerja</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
             <a href="<?php echo base_url('eselon4/add'); ?>" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i> Add</a>
             <p></p>
             <thead>
              <tr>
                <th><center>No</center></th>
                <th><center>Satuan Kerja Eselon 1</center></th>
                <th><center>Satuan Kerja Eselon 2</center></th>
                <th><center>Satuan Kerja Eselon 3</center></th>
                <th><center>Satuan Kerja Eselon 4</center></th>                                
                <th><center>Action</center></th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach($eselon4 as $key => $data) { ?>
                <tr>
                  <td><center><?php echo $key+1; ?></center></td>
                  <td><center><?php echo $data['n_unitsatuankerja1']; ?></center></td>
                  <td><center><?php echo $data['n_unitsatuankerja2']; ?></center></td>
                  <td><center><?php echo $data['n_unitsatuankerja3']; ?></center></td>
                  <td><center><?php echo $data['n_unitsatuankerja4']; ?></center></td>                                    
                 <td>
                  <center>
                    <a href="<?php echo base_url('eselon4/edit/'.$data['ideselon4']); ?>" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>
                    <?php if(session()->get('level')=="Administrator"){ ?>
                      <a href="<?php echo base_url('eselon4/delete/'.$data['ideselon4']); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus <?php echo $data['n_unitsatuankerja4']; ?> ini?')"><i class="fas fa-trash-alt"></i></a>
                    <?php } ?>
                  </center>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th><center>No</center></th>
              <th><center>Satuan Kerja Eselon 1</center></th>
              <th><center>Satuan Kerja Eselon 2</center></th>
              <th><center>Satuan Kerja Eselon 3</center></th>              
              <th><center>Satuan Kerja Eselon 4</center></th>                            
              <th><center>Action</center></th>
            </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>