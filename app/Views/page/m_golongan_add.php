  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Tambah Golongan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/m_golongan">Form Data Golongan</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form action="<?= base_url() ?>/m_golongan/save" method="POST" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label>Data Golongan</label>
                    <select class="form-control" name="golongan" required>                      
                    <option>--Pilih--</option>
                    <option>III/a</option>
                    <option>III/b</option>
                    <option>III/c</option>
                    <option>III/d</option>
                    <option>IV/a</option>
                    <option>IV/b</option>
                    <option>IV/c</option>
                    <option>IV/d</option>
                    <option>IV/e</option>
                    </select>
                  </div>  
                  <div class="form-group">
                    <label>Data Jabatan</label>
                    <select class="form-control" name="idjabatan" required>                      
                    <option>--Pilih--</option>
                      <?php foreach($m_jabatan as $mj) { ?>
                        <option value="<?php echo $mj['idjabatan']; ?>" ><?php echo $mj['jabatan']; ?></option>
                      <?php } ?>
                    </select>
                  </div>                
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

            <!-- Form Element sizes -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

            <!-- /.card -->

          </div>
          <!--/.col (left) -->

          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>