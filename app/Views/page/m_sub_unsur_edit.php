  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Sub Unsur</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/m_sub_unsur">Form Data Master Sub Unsur</a></li>
              <li class="breadcrumb-item active">Ubah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Ubah Data Sub Unsur</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <section class="content">
              <div class="container-fluid">
                <div class="form-group">
                  <!-- left column -->
                  <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <!-- /.card-header -->
                      <!-- form start -->
                      <form action="<?= base_url() ?>/m_sub_unsur/update" method="POST" role="form">
                        <div class="card-body">
                        <div class="form-group">
                            <label>Nama Jenis Unsur</label>  
                            <input type="hidden" name="idsubunsur" value="<?php echo $m_sub_unsur['idsubunsur']; ?>" class="form-control" id="id">                         
                            <select class="form-control" name="idjenisunsur" required>
                              <option value="">--Pilih--</option>
                              <?php foreach($jenis_unsur as $ju) { ?>
                                <option value="<?php echo $ju['idjenisunsur']; ?>"><?php echo $ju['jenis']; ?></option>
                              <?php } ?>
                            </select>
                          </div>                         
                          <div class="form-group">
                            <label>Nama Unsur</label>
                            <select class="form-control" name="idunsur" required>
                              <option value="">--Pilih--</option>
                              <?php foreach($m_unsur as $mu) { ?>
                                <option value="<?php echo $mu['idunsur']; ?>"><?php echo $mu['unsur']; ?></option>
                              <?php } ?>
                            </select>
                          </div>                            
                          <div class="form-group">
                            <label>Nama Sub Unsur</label>
                            <input type="text" name="keterangan" value="<?php echo $m_sub_unsur['keterangan']; ?>" class="form-control" placeholder="Keterangan Sub Unsur" required="">
                          </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/.col (left) -->

                  <!--/.col (right) -->
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>