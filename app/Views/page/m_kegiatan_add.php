  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Kegiatan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/m_kegiatan">Form Data Master Kegiatan</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Master Kegiatan</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <section class="content">
              <div class="container-fluid">
                <div class="form-group">
                  <!-- left column -->
                  <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <!-- /.card-header -->
                      <!-- form start -->
                      <form action="<?= base_url() ?>/m_kegiatan/save" method="POST" role="form">
                        <div class="card-body">
                        <div class="form-group">
                            <label>Nama Jenis Unsur</label>
                            <select class="form-control" name="idjenisunsur" id="idjenisunsur" required onchange="aktifkan_bidang(this.value)">
                              <option value="">-- Pilih Jenis Unsur --</option>
                              <?php foreach($jenis_unsur as $ju) { ?>
                                <option value="<?php echo $ju['idjenisunsur']; ?>" ><?php echo $ju['jenis']; ?></option>
                              <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                          <label>Direktorat</label>
                          <select class="form-control" name="idbidang" id="idbidang" name="idbidang" disabled onchange="aktifkan_unsur(this.value)">
                              <option value="" selected disabled>-- Pilih Direktorat --</option>
                              <option value="1">Desain Industri</option>
                              <option value="2">Paten</option>
                              <option value="3">Merek</option>
                          </select>
                        </div>
                        <div class="form-group">
                            <label>Nama Unsur</label>
                            <select class="form-control" name="idunsur" id="idunsur" required disabled onclick="aktifkan_subunsur(this.value)">
                              <option value="">-- Pilih Unsur --</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Nama Sub Unsur</label>
                            <select class="form-control" name="idsubunsur" id="idsubunsur" disabled required>
                              <option value="">-- Pilih Sub Unsur --</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Nama Master Kegiatan</label>
                            <textarea name="masterkegiatan" id="masterkegiatan" class="form-control" placeholder="Master Kegiatan" required disabled> </textarea>
                          </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/.col (left) -->

                  <!--/.col (right) -->
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
function aktifkan_bidang(idjenisunsur){
  var idbidang = $('#idbidang')
  var idunsur = $('#idunsur')
  idunsur.prop('disabled', true)
  idunsur.val('')
  idbidang.val('')
  if(idjenisunsur != ""){
    idbidang.prop('disabled', false);
  }else{
    idbidang.prop('disabled', true);
  }
}

function aktifkan_unsur(idbidang){
  var idjenisunsur = $('#idjenisunsur').val()
  $('#idunsur').val('')

  if(idbidang !== ""){
    $('#idunsur').prop('disabled', false)
    $.ajax({
      url: '<?php echo base_url('M_sub_unsur/tampilkanunsur2') ?>',
      type: "POST",
      data: {
        validasi: 'go',
        idjenisunsur: idjenisunsur,
        idbidang: idbidang
      },
      success: function(hasil) {
        var obj = JSON.parse(hasil);
        $('#idunsur').empty()
        $('#idunsur').append('<option value="">-- Pilih Unsur --</option>')
        obj.forEach(x => {
          $('#idunsur').append('<option value="'+x.idunsur+'">'+x.unsur+'</option>')
        })
      },
      error: function(error) {
        alert("error");
      }
    })
  }else{
    $('#idunsur').prop('disabled', true)
  }
}

function aktifkan_subunsur(idunsur){
  $('#idsubunsur').val('')
  $('#masterkegiatan').val('')
  var idjenisunsur = $('#idjenisunsur').val()
  var idbidang = $('#idbidang').val()
  if(idunsur !== ""){
    $('#idsubunsur').prop('disabled', false)
    $('#masterkegiatan').prop('disabled', false)
    $.ajax({
      url: '<?php echo base_url('M_kegiatan/tampilkansubunsur2') ?>',
      type: "POST",
      data: {
        validasi: 'go',
        idunsur: idunsur,
        idjenisunsur: idjenisunsur,
        idbidang: idbidang
      },
      success: function(hasil) {
        var obj = JSON.parse(hasil);
        $('#idsubunsur').empty()
        $('#idsubunsur').append('<option value="">-- Pilih Sub Unsur --</option>')
        obj.forEach(x => {
          $('#idsubunsur').append('<option value="'+x.idsubunsur+'">'+x.keterangan+'</option>')
        })
      },
      error: function(error) {
        alert("error");
      }
    })
  }else{
    $('#idsubunsur').prop('disabled', true)
    $('#masterkegiatan').prop('disabled', true)
  }
}
</script>
