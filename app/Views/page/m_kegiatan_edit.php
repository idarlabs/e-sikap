  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Kegiatan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/m_kegiatan">Form Data Master Kegiatan</a></li>
              <li class="breadcrumb-item active">Ubah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Ubah Data Master Kegiatan</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <section class="content">
              <div class="container-fluid">
                <div class="form-group">
                  <!-- left column -->
                  <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <!-- /.card-header -->
                      <!-- form start -->
                      <form action="<?= base_url() ?>/m_kegiatan/update" method="POST" role="form">
                        <div class="card-body">
                        <div class="form-group">
                            <label>Nama Jenis Unsur</label>  
                            <input type="hidden" name="idmasterkegiatan" value="<?php echo $m_kegiatan['idmasterkegiatan']; ?>" class="form-control" id="id">                         
                            <select class="form-control" name="idjenisunsur" required>
                              <option value="">--Pilih--</option>
                              <?php foreach($jenis_unsur as $ju) { ?>
                                <option value="<?php echo $ju['idjenisunsur']; ?>"><?php echo $ju['jenis']; ?></option>
                              <?php } ?>
                            </select>
                          </div> 
                          <div class="form-group">
                            <label>Nama Unsur</label>                           
                            <select class="form-control" name="idunsur" required>
                              <option value="">--Pilih--</option>
                              <?php foreach($m_unsur as $mu) { ?>
                                <option value="<?php echo $mu['idunsur']; ?>"><?php echo $mu['unsur']; ?></option>
                              <?php } ?>
                            </select>
                          </div>                                                   
                          <div class="form-group">
                            <label>Nama Sub Unsur</label>                         
                            <select class="form-control" name="idsubunsur" required>
                              <option value="">--Pilih--</option>
                              <?php foreach($m_sub_unsur as $su) { ?>
                                <option value="<?php echo $su['idsubunsur']; ?>"><?php echo $su['keterangan']; ?></option>
                              <?php } ?>
                            </select>
                          </div>                            
                          <div class="form-group">
                            <label>Nama Master Kegiatan</label>
                            <textarea name="masterkegiatan" value="<?php echo $m_kegiatan['masterkegiatan']; ?>" class="form-control" placeholder="Master Kegiatan" required="" ></textarea>
                          </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/.col (left) -->

                  <!--/.col (right) -->
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>