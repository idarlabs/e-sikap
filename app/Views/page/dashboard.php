  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">

          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>


          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Apa itu E-SIKAP ?</strong></h5>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-left">
                      E-SIKAP adalah elektronik Sistem Informasi Kinerja Pemeriksa yang berfungsi sebagai sarana untuk penilaian kreditbilitas dokumen
                    </p>


                    <!-- /.chart-responsive -->
                  </div>
                  <div class="col-md-4">
                    
                    <!-- /.col -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.row -->
                </div>
                <!-- ./card-body -->

                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
          <div class="col-md-5">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Informasi Jumlah Data Pemohon</strong></h5>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-left">
                      <div class="table-responsive">
                        <table class="table m-0">
                          <thead>
                            <tr>
                              <th>NIP</th>
                              <th>Periode</th>
                              <th>Jumlah</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>NAME</a></td>
                              <td>YEAR</td>
                              <td>999</td>
                            </tr>
                            <tr>
                              <td>NAME</a></td>
                              <td>YEAR</td>
                              <td>999</td>
                            </tr>
                            <tr>
                              <td>NAME</a></td>
                              <td>YEAR</td>
                              <td>999</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </p>


                    <!-- /.chart-responsive -->
                  </div>
                  <div class="col-md-4">
                    
                    <!-- /.col -->
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.row -->
                </div>
                <!-- ./card-body -->

                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <div class="col-md-7">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Informasi Jumlah Dokumen Berdasarkan Kategori</strong></h5>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-left">
                     <div class="table-responsive">
                      <table class="table m-0">
                        <thead>
                          <tr>
                            <th>Jenis Dokumen</th>
                            <th>Jumlah Data</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>DocType</a></td>
                            <td>999</td>
                          </tr>
                          <tr>
                            <td>DocType</a></td>
                            <td>999</td>
                          </tr>
                          <tr>
                            <td>DocType</a></td>
                            <td>999</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </p>


                  <!-- /.chart-responsive -->
                </div>
                <div class="col-md-4">
                  
                  <!-- /.col -->
                </div>
                <!-- /.col -->
                
                <!-- /.row -->
              </div>
              <!-- ./card-body -->

              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Main row -->
        <div class="row">
          <!-- Left col -->
          <div class="col-md-5">

            <!-- TABLE: LATEST ORDERS -->

            <div class="col-md-7">
              <!-- Info Boxes Style 2 -->
              
            <!-- /.info-box 
              add menu div -->
              
              <!-- /.card -->

            <!-- PRODUCT LIST 
              add menu div-->
              
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div><!--/. container-fluid -->
      </section>
      <!-- /.content -->
    </div>