  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Sub Unsur</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/m_sub_unsur">Form Data Master Sub Unsur</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Sub Unsur</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <section class="content">
              <div class="container-fluid">
                <div class="form-group">
                  <!-- left column -->
                  <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <!-- /.card-header -->
                      <!-- form start -->
                      <form action="<?= base_url() ?>/m_sub_unsur/save" method="POST" role="form">
                        <div class="card-body">
                        <div class="form-group">
                            <label>Nama Jenis Unsur</label>
                            <select class="form-control" id="idjenisunsur" name="idjenisunsur" required onclick="aktifkan_bidang(this.value)">
                              <option value="" selected disabled>-- Pilih Jenis Unsur --</option>
                              <?php foreach($jenis_unsur as $ju) { ?>
                                <option value="<?php echo $ju['idjenisunsur']; ?>" ><?php echo $ju['jenis']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Direktorat</label>
                            <select class="form-control" name="idbidang" id="idbidang" name="idbidang" disabled onchange="aktifkan_unsur(this.value)">
                                <option value="" selected disabled>-- Pilih Direktorat --</option>
                                <option value="1">Desain Industri</option>
                                <option value="2">Paten</option>
                                <option value="3">Merek</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Nama Unsur</label>
                            <select class="form-control" name="idunsur" required disabled id="idunsur">
                              <option value="">-- Pilih Unsur --</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Nama Sub Unsur</label>
                            <input type="text" name="keterangan" class="form-control" placeholder="Keterangan" required="">
                          </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/.col (left) -->

                  <!--/.col (right) -->
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    function aktifkan_bidang(idjenisunsur){
      var idbidang = $('#idbidang')
      var idunsur = $('#idunsur')
      idunsur.prop('disabled', true)
      idunsur.val('')
      idbidang.val('')
      if(idjenisunsur != ""){
        idbidang.prop('disabled', false);
      }else{
        idbidang.prop('disabled', true);
      }
    }

    function aktifkan_unsur(idbidang){
      var idjenisunsur = $('#idjenisunsur').val()
      $('#idunsur').val('')

      if(idbidang !== ""){
        $('#idunsur').prop('disabled', false)
        $.ajax({
    			url: '<?php echo base_url('M_sub_unsur/tampilkanunsur2') ?>',
    			type: "POST",
    			data: {
    				validasi: 'go',
    				idjenisunsur: idjenisunsur,
    				idbidang: idbidang
    			},
    			success: function(hasil) {
            var obj = JSON.parse(hasil);
            $('#idunsur').empty()
            $('#idunsur').append('<option value="">-- Pilih Unsur --</option>')
            obj.forEach(x => {
              $('#idunsur').append('<option value="'+x.idunsur+'">'+x.unsur+'</option>')
            })
    			},
    			error: function(error) {
    				alert("error");
    			}
    		})
      }else{
        $('#idunsur').prop('disabled', true)
      }
    }
  </script>
