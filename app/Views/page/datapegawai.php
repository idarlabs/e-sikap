  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Pegawai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Tambah Data Pegawai </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 

 <div class="row">
        <div class="col-md-12">
         <section class="content">
          <div class="row">
            <div class="col-12">
              <!-- /.card -->

              <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                  <table id="example1" class="table table-bordered table-striped">
                   <a href="<?= base_url() ?> /add/add_datapegawai" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</a>
                      <p></p>
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pangkat & Golongan Ruang</th>
                        <th>Mulai Tanggal</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>File Foto</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>

                      <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                          <div class="btn-group">
                            <a href="" class="btn btn-primary btn-sm"><i class="fas fa-share"></i></a>&nbsp;
                            <a href="" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></a>&nbsp;
                            <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin data ini?')"><i class="fas fa-trash-alt"></i></a>
                          </div>
                        </td>
                      </tr>

                      </tr>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIP</th>
                        <th>Pangkat & Golongan Ruang</th>
                        <th>Mulai Tanggal</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>File Foto</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </section>
      </div>
    </div>