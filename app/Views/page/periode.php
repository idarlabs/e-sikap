<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Form Data Periode</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
            <li class="breadcrumb-item active">Form Data Periode</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- /.card -->

        <div class="card">
          <!-- /.card-header -->
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
             <a href="<?php echo base_url('periode/add'); ?>" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah</a>
             <p></p>
             <thead>
              <tr>
                <th><center>Kode</center></th>
                <th><center>Tanggal Buka</center></th>
                <th><center>Tanggal tutup</center></th>                                
                <th><center>Action</center></th>
              </tr>
            </thead>
            <tbody>
              <?php 
              if(count($bukamenu) > 0){
              foreach($bukamenu as $key => $data) { ?>
                <tr>
                  <td><center><?php echo $data['kode']; ?></center></td>
                  <td><center><?php echo $data['tgl_buka']; ?></center></td>
                  <td><center><?php echo $data['tgl_tutup']; ?></center></td>
                  <td>
                    <center>
                      <a onclick="tutup()" href="#" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>
                    </center>
                  </td>
                </tr>
              <?php }} ?>
            </tbody>
            <tfoot>
              <tr>
                <th><center>Kode</center></th>
                <th><center>Tanggal Buka</center></th>
                <th><center>Tanggal tutup</center></th>                                
                <th><center>Action</center></th>
              </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
</div>
<script>
  <?php
  helper('ajax');
  echo ajaxCallback();
  ?>
  function tutup(){
    if (confirm('Apakah Anda yakin akan menutup periode?')) {
      let uri = "<?= base_url('periode/tutup');?>";
      let dataJson = {
          submit: true,
      };
      ajaxCallback(function(data){
          if (data.code == 1) {
            location.reload();
          } else {
            alert('Terjadi kesalahan. Hubungi Administrator');
          }
      },uri,dataJson);
    }
  }
</script>