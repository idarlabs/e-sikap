  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Form Data Master Unsur</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/jenis_unsur">Form Data Master Unsur</a></li>
              <li class="breadcrumb-item active">Tambah</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <div class="form-group">
      <!-- left column -->
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Unsur</h3>
          </div>
          <!-- /.card-header -->
          <!-- form start -->
          <div class="card-body">
            <section class="content">
              <div class="container-fluid">
                <div class="form-group">
                  <!-- left column -->
                  <div class="col-md-6">
                    <!-- general form elements -->
                    <div class="card card-primary">
                      <!-- /.card-header -->
                      <!-- form start -->
                      <form action="<?= base_url() ?>/m_unsur/save" method="POST" role="form">
                        <div class="card-body">
                          <div class="form-group">
                            <label>Nama Jenis Unsur</label>
                            <select class="form-control" name="idjenisunsur" required>
                              <?php foreach($jenis_unsur as $ju) { ?>
                                <option value="<?php echo $ju['idjenisunsur']; ?>" ><?php echo $ju['jenis']; ?></option>
                              <?php } ?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Direktorat</label>
                            <select class="form-control" name="idbidang" id="idbidang">
                                <option value="1">Desain Industri</option>
                                <option value="2">Paten</option>
                                <option value="3">Merek</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Kategori Penilaian</label>
                            <select class="form-control" name="idkategori" id="idkategori">
                                <option value="" selected disabled>-- Pilih Kategori --</option>
                                <option value="1">Pendidikan</option>
                                <option value="2">Pemeriksaan</option>
                                <option value="3">Pengembangan Profesi</option>
                                <option value="4">Pendukung Kegiatan</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Nama Unsur</label>
                            <input type="text" name="unsur" class="form-control" placeholder="Nama Unsur" required="">
                          </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                          <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!--/.col (left) -->

                  <!--/.col (right) -->
                </div>
                <!-- /.row -->
              </div><!-- /.container-fluid -->
            </section>
          </div>
        </div>
      </div>
    </div>
  </div>
