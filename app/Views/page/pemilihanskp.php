<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pemilihan Periode SKP</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?= base_url() ?>/home">Home</a></li>
              <li class="breadcrumb-item active">Pemilihan Periode SKP</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <!-- /.card -->

          <div class="card">
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">

                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Pangkat dan Golongan Ruang</th>
                  <th>Periode</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
       
                <tr>
                  <td>1</td>
                        <td>HENDRA SULAKSANA, SH</td>
                        <td>Pembina Utama Muda (IV/c)</td>
                        <td>1 Okt 2018</td>
                        <td>
                            <div class="btn-group">
                                <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>&nbsp;
                                <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin data ini?')"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </td>
                </tr>
                <tr>
                  <td>2</td>
                        <td>MOH. ZAKY, SH</td>
                        <td>Pembina Utama Muda (IV/c)</td>
                        <td>1 April 2019</td>
                        <td>
                            <div class="btn-group">
                                <a href="" class="btn btn-primary btn-sm"><i class="fas fa-edit"></i></a>&nbsp;
                                <a href="" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin data ini?')"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </td>
                </tr>
       
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Periode</th>
                  <th>Jenis</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>