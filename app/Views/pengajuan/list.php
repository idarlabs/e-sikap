<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten'); ?>
<div class="card">
    <div class="card-body">
      <div class="col-sm-12">
          <table id="example1" class="table table-bordered table-striped teks15">
              <thead>
                  <tr>
                      <th class="tengah">No</th>
                      <th>Nama Pengaju</th>
                      <th class="tengah">Jabatan Saat Ini</th>
                      <th class="tengah">Golongan / Ruang</th>
                      <th class="tengah" width="15%">Detail</th>
                  </tr>
              </thead>
              <tbody>
                <?php
                if(count($datapengajuan) > 0){
                  $no = 1;
                  foreach($datapengajuan as $dp):
                ?>
                <tr>
                  <td class="tengah"><?=$no;?></td>
                  <td><?=$dp->nama;?></td>
                  <td class="tengah"><?=$dp->jabatan;?></td>
                  <td class="tengah"><?=$dp->golongan;?></td>
                  <td class="tengah">
                    <a href="<?=base_url();?>/pengajuan/detail/<?=$dp->idpengajuan;?>" class="btn btn-sm btn-info">Detail</a>
                  </td>
                </tr>
                <?php
                  $no++;
                  endforeach;
                }
                ?>
              </tbody>
          </table>
      </div>
    </div>
</div>
<?php echo $this->endSection(); ?>
