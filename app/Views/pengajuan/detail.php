<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten'); ?>
<div class="card">
    <div class="card-body">
        <div class="col-sm-12">
          <a href="<?=base_url();?>/pengajuan" class="btn btn-sm btn-info"><i class="fas fa-arrow-left"></i> Kembali</a>
          <!-- <button class="btn btn-sm btn-success" id="pdf"><i class="fa fa-book"></i> Generate PDF</button> -->
          <p></p>
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Kegiatan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Angka Kredit</a>
            </li>
            <li class="nav-item" style="display:none;" id="itemakhir">
              <a class="nav-link" id="pengesahan-tab" data-toggle="tab" href="#pengesahan" role="tab" aria-controls="pengesahan" aria-selected="false">SK. Kenaikan Pangkat</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent" style="padding:20px;">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <table id="example1" class="table table-bordered table-striped teks15">
                  <thead>
                      <tr>
                          <th class="tengah">No</th>
                          <th>Kegiatan</th>
                          <th>Butir</th>
                          <th>Doc</th>
                          <th class="tengah" width="15%">Aksi</th>
                      </tr>
                  </thead>
                  <tbody>
                      <?php foreach ($pengajuan as $key => $item) : ?>
                          <tr>
                              <td class="tengah"><?= $key+1 ?></td>
                              <td><?= $item['masterkegiatan'] ?></td>
                              <td><?= $item['butirkegiatan'] ?></td>
                              <td><a href="<?= base_url() . "/doc/" . $item['path']; ?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-alt"></i></a></td>
                              <td class="tengah">
                                      <a class="btn btn-sm btn-success" onclick="nilai('<?= $item['idriwayatpengajuan'] ?>')">Terima</a>
                                      <a href="#" class="btn btn-sm btn-warning" onclick="inputketerangan('<?= $item['idriwayatpengajuan'] ?>')">Tolak</a>
                              </td>
                          </tr>
                      <?php endforeach ?>
                  </tbody>
              </table>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
              <table class="table table-bordered teks15">
                <tr>
                  <th colspan="2">PENETAPAN ANGKA KREDIT</th>
                  <th class="tengah">LAMA</th>
                  <th class="tengah">BARU</th>
                  <th class="tengah">JUMLAH</th>
                </tr>
                <tr>
                  <td class="tengah">1</td>
                  <th colspan="4">UNSUR UTAMA</th>
                </tr>
                <tr>
                  <td></td>
                  <td>a. (1) Pendidikan Formal dan mencapai gelar / ijasah</td>
                  <td class="tengah"><?=$pen1;?></td>
                  <td class="tengah"><?=$pendidikan_1;?></td>
                  <td class="tengah"><?=($pen1+$pendidikan_1);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat surat Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
                  <td class="tengah"><?=$pen2;?></td>
                  <td class="tengah"><?=$pendidikan_2;?></td>
                  <td class="tengah"><?=($pen2+$pendidikan_2);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;(3) Diklat Prajabatan</td>
                  <td class="tengah"><?=$pen3;?></td>
                  <td class="tengah"><?=$pendidikan_3;?></td>
                  <td class="tengah"><?=($pen3+$pendidikan_3);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>b. Pemeriksaan</td>
                  <td class="tengah"><?=$pem;?></td>
                  <td class="tengah"><?=$pemeriksaan;?></td>
                  <td class="tengah"><?=($pem+$pemeriksaan);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>c. Pengembangan Profesi</td>
                  <td class="tengah"><?=$peng;?></td>
                  <td class="tengah"><?=$pengembangan;?></td>
                  <td class="tengah"><?=($peng+$pengembangan);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR UTAMA</th>
                  <th class="tengah"><?=$jmlunsurutama2;?></th>
                  <th class="tengah"><?=$jmlunsurutama;?></th>
                  <th class="tengah"><?=($jmlunsurutama2+$jmlunsurutama);?></th>
                </tr>
                <tr>
                  <td class="tengah">2</td>
                  <th colspan="4">UNSUR PENUNJANG</th>
                </tr>
                <tr>
                  <td></td>
                  <td>Pendukung Kegiatan Pemeriksaan</td>
                  <td class="tengah"><?=$pen;?></td>
                  <td class="tengah"><?=$penunjang;?></td>
                  <td class="tengah"><?=($pen+$penunjang);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR PENUNJANG</th>
                  <th class="tengah"><?=$pen;?></th>
                  <th class="tengah"><?=$penunjang;?></th>
                  <td class="tengah"><?=($pen+$penunjang);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR UTAMA DAN PENUNJANG</th>
                  <th class="tengah"><?=$jmlkeduanya2;?></th>
                  <th class="tengah"><?=$jmlkeduanya;?></th>
                  <td class="tengah text-danger"><?=($jmlkeduanya2+$jmlkeduanya);?></td>
                </tr>
              </table>
              <?php
              $totalsemua = $jmlkeduanya2+$jmlkeduanya;
              $msg_terakhir = "";
              $itemakhir = 0;
              if($totalsemua >= $pengangkatan[0]->minimum){
                $itemakhir = 1;
                $msg_terakhir = "Dapat dipertimbangkan untuk dinaikkan dalam jabatan Pemeriksa ".$datapengaju->jenispemeriksa." ".$datapengaju->jabatan;
              }else{
                $itemakhir = 0;
                $msg_terakhir = "Belum dapat dipertimbangkan untuk dinaikkan dalam pangkat / jabatan.";
              }
              ?>
              <span><span class="text-danger"><strong>*</strong></span><?=$msg_terakhir;?></span>
            </div>
            <div class="tab-pane fade" id="pengesahan" role="tabpanel" aria-labelledby="pengesahan-tab">
              <div class="row">
                <div class="col-sm-12">
                  <label for="dinaikkan">Dinaikkan</label>
                  <input type="text" readonly id="dinaikkan" class="form-control" value="Dalam Pangkat <?=$pengangkatan[0]->pangkat.", Golongan/Ruang - ".$pengangkatan[0]->golongan;?>">
                </div>
                <div class="col-sm-12"><hr></div>
                <div class="col-sm-12">
                  <label for="nosk">Nomor SK</label>
                  <input type="text" id="nosk" class="form-control" placeholder="Masukkan Nomor SK">
                </div>
                <div class="col-sm-6">
                  <label for="tmt">Terhitung Mulai Tanggal</label>
                  <input type="date" id="tmt" class="form-control">
                </div>
                <div class="col-sm-6">
                  <label for="fileUpload">Upload SK</label>
                  <input type="file" class="form-control" name="fileUpload" id="fileUpload" placeholder="Upload SK">
                </div>
                <div class="col-sm-12 pt-2">
                <div class="col-sm-12 pt-2">
                  <button onclick="submitKenaikanPangkat()" type="button" id="update" class="btn btn-primary btn-block">Submit</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalketerangan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <h4>Alasan Ditolak.</h4>
        <input type="hidden" id="idriw">
      <label for="isiketerangan"></label>
      <textarea id="isiketerangan" rows="8" cols="80" class="form-control"></textarea>
      <button type="button" name="button" class="btn btn-primary btn-block" onclick="tolak()">Kirim</button>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
  var base = "<?=base_url();?>";
  var id = "<?=$id;?>";
  var nipnya = "<?=$datapengaju->nip;?>";
  var periodenya = "<?=$datapengaju->idperiode;?>";
  $('#pdf').on('click', function(){
    window.open(base+'/pengajuan/exportPDF/'+id+'/'+nipnya+'/'+periodenya, '_blank');
  })

  var itemakhir = "<?=$itemakhir;?>";
  if(itemakhir == '1'){
    // $('#itemakhir').show()
  }else{
    // $('#itemakhir').hide()
  }

  function inputketerangan(idriwayatpengajuan){
    $('#modalketerangan').modal('show')
    $('#idriw').val(idriwayatpengajuan)
  }

  function tolak(){
    var idriwayatpengajuan = $('#idriw').val();
    var keterangan = $('#isiketerangan').val()
    if(keterangan != ""){
      $.ajax({
        url: '<?php echo base_url('Pengajuan/tolak') ?>',
        type: "POST",
        data: {
          validasi: 'go',
          idriwayatpengajuan: idriwayatpengajuan,
          keterangan: keterangan,
        },
        success: function(hasil) {
          location.reload()
        },
        error: function(error) {
          alert("error");
          console.error(error);
        }
      })
    }else{
      alert('Anda belum mengisi keterangan.')
    }
  }

  function nilai(idriwayatpengajuan){
    if(confirm('Anda yakin akan menerima kegiatan ini?')){
      $.ajax({
        url: '<?php echo base_url('Pengajuan/proses') ?>',
        type: "POST",
        data: {
          validasi: 'go',
          idriwayatpengajuan: idriwayatpengajuan
        },
        success: function(hasil) {
          location.reload()
        },
        error: function(error) {
          alert("error");
          console.error(error);
        }
      })
    }
  }

  function submitKenaikanPangkat(){
    let uri = "<?= base_url('pengajuan/submit');?>";
    let currentUri = $(location).attr("href");
    let idPengajuan = currentUri.split('/').pop();
    let fd = new FormData();
    fd.append('submit',true);
    fd.append('sk',$('#nosk').val());
    fd.append('tmt',$('#tmt').val());
    fd.append('idpengajuan',idPengajuan);
    fd.append('nip',"<?=$datapengaju->nip;?>");
    fd.append('idgolongan',"<?=$pengangkatan[0]->idgolongan;?>");
    fd.append('idpangkat',"<?=$pengangkatan[0]->idpangkat;?>");
    let file = $('#fileUpload').prop('files')[0];
    if (file) {
      fd.append('file',file)
    }
    fd.append('pendidikan_1',"<?=$pendidikan_1;?>");
    fd.append('pendidikan_2',"<?=$pendidikan_2;?>");
    fd.append('pendidikan_3',"<?=$pendidikan_3;?>");
    fd.append('pemeriksaan',"<?=$pemeriksaan;?>");
    fd.append('pengembangan',"<?=$pengembangan;?>");
    fd.append('penunjang',"<?=$penunjang;?>");
    fd.append('pen1',"<?=$pen1;?>");
    fd.append('pen2',"<?=$pen2;?>");
    fd.append('pen3',"<?=$pen3;?>");
    fd.append('pem',"<?=$pem;?>");
    fd.append('peng',"<?=$peng;?>");
    fd.append('pen',"<?=$pen;?>");
    fd.append('idpengajuan',"<?=$id;?>");
    fd.append('idperiode',"<?=$idperiode;?>");
    ajaxCallbackFormData(function(data){
        if (data.code == 1) {
          // alert('Berhasil');
          // location.reload();
          location.href="<?= base_url('pengajuan');?>"
        } else {
          alert('Gagal: ' + data.message.text);
        }
    },uri,fd);
  }
</script>
<?php echo $this->endSection(); ?>
