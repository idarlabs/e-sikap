<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>
<div class="card">
  <div class="card-body">
    <div class="col-sm-12">
      <table id="example1" class="table table-bordered table-striped teks15">
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th>NIP</th>
            <th>No. Karpeg</th>
            <th>Nama</th>
            <th>Pangkat / Golongan</th>
            <th>TMT</th>
            <th>TTL</th>
            <th>Jenis Kelamin</th>
            <th>Pendidikan Terakhir</th>
            <th>Alamat</th>
            <th>Telepon</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach($pegawai as $peg): ?>
            <tr>
              <td class="tengah"><?=$no;?></td>
              <td><?=$peg->nip;?></td>
              <td><?=$peg->no_karpeg;?></td>
              <td><?=$peg->nama;?></td>
              <td><?=$peg->pangkat." / ".$peg->golongan;?></td>
              <td><?=$peg->tmt_pegawai;?></td>
              <td><?=$peg->tempat_lahir;?></td>
              <td><?=$peg->jekel;?></td>
              <td><?=$peg->pendidikan;?></td>
              <td><?=$peg->alamat;?></td>
              <td><?=$peg->telepon;?></td>
              <td><?=$peg->email;?></td>
            </tr>
          <?php $no++; endforeach;?>
        </tbody>
    </table>
    </div>
  </div>
</div>
<?php echo $this->endSection();?>
