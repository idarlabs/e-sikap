<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>

<div class="card">
  <div class="card-body">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <table id="example1" class="table table-bordered table-striped teks15">
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th>Kegiatan</th>
            <th>Butir</th>
            <th>Doc</th>
            <th class="tengah">#</th>
          </tr>
        </thead><i class="fa file-alt"></i>
        <tbody>
          
        </tbody>
      </table>
    </div>
  </div>
</div>

<?php echo $this->endSection();?>
