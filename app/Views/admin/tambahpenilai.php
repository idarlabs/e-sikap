<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten'); ?>

<div class="card">
  <div class="card-body">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
      <button type="button" name="tambah" class="btn btn-primary" data-toggle="modal" data-target="#modaltambah">Tambah Penilai</button>
      <p></p>
      <table id="example1" class="table table-bordered table-striped teks15">
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th>Nama Pejabat</th>
            <th class="tengah">Golongan</th>
            <th>Jabatan</th>
            <th class="tengah">Hapus</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1; foreach($penilai as $p):?>
            <tr>
              <td class="tengah"><?=$no;?></td>
              <td><?=$p->nama;?></td>
              <td class="tengah"><?=$p->golongan;?></td>
              <td><?=$p->jabatan;?></td>
              <td class="tengah">
                <button type="button" class="btn btn-sm btn-danger" name="button" onclick="hapus('<?=$p->idpenilai;?>', '<?=$p->nama;?>')">
                  <i class="fas fa-trash-alt"></i>
                </button>
              </td>
            </tr>
          <?php $no++; endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<!-- pilih penilai -->
<div class="modal fade" id="modaltambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Penilai</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-12">
            <select class="form-control" name="pilihbidang" id="pilihbidang">
              <option value="">-- Pilih Bidang --</option>
              <?php foreach ($bidangnya as $b) : ?>
                <option value="<?= $b->idjp; ?>"><?= $b->jenispemeriksa; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="col-12">
            <select class="form-control" name="piliheselon" id="piliheselon" disabled>
              <option value="">-- Pilih Eselon --</option>
              <option value="1">Eselon 1</option>
              <option value="2">Eselon 2</option>
            </select>
          </div>
          <div class="col-12">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
              <table id="example1" class="table table-bordered table-striped teks15">
                <thead>
                  <tr>
                    <th class="tengah">No</th>
                    <th>Nama</th>
                    <th>Gol / Ruang</th>
                    <th class="tengah">#</th>
                  </tr>
                </thead>
                <tbody id="listpenilai"></tbody>
              </table>
            </div>
          </div>
          <div class="col-12 kanan">
            <button type="submit" name="button" data-dismiss="modal" class="btn btn-primary" id="tambah-penilai" style="display:none;">Tambah Penilai</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="datanya">

<script type="text/javascript">
  function hapus(idpenilai, nama){
    if(confirm('Anda yakin akan menghapus penilai '+nama+'?')){
      $.ajax({
        url: '<?php echo base_url('tambahpenilai/hapus') ?>',
        type: "POST",
        data: {
          validasi: 'go',
          id: idpenilai
        },
        success: function(hasil) {
          location.reload()
        },
        error: function(error) {
          alert("error");
        }
      });
    }
  }
  $("#pilihbidang").change(function() {
    var id = $(this).val();
    $('#piliheselon').val("")
    $('#listpenilai').empty()
    $('#tambah-penilai').hide()
    $('#datanya').val('')
    if (id !== "") {
      $('#piliheselon').prop('disabled', false)
    } else {
      $('#piliheselon').prop('disabled', true)
    }
  });

  $("#piliheselon").change(function() {
    var id = $(this).val();
    $('#listpenilai').empty()
    $.ajax({
      url: '<?php echo base_url('tambahpenilai/ambileselon') ?>',
      type: "POST",
      data: {
        validasi: 'go',
        idbidang: $('#pilihbidang').val()
      },
      success: function(hasil) {
        var obj = JSON.parse(hasil);
        var no1 = 1;
        $('#listpenilai').empty()
        obj.forEach(data => {
          if (id !== "") {
            if (id == "1") {
              if (data.idgolongan == "8") {
                var baris = $('<tr data-id="' + data.idpegawai + '"></tr>')
                baris.append('<td>' + no1 + '</td>')
                baris.append('<td>' + data.nama + '</td>')
                baris.append('<td>' + data.golongan + '</td>')
                baris.append('<td class="text-center"><input type="radio" id="radiopegawai" name="pegawai" onclick="setData(\''+data.nip+'\', \''+id+'\', \''+$('#pilihbidang').val()+'\')"></td>')
                $('#listpenilai').append(baris)
                no1++;
              }
            } else {
              if (data.idgolongan == "5") {
                console.log('panik nggak?');
                var baris = $('<tr></tr>')
                baris.append('<td>' + no1 + '</td>')
                baris.append('<td>' + data.nama + '</td>')
                baris.append('<td>' + data.golongan + '</td>')
                baris.append('<td class="text-center"><input type="radio" id="radiopegawai" name="pegawai" onclick="setData(\''+data.nip+'\', \''+id+'\', \''+$('#pilihbidang').val()+'\')"></td>')
                $('#listpenilai').append(baris)
                no1++;
              }
            }
          }
        })
      },
      error: function(error) {
        alert("error");
      }
    });
  });

  function setData(nip, ideselon, idbidang){
    $('#tambah-penilai').show()
    $('#datanya').val(nip+'|'+ideselon+'|'+idbidang)
  }

  $('#tambah-penilai').on('click', function(){
    if(confirm('Anda yakin akan menambah penilai baru?')){
      var datanya = $('#datanya').val().split("|")
      $.ajax({
        url: '<?php echo base_url('tambahpenilai/proses') ?>',
        type: "POST",
        data: {
          validasi: 'go',
          nip: datanya[0],
          ideselon: datanya[1],
          idbidang: datanya[2]
        },
        success: function(hasil) {
          console.log(hasil)
          if(hasil == "oke"){
            location.reload()
          }
        },
        error: function(error) {
          alert("error");
        }
      });
    }else{
      location.reload()
    }
  })
</script>

<?php echo $this->endSection(); ?>
