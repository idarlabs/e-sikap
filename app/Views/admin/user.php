<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>

<div class="card">
  <div class="card-body">
    <div class="col-sm-12">
      <table id="example1" class="table table-bordered table-striped">
        <button type="button" data-toggle="modal" data-target="#pilihform" class="btn btn-primary"><i class="fas fa-plus fa-lg mr-2"></i>Tambah User</button>
        <p></p>
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th class="tengah">NIP</th>
            <th class="tengah">Username</th>
            <th class="tengah">Level</th>
            <th class="tengah">Status</th>
            <th class="tengah">#</th>
          </tr>
        </thead>
        <tbody>
          <?php $nomor = 1; foreach($user as $usr): ?>
            <tr>
              <td class="tengah"><?=$nomor;?></td>
              <td class="tengah"><?=$usr->nip;?></td>
              <td class="tengah"><?=$usr->username;?></td>
              <td class="tengah"><?=$usr->level;?></td>
              <td class="tengah"><?=($usr->status == 1) ? "Aktif" : "Tidak Aktif";?></td>
              <td class="tengah">#</td>
            </tr>
          <?php $nomor++; endforeach;?>
        </tbody>
        <tfooter>
          <tr>
            <th class="tengah">No</th>
            <th class="tengah">NIP</th>
            <th class="tengah">Username</th>
            <th class="tengah">Level</th>
            <th class="tengah">Status</th>
            <th class="tengah">#</th>
          </tr>
        </tfooter>
    </table>
    </div>
  </div>
</div>

<!-- pilih form -->
<div class="modal fade" id="pilihform" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Pilih Level</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <div class="row">
            <div class="col-6 tengah">
              <a href="#" class="btn btn-danger btn-block" data-dismiss="modal" onclick="pilihlevel('addModal1')">
                <i class="fas fa-user-shield fa-lg"></i><br>
                <strong>Administrator</strong>
              </a>
            </div>
            <div class="col-6 tengah">
              <a href="#" class="btn btn-primary btn-block" data-dismiss="modal" onclick="pilihlevel('addModal2')">
                <i class="fas fa-user-tie fa-lg"></i><br>
                <strong>Pejabat Fungsional</strong>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>

<!-- form tambah administrator -->
<form action="<?= base_url() ?>/user/tambah" method="POST" role="form">
  <div class="modal fade" id="addModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Tambah User Administrator</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <input type="text" name="nip1" id="nip1" class="form-control" placeholder="Nomor Induk Pegawai">
            <input type="text" name="username1" id="username1" class="form-control" placeholder="Username">
            <input type="password" name="password1" id="password1" class="form-control" placeholder="Password">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger">Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<!-- form tambah pejabat fungsional -->
<div class="modal fade" id="addModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah User Pejabat Fungsional</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <input type="text" name="nip2" id="nip2" class="form-control" placeholder="Nomor Induk Pegawai">
          <input type="text" name="username2" id="username2" class="form-control" placeholder="Username">
          <input type="password" name="password2" id="password2" class="form-control" placeholder="Password">
          <hr>
          <strong>Data Pejabat Fungsional</strong><br>
          <label><small>Nomor Kartu Pegawapasswordi</small></label>
          <input type="text" name="no_karpeg" id="no_karpeg" class="form-control" placeholder="Nomor Kartu Pegawai">
          <label><small>Nama Lengkap</small></label>
          <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Lengkap">
          <!-- <select class="form-control" id="idpangkat" name="idpangkat">

          </select> -->
          <select class="form-control" id="idgolongan" name="idgolongan">

          </select>
          <label><small>Terhitung Mulai Tanggal</small></label>
          <input type="date" name="tmt" id="tmt" class="form-control" placeholder="Terhitung Mulai Tanggal">
          <label><small>Tempat Lahir</small></label>
          <input type="text" name="tempat_lahir" id="tempat_lahir" class="form-control" placeholder="Tempat Lahir">
          <label><small>Tanggal Lahir</small></label>
          <input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="Tanggal Lahir">
          <select class="form-control" id="jk" name="jk">
            <option value="" selected disabled>-- Pilih Jenis Kelamin --</option>
            <option value="1">Laki-laki</option>
            <option value="2">Perempuan</option>
          </select>
          <select class="form-control" id="idpendidikan" name="idpendidikan">

          </select>
          <label><small>Alamat</small></label>
          <textarea name="alamat" id="alamat" rows="8" cols="80" class="form-control" placeholder="Alamat"></textarea>
          <label><small>Nomor Telepon</small></label>
          <input type="number" name="telepon" id="telepon" class="form-control" placeholder="Nomor Telepon">
          <label><small>Email Pegawai</small></label>
          <input type="mail" name="email" id="email" class="form-control" placeholder="Email">
          <select class="form-control" name="pilibidang" id="pilihbidang">
            <option value="">-- Pilih Bidang --</option>
            <option value="1">Desain Industri</option>
            <option value="2">Paten</option>
            <option value="3">Merek</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button onclick="addUser()" type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  // loadPangkat();
  loadGolongan();
  loadPendidikan();
});

  function pilihlevel(id){
    $('#'+id).modal('show')
  }

function loadPangkat(){
  let uri = "<?= base_url('user/getData');?>";
  let dataJson = {
      submit: true,
      jenis: 'pangkat'
  };
  ajaxCallback(function(data){
      if (data.code == 1) {
        tr =`<option value="" selected disabled>-- Pilih Pangkat --</option>`;
        data.result.data.forEach(element => {
          tr += `<option value="${element.idpangkat}" title="${element.pangkat}">${element.pangkat}</option>`
        });
        $('#idpangkat').html(tr);
      } else {
        alert(data.message.text);
      }
  },uri,dataJson);
}

function loadGolongan(){
  let uri = "<?= base_url('user/getData');?>";
  let dataJson = {
      submit: true,
      jenis: 'golongan'
  };
  ajaxCallback(function(data){
      if (data.code == 1) {
        tr =`<option value="" selected disabled>-- Pilih Golongan --</option>`;
        data.result.data.forEach(element => {
          tr += `<option value="${element.idgolongan}|${element.idpangkat}" title="${element.golongan}">${element.golongan}</option>`
        });
        $('#idgolongan').html(tr);
      } else {
        alert(data.message.text);
      }
  },uri,dataJson);
}

function loadPendidikan(){
  let uri = "<?= base_url('user/getData');?>";
  let dataJson = {
      submit: true,
      jenis: 'pendidikan'
  };
  ajaxCallback(function(data){
      if (data.code == 1) {
        tr =`<option value="" selected disabled>-- Pilih Pendidikan --</option>`;
        data.result.data.forEach(element => {
          tr += `<option value="${element.idpendidikan}" title="${element.pendidikan}">${element.pendidikan}</option>`
        });
        $('#idpendidikan').html(tr);
      } else {
        alert(data.message.text);
      }
  },uri,dataJson);
}

function addUser(){
  let uri = "<?= base_url('user/addUser');?>";
  let dataJson = {
      submit: true,
      nip:$('#nip2').val(),
      username:$('#username2').val(),
      password:$('#password2').val(),
      no_karpeg:$('#no_karpeg').val(),
      nama:$('#nama').val(),
      idpangkat:$('#idpangkat').val(),
      idgolongan:$('#idgolongan').val(),
      tmt:$('#tmt').val(),
      tempat_lahir:$('#tempat_lahir').val(),
      tgl_lahir:$('#tgl_lahir').val(),
      jk:$('#jk').val(),
      idpendidikan:$('#idpendidikan').val(),
      alamat:$('#alamat').val(),
      telepon:$('#telepon').val(),
      email:$('#email').val(),
      idbidang:$('#pilihbidang').val(),
  };
  ajaxCallback(function(data){
      if (data.code == 1) {
        alert('berhasil');
        location.reload();
      } else {
        alert('Pastikan Anda sudah mengisi form dengan benar')
      }
  },uri,dataJson);
}

</script>
<?php echo $this->endSection();?>
