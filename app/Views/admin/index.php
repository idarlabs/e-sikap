<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>
<!-- first -->

<div class="container-fluid">
        <!-- Info boxes -->

        <div class="row">

          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box">
              <span class="info-box-icon bg-success elevation-1"><i class="fas fa-smile"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pengajuan Diterima</span>
                <span class="info-box-number">
                <?= $diterima ?>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-dna"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pengajuan Pending</span>
                <span class="info-box-number">
                <?= $pending ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-4 col-md-4">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-user-alt-slash"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pengajuan Ditolak</span>
                <span class="info-box-number">
                <?= $ditolak ?></span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <!-- <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pelanggan</span>
                <span class="info-box-number">8</span>
              </div>
               /.info-box-content -->
            </div>
            <!-- /.info-box -->

          <!-- /.col -->

        </div>

        <div class="col-md-12">
            <div class="card">
              <div class="card-header">
               <h5 class="card-title" ><strong>Informasi Jumlah Data Pemohon Terakhir</strong></h5>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-7">
                    <p class="text-left">
                      <div class="table-responsive">
                        <table class="table m-0">
                          <thead>
                          <tr>
                            <th><center>Bidang</th>
                              <th><center>Diterima</th>
                              <th><center>Pending</th>
                              <th><center>Ditolak</th>
                            </tr>
                          </thead>
                          <tbody>
                          <tr>
                            <td><center>Desain Industri</td>
                              <td><center><span class="badge badge-success"><?= $industriselesai ?></td>
                              <td><center><span class="badge badge-warning"><?= $industripending ?></td>
                              <td><center><span class="badge badge-danger"><?= $industriditolak ?></td>
                            </tr>
                            <tr>
                            <td><center>Paten</td>
                              <td><center><span class="badge badge-success"><?= $patenselesai ?></td>
                              <td><center><span class="badge badge-warning"><?= $patenpending ?></td>
                              <td><center><span class="badge badge-danger"><?= $patenditolak ?></td>
                            </tr>
                            <tr>
                            <td><center>Merek</td>
                              <td><center><span class="badge badge-success"><?= $merekselesai ?></td>
                              <td><center><span class="badge badge-warning"><?= $merekpending ?></td>
                              <td><center><span class="badge badge-danger"><?= $merekditolak ?></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </p>


                    <!-- /.chart-responsive -->
                  </div>
                  <div class="col-md-5">
                    <div class="row">
                      <div class="col-sm-2"></div>
                      <div class="col-sm-8">
                        <div class="kontener tengah" style="display:none;">
                          <center><canvas id="piechart" width="100" height="100"></canvas></center>
                        </div>
                        <div class="text-dark tengah" id="datakosong" style="display:none;margin-top:100px;">Data <strong>Chart Permohonan</strong> masih kosong.</div>
                      </div>
                      <div class="col-sm-2"></div>
                    </div>
                  </div>
                  <div class="col-md-4">

                    <!-- /.col -->
                  </div>
                  <!-- /.col -->

                  <!-- /.row -->
                </div>
                <!-- ./card-body -->

                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                <h5 class="card-title"><strong>Apa itu E-SIKAP ?</strong></h5>

              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <div class="row">
                  <div class="col-md-12">
                    <p class="text-left">
                      E-SIKAP adalah elektronik Sistem Informasi Kinerja Pemeriksa yang berfungsi sebagai sarana untuk penilaian kredibilitas dokumen
                    </p>


                    <!-- /.chart-responsive -->
                  </div>
                  <div class="col-md-4">

                    <!-- /.col -->
                  </div>
                  <!-- /.col -->

                  <!-- /.row -->
                </div>
                <!-- ./card-body -->

                <!-- /.card-footer -->
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
    </div>

</div>
<script type="text/javascript">
  var satu = "<?=$diterima;?>";
  var dua = "<?=$pending;?>";
  var tiga = "<?=$ditolak;?>";

  if(satu == "0" && dua == "0" && tiga == "0"){
    $('.kontener').hide()
    $('#datakosong').show()
  }else{
    $('.kontener').show()
    $('#datakosong').hide()
  }

  var ctx = document.getElementById("piechart").getContext("2d");
  var data = {
    labels: ['Diterima', 'Pending', 'Ditolak'],
    datasets: [{
      label: "Jumlah Permohonan",
      data: [satu,dua,tiga],
      backgroundColor: [
        '#28A745',
        '#FFC107',
        '#DC3545'
      ]
    }]
  };

  var myPieChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    option: {
      responsive: true
    }
  });
</script>

<!-- end -->

<?php echo $this->endSection();?>
