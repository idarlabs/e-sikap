<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>

<div class="card">
  <div class="card-body">
    <div class="tab-content" id="myTabContent">
      <table id="example1" class="table table-bordered table-striped teks15">
        <thead>
          <tr>
            <th class="tengah">No</th>
            <th>Nip</th>
            <th>Pengusul</th>
            <th>Gol / Ruang</th>
            <th class="tengah">#</th>
          </tr>
        </thead>
        <tbody>
          <?php if($precutoff !== NULL){ $nom = 1; foreach($precutoff as $dp): ?>
            <tr>
              <td class="tengah"><?=$nom;?></td>
              <td><?=$dp->nip1;?></td>
              <td><?=$dp->nama;?></td>
              <td><?=$dp->golongan;?></td>
              <td class="tengah">
                <?php if($dp->nip !== $dp->nip1){ ?>
                  <a href="#" class="btn btn-sm btn-primary" onclick="tambahdata('<?=$dp->nip1;?>')">Tambah Data</a>
                <?php }else{ ?>
                  <a href="#" class="btn btn-sm btn-warning" onclick="ubahdata('<?=$dp->nip1;?>')">Ubah Data</a>
                <?php } ?>
              </td>
            </tr>
          <?php $nom++; endforeach; } ?>
        </tbody>
      </table>
    </div>
  </div>
  </div>
</div>

<div class="modal fade" id="formtambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Form Tambah Kredit</h4>
      </div>
      <div class="modal-body">
        <label for="penijazah">Pendidikan Formal dan mencapai gelar / ijasah</label>
        <input type="text" class="form-control" id="penijazah" placeholder="Kredit Pendidikan Formal">
        <label for="sttpp">Pendidikan dan Pelatihan (STTPP)</label>
        <input type="text" class="form-control" id="sttpp" placeholder="Kredit Pelatihan STTPP">
        <label for="diklat">Diklat Prajabatan</label>
        <input type="text" class="form-control" id="diklat" placeholder="Kredit Diklat Prajabatan">
        <label for="pemeriksaan">Pemeriksaan</label>
        <input type="text" class="form-control" id="pemeriksaan" placeholder="Kredit Pemeriksaan">
        <label for="profesi">Pengembangan Profesi</label>
        <input type="text" class="form-control" id="profesi" placeholder="Kredit Pengembangan Profesi">
        <label for="pendukung">Pendukung Kegiatan Pemeriksaan</label>
        <input type="text" class="form-control" id="pendukung" placeholder="Kredit Pendukung Kegiatan">
      </div>
      <div class="modal-footer">
        <input type="hidden" id="nip_pengusul">
        <button type="button" class="btn btn-primary btn-block" onclick="simpan()">Simpan Data</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function tambahdata(nip){
    $('#formtambah').modal('show')
    $('#nip_pengusul').val(nip)
  }

  function simpan(){
    var nip_pengusul = $('#nip_pengusul').val();
    var penijazah = $('#penijazah').val();
    var sttpp = $('#sttpp').val();
    var diklat = $('#diklat').val();
    var pemeriksaan = $('#pemeriksaan').val();
    var profesi = $('#profesi').val();
    var pendukung = $('#pendukung').val();

    if(nip_pengusul != '' && penijazah != '' && sttpp != '' && diklat != '' && pemeriksaan != '' && profesi != '' && pendukung != ''){
      if(confirm('Anda yakin akan menyimpan data ini?')){
        let uri = "<?= base_url('kreditlama/tambah');?>";
        let dataJson = {
            submit: true,
            nip: nip_pengusul,
            pendidikan_1: penijazah,
            pendidikan_2: sttpp,
            pendidikan_3: diklat,
            pemeriksaan: pemeriksaan,
            pengembangan: profesi,
            penunjang: pendukung
        };
        ajaxCallback(function(data){
            if (data.code == 1) {
              console.log('berhasil')
              location.reload();
            } else {
              console.log('gagal');
            }
        },uri,dataJson);
      }
    }else{
      alert('Pastikan form yang Anda isi sudah benar..!!!')
    }
  }
</script>

<?php echo $this->endSection();?>
