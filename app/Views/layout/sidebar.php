<aside class="main-sidebar elevation-4 sidebar-dark-danger">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('home'); ?>" class="brand-link">
      <img src="<?= base_url() ?>/assets/dist/img/logonya.jpg" class="brand-image img-circle">
      <span class="brand-text font-weight-bold">DJKI</span>
    </a>

    <!-- Sidebar -->

    <div class="sidebar">
      <!-- Sidebar user panel (optional)
      <div class="user-panel   ">
        <div class="image">
        </div>

        <div class="info">
        </div>
      </div>
    -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item has-treeview menu-open">
            <a href="<?= base_url() ?>/home" class="nav-link active">
              &nbsp;<i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>

          </li>

            <?php if(session()->get('level')=="Admin"){ ?>

              <li class="nav-header">MENU</li>
              <li class="nav-item has-treeview">
                <a href="<?= base_url() ?>/kreditlama" class="nav-link">
                  <i class="nav-icon fas fa-book-open"></i>
                  <p>
                    Kredit Pengusul
                  </p>
                </a>
              </li>

          <li class="nav-header">DATA MASTER</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Master Pegawai
                <i class="fas fa-angle-left right"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_pendidikan" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Pendidikan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_pangkat" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Pangkat</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_golongan" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Golongan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_jabatan" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Jabatan</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/jenis_pemeriksa" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Jenis Pemeriksa</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/tim_penilai" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Daftar Penilai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/pegawai" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Daftar Pejabat</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <!-- <li class="nav-item">
                <a href="<?= base_url() ?>/jenis_unsur" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Master Jenis Unsur</p>
                </a>
              </li> -->
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_unsur" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Master Unsur</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_sub_unsur" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Master Sub Unsur</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/m_kegiatan" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Master Kegiatan</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="<?= base_url() ?>/masterkegiatan" class="nav-link">
              <i class="nav-icon fas fa-book-open"></i>
              <p>
                Create Kegiatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url() ?>/periode" class="nav-link">
              <i class="nav-icon fas fa-calendar-alt"></i>
              <p>Data Periode</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url() ?>/tambahpenilai" class="nav-link">
              <i class="nav-icon fas fa-book"></i>
              <p>Penilai</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url() ?>/list_pengusul" class="nav-link">
              <i class="nav-icon fas fa-list"></i>
              <p>List Pengusul</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url() ?>/pengajuan_all" class="nav-link">
              <i class="nav-icon fas fa-dna"></i>
              <p>Daftar Upload SK</p>
            </a>
          </li>
          <li class="nav-header">PENGATURAN</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-wrench"></i>
              <p>
                Setelan
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url() ?>/user/profil" class="nav-link">
                  <i class="fas fa-user nav-icon"></i>
                  <p>Profil</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/user/ubah_password" class="nav-link">
                  <i class="fas fa-wrench nav-icon"></i>
                  <p>Ubah Password</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/user" class="nav-link">
                  <i class="fas fa-user-cog nav-icon"></i>
                  <p>User Manajemen</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="<?= base_url() ?>//login/logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>

 <?php
    }else{
      $linkbidang = "";
      if(session()->get('idbidang') == "1"){
        $linkbidang = "/bidang/desainindustri";
      }else if(session()->get('idbidang') == "2"){
        $linkbidang = "/bidang/paten";
      }else if(session()->get('idbidang') == "3"){
        $linkbidang = "/bidang/merek";
      }
    ?>

<?php if(session()->get('statuspenilai')){ ?>
  <li class="nav-item has-treeview">
    <a href="<?= base_url();?>/pengajuan" class="nav-link">
      <i class="nav-icon fas fa-table"></i>
      <p>Pengajuan</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="<?= base_url() ?>/user/profil" class="nav-link">
      <i class="fas fa-user nav-icon"></i>
        <p>Profil</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="<?= base_url() ?>/user/ubah_password" class="nav-link">
      <i class="fas fa-wrench nav-icon"></i>
        <p>Ubah Password</p>
    </a>
  </li>
<?php }else{ ?>
  <li class="nav-item has-treeview">
    <a href="<?= base_url().$linkbidang; ?>" class="nav-link">
      <i class="nav-icon fas fa-table"></i>
      <p>Pengusulan</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="<?= base_url() ?>/user/profil" class="nav-link">
      <i class="fas fa-user nav-icon"></i>
        <p>Profil</p>
    </a>
  </li>
  <li class="nav-item">
    <a href="<?= base_url() ?>/user/ubah_password" class="nav-link">
      <i class="fas fa-wrench nav-icon"></i>
        <p>Ubah Password</p>
    </a>
  </li>
<?php } ?>
   <li class="nav-item has-treeview">
     <a href="<?= base_url() ?>/login/logout" class="nav-link">
       <i class="nav-icon fas fa-sign-out-alt"></i>
       <p>
         Logout
       </p>
     </a>
   </li>
 <?php } ?>
</ul>
        </nav>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
