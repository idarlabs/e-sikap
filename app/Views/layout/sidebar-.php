  <aside class="main-sidebar elevation-4 sidebar-dark-danger">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('home'); ?>" class="brand-link">
      <img src="<?= base_url() ?>/assets/dist/img/e.png" class="brand-image img-circle elevation-5" style="opacity: .">
      <span class="brand-text font-weight-bold">e-SIKAP</span>
    </a>

    <!-- Sidebar -->

    <div class="sidebar">
      <!-- Sidebar user panel (optional)
      <div class="user-panel   ">
        <div class="image">
        </div>

        <div class="info">
        </div>
      </div>
    -->


    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item has-treeview menu-open">
            <a href="<?= base_url() ?>/login" class="nav-link active">
              &nbsp;<i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>

            </li>

              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-user-alt"></i>
                  <p>
                    Master Pegawai
                    <i class="fas fa-angle-left right"></i>

                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/m_unit_kerja" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Data Unit Kerja</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/m_pendidikan" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Data Pendidikan</p>
                    </a>
                  </li>
              <!--    <li class="nav-item">
                    <a href="<?= base_url() ?>/ms_kelompok" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Data Kelompok</p>
                    </a>
                  </li> -->
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/m_pangkatgol" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Pangkat & Golongan</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/datapegawai" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Data Pegawai</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/portofolio" class="nav-link">
                      <i class="nav-icon fas fa-users"></i>
                      <p>Portofolio</p>
                    </a>
                  </li>

                </ul>
              </li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-table"></i>
                  <p>
                    Master Data
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/pemilihanskp" class="nav-link">
                      <i class="nav-icon fas fa-book"></i>
                      <p>Pemilihan Periode SKP</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/pemilihanpenilai" class="nav-link">
                      <i class="nav-icon fas fa-book-open"></i>
                      <p>Pemilihan Tim Penilai</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/datapenilaian" class="nav-link">
                      <i class="nav-icon fas fa-coins"></i>
                      <p>Data Item Penilaian</p>
                    </a>
                  </li>


                </ul>
              </li>

              <li class="nav-item has-treeview">
                <a href="<?= base_url() ?>/masterkegiatan" class="nav-link">
                  <i class="nav-icon fas fa-book-open"></i>
                  <p>
                    Master Kegiatan
                  </p>
                </a>
              </li>

              <li class="nav-header">PENGATURAN</li>
              <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                  <i class="nav-icon fas fa-wrench"></i>
                  <p>
                    Setelan
                    <i class="fas fa-angle-left right"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview">
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/user/profil" class="nav-link">
                      <i class="fas fa-user nav-icon"></i>
                      <p>Profil</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/user/ubah_password" class="nav-link">
                      <i class="fas fa-wrench nav-icon"></i>
                      <p>Ubah Password</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="<?= base_url() ?>/user" class="nav-link">
                      <i class="fas fa-user-cog nav-icon"></i>
                      <p>User Manajemen</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="nav-item has-treeview">
                <a href="<?= base_url() ?>//login/logout" class="nav-link">
                  <i class="nav-icon fas fa-sign-out-alt"></i>
                  <p>
                    Logout
                  </p>
                </a>
              </li>
            </nav>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
