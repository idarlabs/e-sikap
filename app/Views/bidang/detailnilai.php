<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten'); ?>
<div class="card">
    <div class="card-body">
        <div class="col-sm-12">
          <a href="<?=base_url();?>/bidang/<?=$_GET['bidang'];?>" class="btn btn-sm btn-info"><i class="fas fa-arrow-left"></i> Kembali</a>
          <button class="btn btn-sm btn-danger" id="pdf"><i class="fa fa-book"></i>&nbsp;PDF</button>
          <p></p>
          <div>
            <table class="table table-bordered teks15">
                <tr>
                  <th colspan="2">PENETAPAN ANGKA KREDIT</th>
                  <th class="tengah">LAMA</th>
                  <th class="tengah">BARU</th>
                  <th class="tengah">JUMLAH</th>
                </tr>
                <tr>
                  <td class="tengah">1</td>
                  <th colspan="4">UNSUR UTAMA</th>
                </tr>
                <tr>
                  <td></td>
                  <td>a. (1) Pendidikan Formal dan mencapai gelar / ijasah</td>
                  <td class="tengah"><?=$pen1;?></td>
                  <td class="tengah"><?=$pendidikan_1;?></td>
                  <td class="tengah"><?=($pen1+$pendidikan_1);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat surat Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
                  <td class="tengah"><?=$pen2;?></td>
                  <td class="tengah"><?=$pendidikan_2;?></td>
                  <td class="tengah"><?=($pen2+$pendidikan_2);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;(3) Diklat Prajabatan</td>
                  <td class="tengah"><?=$pen3;?></td>
                  <td class="tengah"><?=$pendidikan_3;?></td>
                  <td class="tengah"><?=($pen3+$pendidikan_3);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>b. Pemeriksaan</td>
                  <td class="tengah"><?=$pem;?></td>
                  <td class="tengah"><?=$pemeriksaan;?></td>
                  <td class="tengah"><?=($pem+$pemeriksaan);?></td>
                </tr>
                <tr>
                  <td></td>
                  <td>c. Pengembangan Profesi</td>
                  <td class="tengah"><?=$peng;?></td>
                  <td class="tengah"><?=$pengembangan;?></td>
                  <td class="tengah"><?=($peng+$pengembangan);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR UTAMA</th>
                  <th class="tengah"><?=$jmlunsurutama2;?></th>
                  <th class="tengah"><?=$jmlunsurutama;?></th>
                  <th class="tengah"><?=($jmlunsurutama2+$jmlunsurutama);?></th>
                </tr>
                <tr>
                  <td class="tengah">2</td>
                  <th colspan="4">UNSUR PENUNJANG</th>
                </tr>
                <tr>
                  <td></td>
                  <td>Pendukung Kegiatan Pemeriksaan</td>
                  <td class="tengah"><?=$pen;?></td>
                  <td class="tengah"><?=$penunjang;?></td>
                  <td class="tengah"><?=($pen+$penunjang);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR PENUNJANG</th>
                  <th class="tengah"><?=$pen;?></th>
                  <th class="tengah"><?=$penunjang;?></th>
                  <td class="tengah"><?=($pen+$penunjang);?></td>
                </tr>
                <tr>
                  <td></td>
                  <th>JUMLAH UNSUR UTAMA DAN PENUNJANG</th>
                  <th class="tengah"><?=$jmlkeduanya2;?></th>
                  <th class="tengah"><?=$jmlkeduanya;?></th>
                  <td class="tengah text-danger"><?=($jmlkeduanya2+$jmlkeduanya);?></td>
                </tr>
            </table>
              <?php
              $totalsemua = $jmlkeduanya2+$jmlkeduanya;
              $msg_terakhir = "";
              $itemakhir = 0;
              if($totalsemua >= $pengangkatan[0]->minimum){
                $itemakhir = 1;
                $msg_terakhir = "Dapat dipertimbangkan untuk dinaikkan dalam jabatan Pemeriksa ".$datapengaju->jenispemeriksa." ".$datapengaju->jabatan;
              }else{
                $itemakhir = 0;
                $msg_terakhir = "Belum dapat dipertimbangkan untuk dinaikkan dalam pangkat / jabatan.";
              }
              ?>
              <span><span class="text-danger"><strong>*</strong></span><?=$msg_terakhir;?></span>
          </div>
    </div>
</div>
<script type="text/javascript">
  $('#pdf').on('click', function(){
    var base = "<?=base_url();?>";
    var idpeng = "<?=$idpeng;?>";
    var idper = "<?=$idper;?>";
    var nipnya = "<?=session()->get('nip');?>";
    window.open(base+'/bidang/pdf/'+idpeng+'/'+nipnya+'/'+idper, '_blank');
  })
</script>
<?php echo $this->endSection(); ?>
