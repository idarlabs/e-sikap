<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>
<div class="pengajuan">
  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col-sm-6">
          <div class="row">
            <div class="col-sm-7">
              <select class="form-control" id="jenisunsur" name="jenisunsur">
                <option value="">-- Pilih Jenis Unsur --</option>
                <?php foreach($jenis_unsur as $ju):?>
                  <option value="<?=$ju->idjenisunsur;?>"><?=$ju->jenis;?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-sm-5">
              <?php if(!$sudahmengajukan){?>
                <button type="button" class="btn btn-primary" onclick="lanjut()">Lanjutkan</button>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <div class="row">
        <div class="col kanan">
          <button type="button" id="kegi" class="btn btn-warning btn-sm" onclick="kegiatan()">Kegiatan</button>
          <button type="button" id="peng" class="btn btn-primary btn-sm" onclick="pengajuan()" href="#terima" data-toggle="tab">Pengajuan</button>
          <p></p>
        </div>
      </div>
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Draft</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Usulan</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Proses</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="contact2-tab" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact2" aria-selected="false">Tolak</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
          <table id="example1" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Kegiatan</th>
                <th>Butir</th>
                <th>Doc</th>
                <th class="tengah">#</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($draft !== NULL){ $nom = 1; foreach($draft as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->masterkegiatan;?></td>
                  <td><?=$dp->butirkegiatan;?></td>
                  <td class="tengah"><a href="<?=base_url()."/doc/".$dp->path;?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-alt"></i></a></td>
                  <td class="tengah">
                        <a href="#" class="btn btn-sm btn-success" onclick="usulkan('<?=$dp->idriwayatpengajuan;?>')">Usulkan</a>
                        <a href="#" class="btn btn-sm btn-danger" onclick="hapusRiwayat('<?=$dp->idriwayatpengajuan;?>')"><i class="fas fa-trash-alt"></i></a>
                  </td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
          <table id="example2" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Kegiatan</th>
                <th>Butir</th>
                <th class="tengah">Doc</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($usulan !== NULL){ $nom = 1; foreach($usulan as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->masterkegiatan;?></td>
                  <td><?=$dp->butirkegiatan;?></td>
                  <td class="tengah"><a href="<?=base_url()."/doc/".$dp->path;?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-alt"></i></a></td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
          <table id="example3" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Kegiatan</th>
                <th>Butir</th>
                <th class="tengah">Doc</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($proses !== NULL){ $nom = 1; foreach($proses as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->masterkegiatan;?></td>
                  <td><?=$dp->butirkegiatan;?></td>
                  <td class="tengah"><a href="<?=base_url()."/doc/".$dp->path;?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-alt"></i></a></td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact2-tab">
          <table id="example3" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Kegiatan</th>
                <th>Butir</th>
                <th class="tengah">Doc</th>
                <th>Keterangan</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($ditolakx !== NULL){ $nom = 1; foreach($ditolakx as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->masterkegiatan;?></td>
                  <td><?=$dp->butirkegiatan;?></td>
                  <td class="tengah"><a href="<?=base_url()."/doc/".$dp->path;?>" target="_blank" class="btn btn-sm btn-outline-primary"><i class="fas fa-file-alt"></i></a></td>
                  <td><?=$dp->keterangan;?></td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
      </div>

      <ul class="nav nav-tabs" id="myTab2" role="tablist" style="display:none;">
        <li class="nav-item">
          <a class="nav-link active" id="terima-tab" data-toggle="tab" href="#terima" role="tab" aria-controls="contact" aria-selected="false">Diterima</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" id="tolak-tab" data-toggle="tab" href="#tolak" role="tab" aria-controls="contact" aria-selected="false">Tidak Mencukupi</a>
        </li>
      </ul>
      <div class="tab-content" id="myTabContent2" style="display:none;">
        <div class="tab-pane fade" id="terima" role="tabpanel" aria-labelledby="terima-tab">
          <table id="example3" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Periode</th>
                <th class="tengah">Detail</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($dataTerima !== NULL){ $nom = 1; foreach($dataTerima as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->tgl_buka . " s/d " . $dp->tgl_tutup;?></td>
                  <td class="tengah"><a href="<?=base_url("bidang/detailnilai/$dp->idpengajuan/$dp->idperiode");?>?bidang=paten" class="btn btn-sm btn-outline-primary"><i class="fas fa-info-circle"></i></a></td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane fade" id="tolak" role="tabpanel" aria-labelledby="tolak-tab">
          <table id="example4" class="table table-bordered table-striped teks15">
            <thead>
              <tr>
                <th class="tengah">No</th>
                <th>Periode</th>
                <th class="tengah">Detail</th>
              </tr>
            </thead><i class="fa file-alt"></i>
            <tbody>
              <?php if($dataTolak !== NULL){ $nom = 1; foreach($dataTolak as $dp):?>
                <tr>
                  <td class="tengah"><?=$nom;?></td>
                  <td><?=$dp->tgl_buka . " s/d " . $dp->tgl_tutup;?></td>
                  <td class="tengah"><a href="<?=base_url("bidang/detailnilai/$dp->idpengajuan");?>?bidang=paten" class="btn btn-sm btn-outline-danger"><i class="fas fa-info-circle"></i></a></td>
                </tr>
              <?php $nom++; endforeach; } ?>
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>

<div class="modal fade" id="formmulai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
      <?php if($idperiode != '0'){ ?>
        <div class="form-group">
          <h3>Pengumuman..!!!</h3>
          <p>Silahkan menekan tombol dibawah ini untuk memulai pengajuan kenaikan pangkat.</p>
          <button type="button" class="btn btn-warning btn-block" name="button" onclick="isipertama()">Mulai</button>
        </div>
      <?php }else{ ?>
        <div class="form-group">
          <h3>Mohon maaf..!!!</h3>
          <p>Untuk saat ini periode pengangkatan belum dibuka.</p>
          <button type="button" class="btn btn-danger btn-block" name="button" onclick="home()">Kembali ke Dashboard</button>
        </div>
      <?php } ?>
      </div>
    </div>
  </div>
</div>
</form>

<script type="text/javascript">
  function kegiatan(){
    $('#myTab').show()
    $('#myTab2').hide()
    $('#myTabContent').show()
    $('#myTabContent2').hide()
  }

  function pengajuan(){
    $('#myTab2').show()
    $('#myTab').hide()
    $('#myTabContent2').show()
    $('#myTabContent').hide()
  }

  function home(){
    location.href="<?=base_url();?>/home"
  }
  function hapusRiwayat(idRiwayatPengajuan){
    if (confirm('Anda yakin akan menghapus kegiatan ini?')) {
      let uri = "<?= base_url('bidang/hapusKegiatan');?>";
      let dataJson = {
          submit: true,
          idriwayatpengajuan: idRiwayatPengajuan,
      };
      ajaxCallback(function(data){
          if (data.code == 1) {
            alert('berhasil');
            location.reload();
          } else {
            alert(data.message.text)
          }
      },uri,dataJson);
    }
  }

  var isipengajuan = "<?=$isipengajuan;?>";
  function usulkan(idriwayatpengajuan){
    if(confirm('Anda yakin akan mengajukan kegiatan ini sekarang?')){
      $.ajax({
        url: '<?php echo base_url('Bidang/usulkandesainindustri') ?>',
        type: "POST",
        data: {
          validasi: 'go',
          idriwayatpengajuan: idriwayatpengajuan
        },
        success: function(hasil) {
          location.reload()
        },
        error: function(error) {
          alert("error");
          console.error(error);
        }
      })
    }
  }
  function isipertama(){
    $('#formmulai').modal('hide')
    $('#formmulai').on('hidden.bs.modal',function(){
      let uri = "<?= base_url('bidang/tambahAwal');?>";
      let dataJson = {
          submit: true,
          idperiode: "<?=$idperiode;?>",
      };
      ajaxCallback(function(data){
          if (data.code == 1) {
            console.log('berhasil')
            location.reload();
          } else {
            console.log('gagal');
          }
      },uri,dataJson);
    })
  }
  if(isipengajuan != "1"){
    $('.pengajuan').hide()
    $(document).ready(function(){
      $('#formmulai').modal('show')
    })
  }else{
    $('.pengajuan').show()
  }
  function lanjut(){
    var base = "<?=base_url();?>";
    var jenisunsur = $('#jenisunsur').val();

    if(jenisunsur !== ""){
      location.href=base+'/bidang/kegiatan/'+jenisunsur
    }
  }
</script>


<?php echo $this->endSection();?>
