<?php echo $this->extend('layout/idar'); ?>
<?php echo $this->section('konten');?>

<div class="card">
  <div class="card-body">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-6">
            <select class="form-control" id="unsur" name="unsur" onchange="aktifkan_subunsur(this.value)">
              <option value="">-- Pilih Unsur --</option>
              <?php foreach($unsur as $u):?>
                <option value="<?=$u->idunsur;?>"><?=$u->unsur;?></option>
              <?php endforeach; ?>
            </select>
          </div>
          <div class="col-sm-6">
            <select class="form-control" id="subunsur" name="subunsur" onchange="aktifkan_mkegiatan(this.value)" disabled>
              <option value="">-- Pilih Sub Unsur --</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <select class="form-control" id="mkegiatan" name="mkegiatan" onchange="aktifkan_kegiatan(this.value)" disabled>
              <option value="">-- Pilih Kegiatan --</option>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12">
            <select class="form-control" id="kegiatan" name="kegiatan" onchange="tampilform(this.value)" disabled>
              <option value="">-- Pilih Butir Kegiatan --</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <hr>
      </div>
      <div class="col-12" id="form" style="display:none;">
        <div class="col-sm-12">
          <ul>
            <li id="kegiatannya"></li>
            <ul>
              <li id="butirnya"></li>
            </ul>
          </ul>
        </div>
        <div class="col-6">
          <input type="file" class="form-control" name="fileUpload" id="fileUpload" placeholder="Upload Dokumen">
        </div>
        <div class="col-6">
          <button onclick="upload()" type="button" class="btn btn-primary" name="button">Simpan ke Draft</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Gagal!</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <div class="modal-body">
        <div class="container-fluid">
            <div id="peringatan" class="alert alert-danger"></div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
function tampilform(id){
  if(id !== ""){
    $('#butirnya').text($("#kegiatan option:selected").text())
    $('#form').show()
  }else{
    $('#form').hide()
  }
}
function aktifkan_kegiatan(id){
  if(id !== ""){
    $('#kegiatan').prop('disabled', false)
    $('#kegiatannya').text($("#mkegiatan option:selected").text())
    tampilkan_kegiatan(id)
  }else{
    $('#form').hide()
    $('#kegiatan').prop('disabled', true)
    $('#kegiatan').val("")
  }
}

function tampilkan_kegiatan(id){
  var jabatanku = "<?=session()->get('jabatan');?>";
  $.ajax({
    url: '<?php echo base_url('Bidang/butirkegiatan') ?>',
    type: "POST",
    data: {
      validasi: 'go',
      idjenispemeriksakerja: "<?=$kode_jpk;?>",
      idjenisunsur: "<?=$jenu;?>",
      idunsur: $('#unsur').val(),
      idsubunsur: $('#subunsur').val(),
      idmasterkegiatan: $('#mkegiatan').val(),
    },
    success: function(hasil) {
      var obj = JSON.parse(hasil);
      $('#kegiatan').empty()
      $('#kegiatan').append('<option value="">-- Pilih Butir Kegiatan --</option>')
      obj.forEach(data => {
        if(data.idjabatan == '5'){
          $('#kegiatan').append('<option value="'+data.idkegiatan+'">'+data.butirkegiatan+'</option>')
        }else{
          if(data.idjabatan == jabatanku){
            $('#kegiatan').append('<option value="'+data.idkegiatan+'">'+data.butirkegiatan+'</option>')
          }
        }
      })
    },
    error: function(error) {
      alert("error");
      console.error(error);
    }
  })
}

function aktifkan_mkegiatan(id){
  if(id !== ""){
    $('#mkegiatan').prop('disabled', false)
    tampilkan_mkegiatan(id)
  }else{
    $('#form').hide()
    $('#kegiatan').prop('disabled', true)
    $('#kegiatan').val("")
    $('#mkegiatan').prop('disabled', true)
    $('#mkegiatan').val("")
  }
}

function tampilkan_mkegiatan(id){
  $.ajax({
    url: '<?php echo base_url('Bidang/mkegiatan') ?>',
    type: "POST",
    data: {
      validasi: 'go',
      id: id
    },
    success: function(hasil) {
      var obj = JSON.parse(hasil);
      $('#mkegiatan').empty()
      $('#mkegiatan').append('<option value="">-- Pilih Kegiatan --</option>')
      obj.forEach(data => {
        $('#mkegiatan').append('<option value="'+data.idmasterkegiatan+'">'+data.masterkegiatan+'</option>')
      })
    },
    error: function(error) {
      alert("error");
      console.error(error);
    }
  })
}

function aktifkan_subunsur(id){
  var subunsur = $('#subunsur')
  $('#subunsur').val("")
  if(id !== ""){
    subunsur.prop('disabled', false)
    tampilkanSubUnsur(id)
  }else{
    $('#form').hide()
    $('#mkegiatan').prop('disabled', true)
    $('#mkegiatan').val("")
    $('#kegiatan').prop('disabled', true)
    $('#kegiatan').val("")
    subunsur.prop('disabled', true)
    subunsur.val("")
  }
}

function tampilkanSubUnsur(id) {
  $.ajax({
    url: '<?php echo base_url('Bidang/subunsur') ?>',
    type: "POST",
    data: {
      validasi: 'go',
      id: id
    },
    success: function(hasil) {
      var obj = JSON.parse(hasil);
      $('#subunsur').empty()
      $('#subunsur').append('<option value="">-- Pilih Sub Unsur --</option>')
      obj.forEach(data => {
        $('#subunsur').append('<option value="'+data.idsubunsur+'">'+data.keterangan+'</option>')
      })
    },
    error: function(error) {
      alert("error");
    }
  })
}

function upload(){
  var basenya = "<?=base_url();?>";
  let uri = "<?= base_url('bidang/addDesainIndustri');?>";
  let fd = new FormData();
  let file = $('#fileUpload').prop('files')[0];
  let idKegiatan = $('#kegiatan').val();
  fd.append('submit',true);
  if (file) {
    fd.append('fileUpload',file);
  }
  fd.append('idkegiatan',idKegiatan);
  ajaxCallbackFormData(function(data){
      if (data.code == 1) {
        location.href=basenya+'/bidang/desainindustri'
      } else {
        $('#peringatan').text(data.message.text);
        $('#modelId').modal('show');
      }
  },uri,fd);
}

</script>

<?php echo $this->endSection();?>
