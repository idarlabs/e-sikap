<?php namespace App\Models;

use CodeIgniter\Model;

class M_sub_unsur_model extends Model
{

    protected $table = "m_sub_unsur";

    public function getM_sub_unsur($id = null)
    {
        if (empty($id )) {
            return $this->table('m_sub_unsur')
                        ->join('jenis_unsur', 'jenis_unsur.idjenisunsur = m_sub_unsur.idjenisunsur')
                        ->join('m_unsur', 'm_unsur.idunsur = m_sub_unsur.idunsur')
                        ->join('jenis_pemeriksa', 'jenis_pemeriksa.idjp = m_sub_unsur.idbidang')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_sub_unsur')
                        ->where('idsubunsur', $id)
                        ->get()
                        ->getRowArray();
        }
    }

    public function insert_m_sub_unsur($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_sub_unsur($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idsubunsur' => $id]);
    }

    public function delete_m_sub_unsur($id)
    {
        return $this->db->table($this->table)->delete(['idsubunsur' => $id]);
    }
}
