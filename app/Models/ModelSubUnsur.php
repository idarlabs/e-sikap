<?php namespace App\Models;

use CodeIgniter\Model;

class ModelSubUnsur extends Model
{

    public function data() {
        $builder = $this->db->table('m_sub_unsur');
        return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('m_sub_unsur')->where("idunsur",$id);
        return $builder->get();
    }

    public function dataid2($idunsur, $idjenisunsur, $idbidang) {
        $builder = $this->db->table('m_sub_unsur')->where(["idunsur" => $idunsur, 'idjenisunsur' => $idjenisunsur, 'idbidang' => $idbidang]);
        return $builder->get();
    }

    public function dataid3($id, $idbidang) {
        $builder = $this->db->table('m_sub_unsur')->where(["idunsur" => $id, "idbidang" => $idbidang]);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('m_sub_unsur')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('m_sub_unsur')->update($data, array('idsubunsur' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('m_sub_unsur')->delete(array('idsubunsur' => $id));
        return $query;
    }
}
