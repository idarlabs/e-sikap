<?php namespace App\Models;

use CodeIgniter\Model;

class Periode_model extends Model
{

    protected $table = "periode";

    public function getPeriode($id=null)
    {

        if (empty($id )) {
            return $this->table('periode')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('periode')
                        ->where('idperiode', $id)
                        ->get()
                        ->getRowArray();
        }
    }

    public function dataperiode($id){
      return $this->table('periode')
      ->where('idperiode', $id)
      ->get()
      ->getResultArray();
    }

    public function insert_periode($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_periode($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idperiode' => $id]);
    }

    public function delete_periode($id)
    {
        return $this->db->table($this->table)->delete(['idperiode' => $id]);
    }
}
