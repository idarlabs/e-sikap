<?php namespace App\Models;

use CodeIgniter\Model;

class List_pengusul_model extends Model
{

    protected $table = "pengajuan";

    public function getList_pengusul($idperiode = false, $idbidang = false)
    {
        if ($idbidang !== false && $idperiode !== false) {
            return $this->table('pengajuan')
                        ->join('pegawai', 'pegawai.nip = pengajuan.nip')
                        ->where(['pengajuan.idbidang' => $idbidang, 'pengajuan.idperiode' => $idperiode])
                        ->get()
                        ->getResultArray();
        }
    }

    public function getList_penilai($idperiode = false, $idbidang = false)
    {
      $q = $this->db->table("penilai p")
      ->select("p.idpenilai,p.nip, peg.nama, p.idjenispemeriksa, p.idperiode")
      ->join("pegawai peg", "peg.nip=p.nip")
      ->where(['p.idjenispemeriksa' => $idbidang, 'p.idperiode' => $idperiode]);
      return $q->get()->getResultArray();
        // return $this->db->query("SELECT penilai.idpenilai, penilai.nip, pegawai.nama, penilai.idjenispemeriksa, penilai.idperiode
        //                         FROM penilai INNER JOIN pegawai ON penilai.nip = pegawai.nip
        //                         WHERE penilai.idjenispemeriksa = '$idbidang' AND penilai.idperiode = '$idperiode' ")->getResultArray();
    }

    // public function getList_penilai($idperiode = false, $idbidang = false)
    // {
    //     if ($idbidang !== false && $idperiode !== false) {
    //         return $this->table('penilai')
    //                     ->join('pegawai', 'pegawai.nip = penilai.nip')
    //                     ->where(['penilai.idperiode' => $idperiode, 'pegawai.idbidang' => $idbidang])
    //                     ->get()
    //                     ->getResultArray();
    //     }
    // }
    // query('SELECT penilai.nip, pegawai.nama, penilai.idjenispemeriksa, penilai.idperiode
    //         FROM penilai INNER JOIN pegawai ON penilai.nip = pegawai.nip
    //         WHERE penilai.idjenispemeriksa = '.$idbidang.' AND penilai.idperiode = '.$idperiode.' ')
    //                     ->get()
    //                     ->getResultArray();

    public function insert_eselon2($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_penilai($data, $idpengajuan)
    {
        return $this->db->table($this->table)->where('idpengajuan',$idpengajuan)->update($data, ['idpengajuan' => $idpengajuan]);
    }

    public function delete_eselon2($id)
    {
        return $this->db->table($this->table)->delete(['ideselon2' => $id]);
    }
}
