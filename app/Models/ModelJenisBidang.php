<?php namespace App\Models;

use CodeIgniter\Model;

class ModelJenisBidang extends Model
{

    public function data() {
        $builder = $this->db->table('jenis_pemeriksa');
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('jenis_pemeriksa')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('jenis_pemeriksa')->update($data, array('idjp' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('jenis_pemeriksa')->delete(array('idbutir1' => $id));
        return $query;
    }
}
