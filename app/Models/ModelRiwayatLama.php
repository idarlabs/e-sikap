<?php namespace App\Models;

use CodeIgniter\Model;

class ModelRiwayatLama extends Model {
  protected $table = 'riwayat_lama';
  protected $primaryKey = 'idriwayatlama';
  protected $useAutoIncrement = true;
  protected $allowedFields = ['nip','idpengajuan','status','sk','tmt','gol_sebelum','gol_setelah','tgl_submit','jam_submit','idperiode','path_dokumen'];
  protected $returnType = 'object';

    public function data() {
        $builder = $this->db->table('riwayat_lama');
        return $builder->get();
    }

    public function datalast($nip){
      $builder = $this->db->table('riwayat_lama')
      ->where('nip', $nip)
      ->orderBy('idriwayatlama', 'DESC')
      ->limit(0, 1);
      return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('riwayat_lama')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('riwayat_lama')->update($data, array('idriwayatlama' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('riwayat_lama')->delete(array('idriwayatlama' => $id));
        return $query;
    }

}
