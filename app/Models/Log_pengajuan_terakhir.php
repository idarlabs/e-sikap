<?php
namespace App\Models;
use CodeIgniter\Model;
class Log_pengajuan_terakhir extends Model{
    protected $table = 'log_pengajuan_terakhir';
    protected $primaryKey = 'idlog';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['nip','idpengajuan','status','sk','tmt','gol_sebelum','gol_setelah','tgl_submit','jam_submit','idperiode','path_dokumen'];
    protected $returnType = 'object';
}