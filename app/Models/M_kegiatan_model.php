<?php namespace App\Models;

use CodeIgniter\Model;

class M_kegiatan_model extends Model
{

    protected $table = "m_kegiatan";

    public function getM_kegiatan($id = null)
    {
        if (empty($id )) {
            return $this->table('m_kegiatan')
                        ->join('jenis_unsur', 'jenis_unsur.idjenisunsur = m_kegiatan.idjenisunsur')
                        ->join('m_unsur', 'm_unsur.idunsur = m_kegiatan.idunsur')
                        ->join('m_sub_unsur', 'm_sub_unsur.idsubunsur = m_kegiatan.idsubunsur')
                        ->join('jenis_pemeriksa', 'jenis_pemeriksa.idjp = m_kegiatan.idbidang')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_kegiatan')
                        ->where('idmasterkegiatan', $id)
                        ->get()
                        ->getRowArray();
        }
    }

    public function insert_m_kegiatan($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_kegiatan($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idmasterkegiatan' => $id]);
    }

    public function delete_m_kegiatan($id)
    {
        return $this->db->table($this->table)->delete(['idmasterkegiatan' => $id]);
    }
}
