<?php namespace App\Models;

use CodeIgniter\Model;

class ModelTimPenilai extends Model
{

    public function data() {
        $builder = $this->db->table('penilai')
        ->join('pegawai p','p.nip=penilai.nip')
        ->join('m_golongan mg','mg.idgolongan=p.idgolongan')
        ->join('m_jabatan mj','mj.idjabatan=mg.idjabatan');
        return $builder->get();
    }

    public function ceksebagaipenilai($nip){
        $r = $this->db->table('penilai')
        ->where('nip', $nip);
        return $r;
    }

    public function data1($id) {
        $builder = $this->db->table('penilai')
        ->where("idperiode", $id);
        return $builder->get();
    }

    public function data2($id) {
        $builder = $this->db->table('penilai')
        ->join("pegawai p", "p.nip=penilai.nip")
        ->where("idperiode", $id);
        return $builder->get();
    }

    public function datapenilaiku($nip){
      $builder = $this->db->table('penilai')
      ->join("pegawai p", "p.nip=penilai.nip")
      ->where("penilai.nip", $nip);
      return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('penilai')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('penilai')->update($data, array('idpenilai' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('penilai')->delete(array('idpenilai' => $id));
        return $query;
    }
}
