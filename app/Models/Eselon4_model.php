<?php namespace App\Models;

use CodeIgniter\Model;

class Eselon4_model extends Model
{

    protected $table = "eselon4";

    public function getEselon4($id = false)
    {
        if ($id === false) {
            return $this->table('eselon4')
                        ->join('eselon1', 'eselon1.ideselon1 = eselon4.ideselon1')
                        ->join('eselon2', 'eselon2.ideselon2 = eselon4.ideselon2')
                        ->join('eselon3', 'eselon3.ideselon3 = eselon4.ideselon3')                                         
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('eselon4')
                        ->where('ideselon4', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_eselon4($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_eselon4($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['ideselon4' => $id]);
    }

    public function delete_eselon4($id)
    {
        return $this->db->table($this->table)->delete(['ideselon4' => $id]);
    }
}