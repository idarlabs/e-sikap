<?php namespace App\Models;
 
use CodeIgniter\Model;
 
class Ms_jabatan_model extends Model
{

    public function getMs_jabatan()
    {
        $builder = $this->db->table('ms_jabatan');
        return $builder->get();
        
//        if($id === false){
  //          return $this->table('ms_jabatan')
    //                    ->get()
      //                  ->getResultArray();
        //} else {
          //  return $this->table('ms_jabatan')
            //            ->where('id_jabatan', $id)
              //          ->get()
                //        ->getRowArray();
        //}   
    }
    
    public function saveMs_jabatan($data)
    {
        $query = $this->db->table('ms_jabatan')->insert($data);
        return $query;
    }
  //  public function insert_ms_jabatan($data)
    //{
      //  return $this->db->table($this->table)->insert($data);
    //}

    public function updateMs_jabatan($data, $id)
    {
        $query = $this->db->table('ms_jabatan')->update($data, array('id_jabatan' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteMs_jabatan($id)
    {
        $query = $this->db->table('ms_jabatan')->delete(array('id_jabatan' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }
 
   
}