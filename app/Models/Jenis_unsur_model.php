<?php namespace App\Models;

use CodeIgniter\Model;

class Jenis_unsur_model extends Model
{

    protected $table = "jenis_unsur";

    public function getJenis_unsur($id = null)
    {
        if (empty($id )) {
            return $this->table('jenis_unsur')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('jenis_unsur')
                        ->where('idjenisunsur', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_jenis_unsur($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_jenis_unsur($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idjenisunsur' => $id]);
    }

    public function delete_jenis_unsur($id)
    {
        return $this->db->table($this->table)->delete(['idjenisunsur' => $id]);
    }
}