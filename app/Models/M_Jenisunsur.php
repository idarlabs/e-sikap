<?php namespace App\Models;

use CodeIgniter\Model;

class M_Jenisunsur extends Model
{

    public function data() {
        $builder = $this->db->table('jenis_unsur');
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('jenis_unsur')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('jenis_unsur')->update($data, array('idjenisunsur' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('jenis_unsur')->delete(array('idjenisunsur' => $id));
        return $query;
    }
}
