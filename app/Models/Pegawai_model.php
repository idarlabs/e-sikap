<?php namespace App\Models;

use CodeIgniter\Model;

class Pegawai_model extends Model
{
    protected $table = 'pegawai';
    protected $primaryKey = 'idpegawai';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['nip','no_karpeg','nama','idpangkat','idgolongan','jk','tmt_pegawai','tempat_lahir','tgl_lahir','idpendidikan','alamat','telepon','email','ktp','ideselon2','tmt_pemeriksa','idunitkerja','idbidang'];
    protected $returnType = 'object';

    public function jGolonganByNip($nip){
        $r = $this->join('m_golongan g','g.idgolongan = pegawai.idgolongan')
                ->where('nip',$nip)
                ->first();
        return $r;
    }
//    protected $table = "pegawai";

//    public function getPegawai($id = null)
//    {
//        if (!empty($id )) {
//            return $this->table('pegawai')
//                        ->join('m_pangkat', 'm_pangkat.idpangkat = pegawai.idpangkat')
//                        ->join('m_pendidikan', 'm_pendidikan.idpendidikan = pegawai.idpendidikan')
//                        ->join('eselon2', 'eselon2.ideselon2 = pegawai.eselon')
//                        ->get()desainindustri(
//                        ->getResultArray();

//        } else {
//            return $this->table('pegawai')
//                        ->where('pegawai.idpegawai', $id)
//                        ->get()
//                        ->getResultArray();
//        }
//    }

    public function data() {
        $builder = $this->db->table('pegawai p')
        ->join("m_pangkat pa","pa.idpangkat=p.idpangkat")
        ->join("m_golongan g","g.idgolongan=p.idgolongan")
        ->join("m_jekel j","j.idjekel=p.jk")
        ->join("m_pendidikan pe","pe.idpendidikan=p.idpendidikan");
        return $builder->get();
    }

    public function data1($id) {
        $builder = $this->db->table('pegawai p')
        ->join("m_pangkat pa","pa.idpangkat=p.idpangkat")
        ->join("m_golongan g","g.idgolongan=p.idgolongan")
        ->join("m_jekel j","j.idjekel=p.jk")
        ->join("m_pendidikan pe","pe.idpendidikan=p.idpendidikan")
        ->where("p.idbidang",$id);
        return $builder->get();
    }

    public function getM_pangkat()
    {
        $builder = $this->db->table('m_pangkat');
        return $builder->get();
    }

    public function getM_golongan()
    {
        $builder = $this->db->table('m_golongan');
        return $builder->get();
    }

    public function getM_pendidikan()
    {
        $builder = $this->db->table('m_pendidikan');
        return $builder->get();
    }

    public function getEselon2()
    {
        $builder = $this->db->table('eselon2');
        return $builder->get();
    }

    public function getPegawai()
    {
        $builder = $this->db->table('pegawai');
        $builder->select('*');
        $builder->join('m_pangkat', 'm_pangkat.idpangkat = pegawai.idpangkat','left');
        $builder->join('m_golongan', 'm_golongan.idgolongan = pegawai.idgolongan','left');
        $builder->join('m_pendidikan', 'm_pendidikan.idpendidikan = pegawai.idpendidikan','left');
        $builder->join('eselon2', 'eselon2.ideselon2 = pegawai.ideselon2','left',);
        return $builder->get();

    }

    public function insert_pegawai($data)
    {
        $query = $this->db->table('pegawai')->insert($data);
        return $query;
    }

    public function update_pegawai($data, $id)
    {
        $query = $this->db->table('pegawai')->update($data, array('idpegawai' => $id));
        return $query;
    }

    public function delete_pegawai($id)
    {
        $query = $this->db->table('pegawai')->delete(array('idpegawai' => $id));
        return $query;
    }
}
