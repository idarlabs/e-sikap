<?php namespace App\Models;

use CodeIgniter\Model;

class ModelJenisPemeriksaKerja extends Model
{

    public function data() {
        $builder = $this->db->table('m_jpk');
        return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('m_jpk')->where("idjenispemeriksakerja",$id);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('m_jpk')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('m_jpk')->update($data, array('idjenispemeriksakerja' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('m_jpk')->delete(array('idjenispemeriksakerja' => $id));
        return $query;
    }
}
