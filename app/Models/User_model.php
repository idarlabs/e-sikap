<?php namespace App\Models;

use CodeIgniter\Model;

class User_model extends Model
{

    protected $table = "user";
    protected $primaryKey = 'userid';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['username','password','idlevel','nip','status', 'email'];
    protected $returnType = 'object';
    protected $beforeInsert = ['hashPassword'];
    protected $beforeUpdate = ['hashPassword'];

    public function data() {
        $builder = $this->db->table('user u')->join("level l","l.idlevel=u.idlevel");
        return $builder->get();
    }

    protected function hashPassword(array $data)
    {
        if (! isset($data['data']['password'])){
            return $data;
        }
        $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_BCRYPT);
        return $data;
    }

    public function jLevel($username){
        $r = $this->join('level l','l.idlevel = user.idlevel')
                ->where('user.username',$username)
                ->first();
        return $r;
    }

    public function cariuser($mail){
      $q = $this->where('user.email',$mail)->first();
      return $q;
    }

    public function getUser($id = false)
    {

		if($id === false){
            return $this->table('user')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('user')
                        ->where('userid', $id)
                        ->get()
                        ->getRowArray();
        }
    }

	public function getUserAdmin()
    {
            return $this->query("SELECT * FROM user WHERE level != 'Administrator'")
                        ->getResultArray();
    }

	public function getEmployee()
    {
			$level = "Employee";
            return $this->table('user')
                        ->where('level', $level)
                        ->get()
                        ->getResultArray();
    }

    public function insert_user($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

    public function update_user($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['userid' => $id]);
    }

    public function delete_user($id)
    {
        return $this->db->table($this->table)->delete(['userid' => $id]);
    }


}
