<?php namespace App\Models;

use CodeIgniter\Model;

class Tim_penilai_model extends Model
{

//    protected $table = "pegawai";

//    public function getPegawai($id = null)
//    {
//        if (!empty($id )) {
//            return $this->table('pegawai')
//                        ->join('m_pangkat', 'm_pangkat.idpangkat = pegawai.idpangkat')
//                        ->join('m_pendidikan', 'm_pendidikan.idpendidikan = pegawai.idpendidikan')
//                        ->join('eselon2', 'eselon2.ideselon2 = pegawai.eselon')                                         
//                        ->get()
//                        ->getResultArray();

//        } else {
//            return $this->table('pegawai')
//                        ->where('pegawai.idpegawai', $id)
//                        ->get()
//                        ->getResultArray();
//        }
//    }    

    public function getM_pangkat()
    {
        $builder = $this->db->table('m_pangkat');
        return $builder->get();
    }

    public function getM_golongan()
    {
        $builder = $this->db->table('m_golongan');
        return $builder->get();
    }    

    public function getM_pendidikan()
    {
        $builder = $this->db->table('m_pendidikan');
        return $builder->get();
    }

    public function getEselon2()
    {
        $builder = $this->db->table('eselon2');
        return $builder->get();
    }   

    public function getJenis_pemeriksa()
    {
        $builder = $this->db->table('jenis_pemeriksa');
        return $builder->get();
    }  

    public function getTim_penilai()
    {
        $builder = $this->db->table('tim_penilai');
        $builder->select('*');
        $builder->join('m_pangkat', 'm_pangkat.idpangkat = tim_penilai.idpangkat','left');
        $builder->join('m_golongan', 'm_golongan.idgolongan = tim_penilai.idgolongan','left');        
        $builder->join('m_pendidikan', 'm_pendidikan.idpendidikan = tim_penilai.idpendidikan','left');
        $builder->join('eselon2', 'eselon2.ideselon2 = tim_penilai.ideselon2','left',);
        $builder->join('jenis_pemeriksa', 'jenis_pemeriksa.idjp = tim_penilai.idjp','left',);                        
        return $builder->get();

    }     

    public function insert_tim_penilai($data)
    {
        $query = $this->db->table('tim_penilai')->insert($data);
        return $query;
    }

    public function update_tim_penilai($data, $id)
    {
        $query = $this->db->table('tim_penilai')->update($data, array('idtp' => $id));
        return $query;        
    }

    public function delete_tim_penilai($id)
    {
        $query = $this->db->table('tim_penilai')->delete(array('idtp' => $id));
        return $query;        
    }
}