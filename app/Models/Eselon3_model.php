<?php namespace App\Models;

use CodeIgniter\Model;

class Eselon3_model extends Model
{

    protected $table = "eselon3";

    public function getEselon3($id = false)
    {
        if ($id === false) {
            return $this->table('eselon3')
                        ->join('eselon1', 'eselon1.ideselon1 = eselon3.ideselon1')
                        ->join('eselon2', 'eselon2.ideselon2 = eselon3.ideselon2')                        
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('eselon3')
                        ->where('ideselon3', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_eselon3($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_eselon3($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['ideselon3' => $id]);
    }

    public function delete_eselon3($id)
    {
        return $this->db->table($this->table)->delete(['ideselon3' => $id]);
    }
}