<?php namespace App\Models;

use CodeIgniter\Model;

class Ms_kelompok_model extends Model
{

    public function getMs_kelompok()
    {
        $builder = $this->db->table('ms_kelompok');
        return $builder->get();
        
//        if($id === false){
//          return $this->table('ms_jabatan')
//                      ->get()
//                      ->getResultArray();
//          } else {
//           return $this->table('ms_jabatan')
//                 ->where('id_jabatan', $id)
//                 ->get()
//                 ->getRowArray();
//}   
    }
    
    public function saveMs_kelompok($data)
    {
        $query = $this->db->table('ms_kelompok')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateMs_kelompok($data, $id)
    {
        $query = $this->db->table('ms_kelompok')->update($data, array('id_kelompok' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteMs_pendidikan($id)
    {
        $query = $this->db->table('ms_kelompok')->delete(array('id_kelompok' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}