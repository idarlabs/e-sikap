<?php namespace App\Models;

use CodeIgniter\Model;

class M_pangkatgol_model extends Model
{

    public function getM_pangkatgol()
    {
        $builder = $this->db->table('m_pangkatgol');
        return $builder->get();
        
//        if($id === false){
//          return $this->table('ms_jabatan')
//                      ->get()
//                      ->getResultArray();
//          } else {
//           return $this->table('ms_jabatan')
//                 ->where('id_jabatan', $id)
//                 ->get()
//                 ->getRowArray();
//}   
    }
    
    public function saveM_pangkatgol($data)
    {
        $query = $this->db->table('m_pangkatgol')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateM_pangkatgol($data, $id)
    {
        $query = $this->db->table('m_pangkatgol')->update($data, array('idpangkatgol' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteM_pangkatgol($id)
    {
        $query = $this->db->table('m_pangkatgol')->delete(array('idpangkatgol' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}