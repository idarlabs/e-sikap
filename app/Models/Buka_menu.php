<?php 
namespace App\Models;
use CodeIgniter\Model;

class Buka_menu extends Model
{

    protected $table = "buka_menu";
    protected $primaryKey = 'idbuka_menu';
    protected $useAutoIncrement = false;
    protected $allowedFields = ['tgl_admin_buka','idperiode','status'];
    protected $returnType = 'object';

    public function data() {
        $builder = $this->db->table('buka_menu');
        return $builder->get();
    }

    public function jPeriode(){
        $result = $this->join('periode p','p.idperiode = buka_menu.idperiode')
                    ->where('buka_menu.idbuka_menu','1')
                    ->first();
        return $result;
    }

    public function jPeriode2(){
        $result = $this->db->table('buka_menu')
                    ->join('periode p','p.idperiode = buka_menu.idperiode')
                    ->where('buka_menu.idbuka_menu','1')
                    ->get()->getResultArray();
        return $result;
    }

    public function simpan($data){
        $query = $this->db->table('buka_menu')->insert($data);
        return $query;
    }
}