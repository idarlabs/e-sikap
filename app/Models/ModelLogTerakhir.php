<?php namespace App\Models;

use CodeIgniter\Model;

class ModelLogTerakhir extends Model
{

    public function data() {
        $builder = $this->db->table('log_pengajuan_terakhir');
        return $builder->get();
    }

    public function cek($nip, $idperiode){
      $builder = $this->db->table('log_pengajuan_terakhir')
      ->where(['nip' => $nip, 'idperiode' => $idperiode]);
      return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('log_pengajuan_terakhir')->where("idlog",$id);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('log_pengajuan_terakhir')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('log_pengajuan_terakhir')->update($data, array('idlog' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('log_pengajuan_terakhir')->delete(array('idlog' => $id));
        return $query;
    }
}
