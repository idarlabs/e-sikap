<?php namespace App\Models;

use CodeIgniter\Model;

class ModelButir1 extends Model
{

    public function data() {
        $builder = $this->db->table('butir1');
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('butir1')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('butir1')->update($data, array('idbutir1' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('butir1')->delete(array('idbutir1' => $id));
        return $query;
    }
}
