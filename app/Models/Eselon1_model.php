<?php namespace App\Models;

use CodeIgniter\Model;

class Eselon1_model extends Model
{

    protected $table = "eselon1";

    public function getEselon1($id = false)
    {
        if ($id === false) {
            return $this->table('eselon1')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('eselon1')
                        ->where('ideselon1', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_eselon1($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_eselon1($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['ideselon1' => $id]);
    }

    public function delete_eselon1($id)
    {
        return $this->db->table($this->table)->delete(['ideselon1' => $id]);
    }
}