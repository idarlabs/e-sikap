<?php namespace App\Models;

use CodeIgniter\Model;

class ModelPengangkatan extends Model
{

    public function data() {
        $builder = $this->db->table('pengangkatan');
        return $builder->get();
    }

    public function dataid($idgolongan) {
        $builder = $this->db->table('pengangkatan')
        ->join('m_golongan mg', 'mg.idgolongan=pengangkatan.gol_naik')
        ->join('m_pangkat mp', 'mp.idpangkat=mg.idpangkat')
        ->where("gol_saatini",$idgolongan);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('pengangkatan')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('pengangkatan')->update($data, array('idpengangkatan' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('pengangkatan')->delete(array('idpengangkatan' => $id));
        return $query;
    }
}
