<?php 
namespace App\Models;
use CodeIgniter\Model;

class M_Pangkat extends Model
{

    protected $table = "m_pangkat";
    protected $primaryKey = 'idpangkat';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['pangkat'];
    protected $returnType = 'object';

}