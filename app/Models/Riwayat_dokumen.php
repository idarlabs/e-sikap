<?php
namespace App\Models;
use CodeIgniter\Model;

class Riwayat_dokumen extends Model
{
    protected $table = "riwayat_dokumen";
    protected $primaryKey = 'idriwayatdokumen';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['path','idriwayatpengajuan','tgl_upload','jam_upload'];
    protected $returnType = 'object';

    public function data() {
        $builder = $this->db->table('riwayat_dokumen');
        return $builder->get();
    }

    public function updatedata($data, $id) {
        $query = $this->db->table('riwayat_dokumen')->update($data, array('idriwayatpengajuan' => $id));
        return $query;
    }

    public function simpan($data){
        $query = $this->db->table('riwayat_dokumen')->insert($data);
        return $query;
    }

}
