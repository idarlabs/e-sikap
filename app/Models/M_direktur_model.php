<?php namespace App\Models;

use CodeIgniter\Model;

class M_direktur_model extends Model
{

    protected $table = "m_direktur";

    public function getM_direktur($id = null)
    {
        if (empty($id )) {
            return $this->table('m_direktur')
                        ->get()
                        ->getResultArray();
        }
    }

    public function saveM_direktur($data)
    {
        $query = $this->db->table('m_direktur')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateM_direktur($data, $id)
    {
        $query = $this->db->table('m_direktur')->update($data, array('iddirektur' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteM_direktur($id)
    {
        $query = $this->db->table('m_direktur')->delete(array('iddirektur' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}
