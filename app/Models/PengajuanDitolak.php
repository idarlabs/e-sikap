<?php
namespace App\Models;
use CodeIgniter\Model;

class PengajuanDitolak extends Model
{

    public function simpan($data){
        $query = $this->db->table('riwayat_pengajuan_ditolak')->insert($data);
        return $query;
    }

    public function draft($id, $idstep) {
        $builder = $this->db->table('pengajuan p')
        ->select("p.idpengajuan, rp.idpengajuan, rp.idstep, rp.idriwayatpengajuan, rp.kredit, rp.idkegiatan, rd.path, mk.masterkegiatan, k.butirkegiatan, p.status, rp.keterangan")
        ->join("riwayat_pengajuan_ditolak rp", "rp.idpengajuan=p.idpengajuan")
        ->join("riwayat_dokumen rd", "rd.idriwayatpengajuan=rp.idriwayatpengajuan")
        ->join("kegiatan k", "k.idkegiatan=rp.idkegiatan")
        ->join("m_kegiatan mk", "mk.idmasterkegiatan=k.idmasterkegiatan")
        ->where(['p.idpengajuan' => $id, 'rp.idstep' => $idstep])->orderBy("rp.idriwayatpengajuan","DESC");
        return $builder->get();
    }

}
