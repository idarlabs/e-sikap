<?php namespace App\Models;

use CodeIgniter\Model;

class Jenis_pemeriksa_model extends Model
{

    public function getJenis_pemeriksa()
    {
        $builder = $this->db->table('jenis_pemeriksa');
        return $builder->get()
                        ->getResultArray();

//        if($id === false){
//          return $this->table('ms_jabatan')
//                      ->get()
//                      ->getResultArray();
//          } else {
//           return $this->table('ms_jabatan')
//                 ->where('id_jabatan', $id)
//                 ->get()
//                 ->getRowArray();
//}   
    }
    
    public function saveJenis_pemeriksa($data)
    {
        $query = $this->db->table('jenis_pemeriksa')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateJenis_pemeriksa($data, $id)
    {
        $query = $this->db->table('jenis_pemeriksa')->update($data, array('idjp' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteJenis_pemeriksa($id)
    {
        $query = $this->db->table('jenis_pemeriksa')->delete(array('idjp' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}