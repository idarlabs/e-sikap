<?php namespace App\Models;

use CodeIgniter\Model;

class M_Golongan_model extends Model
{

    protected $table = "m_golongan";

    public function getM_golongan($id=null)
    {

        if (empty($id )) {
            return $this->table('m_golongan')
                        ->join('m_jabatan', 'm_jabatan.idjabatan = m_golongan.idjabatan')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_golongan')
                        ->where('idgolongan', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_m_golongan($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_golongan($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idgolongan' => $id]);
    }

    public function delete_m_golongan($id)
    {
        return $this->db->table($this->table)->delete(['idgolongan' => $id]);
    }
}