<?php namespace App\Models;

use CodeIgniter\Model;

class ModelButir2 extends Model
{

    public function data() {
        $builder = $this->db->table('butir2');
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('butir2')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('butir2')->update($data, array('idbutir2' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('butir2')->delete(array('idbutir2' => $id));
        return $query;
    }
}
