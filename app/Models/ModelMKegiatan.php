<?php namespace App\Models;

use CodeIgniter\Model;

class ModelMKegiatan extends Model
{

    public function data() {
        $builder = $this->db->table('m_kegiatan');
        return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('m_kegiatan')->where("idsubunsur",$id);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('m_kegiatan')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('m_kegiatan')->update($data, array('m_kegiatan' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('m_kegiatan')->delete(array('m_kegiatan' => $id));
        return $query;
    }
}
