<?php
namespace App\Models;
use CodeIgniter\Model;

class Riwayat_pengajuan extends Model
{
    protected $table = "riwayat_pengajuan";
    protected $primaryKey = 'idriwayatpengajuan';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['idpengajuan','tgl_submit','jam_submit','kredit','idkegiatan','idstep','nip'];
    protected $returnType = 'object';

    public function hapus($id){
        $query = $this->db->table('riwayat_pengajuan')->delete(array('idriwayatpengajuan' => $id));
        return $query;
    }

    public function ambilsebaris($idriwayatpengajuan){
      $query = $this->db->table('riwayat_pengajuan')->where('idriwayatpengajuan', $idriwayatpengajuan);
      return $query->get()->getResultArray()[0];
    }

    public function data($id){
      $q = $this->db->table('riwayat_pengajuan rp')
      ->join("riwayat_dokumen rd", "rd.idriwayatpengajuan=rp.idriwayatpengajuan")
      ->join("kegiatan k", "k.idkegiatan=rp.idkegiatan")
      ->join("m_kegiatan mk", "mk.idmasterkegiatan=k.idmasterkegiatan")
      ->where(["rp.idpengajuan" => $id, 'rp.idstep' => '2']);
      return $q->get();
    }

    public function updatedata($data, $id) {
        $query = $this->db->table('riwayat_pengajuan')->update($data, array('idriwayatpengajuan' => $id));
        return $query;
    }

    public function ambildata($idpengajuan, $idstep){
      $wer = ['p.idpengajuan' => $idpengajuan, 'rp.idstep' => $idstep];

      $query = $this->db->table('pengajuan p')
      ->select("p.idpengajuan,p.nip,p.ideselon,rp.idkegiatan,rp.kredit,rp.idriwayatpengajuan,k.idjenispemeriksakerja, k.idjenisunsur, k.idunsur,k.idsubunsur,rp.idstep,u.idkategori, s.keterangan")
      ->join("riwayat_pengajuan rp", "rp.idpengajuan = p.idpengajuan")
      ->join("kegiatan k", "k.idkegiatan = rp.idkegiatan")
      ->join("m_unsur u", "u.idunsur = k.idunsur")
      ->join("m_sub_unsur s", "s.idsubunsur = k.idsubunsur")
      ->where($wer);
      return $query->get();
    }

    public function jumlahTerakhir($idperiode, $idstep, $nip){
      $wer = [
        'p.idperiode' => $idperiode,
        'rp.idstep' => $idstep,
        'p.nip' => $nip,
      ];

      $query = $this->db->table('pengajuan p')
      ->select("p.idperiode, p.idpengajuan,p.nip,p.ideselon,rp.idkegiatan,rp.kredit,rp.idriwayatpengajuan,k.idjenispemeriksakerja, k.idjenisunsur, k.idunsur,k.idsubunsur,rp.idstep,u.idkategori, s.keterangan")
      ->join("riwayat_pengajuan rp", "rp.idpengajuan = p.idpengajuan")
      ->join("kegiatan k", "k.idkegiatan = rp.idkegiatan")
      ->join("m_unsur u", "u.idunsur = k.idunsur")
      ->join("m_sub_unsur s", "s.idsubunsur = k.idsubunsur")
      ->where($wer);
      return $query->get();
    }

    public function jumlahTerakhir2($idperiode='', $idstep='', $nip=''){
      $this->join('pengajuan p','p.idpengajuan = riwayat_pengajuan.idpengajuan')
        ->join("kegiatan k", "k.idkegiatan = riwayat_pengajuan.idkegiatan")
        ->join("m_unsur u", "u.idunsur = k.idunsur")
        ->join("m_sub_unsur s", "s.idsubunsur = k.idsubunsur");
      if ($idperiode != '') {
        $this->where('p.idperiode',$idperiode);
      }
      if ($idstep != '') {
        $this->where('riwayat_pengajuan.idstep',$idstep);
      }
      if ($nip != '') {
        $this->where('p.nip',$nip);
      }
      return $this->findAll();
    }

    public function semuariwayatpengaju($nip, $idstep){
      $wer = [
        'rp.idstep' => $idstep,
        'p.nip' => $nip,
      ];

      $query = $this->db->table('pengajuan p')
      ->select("p.idperiode, p.idpengajuan, p.nip,p.ideselon,rp.idkegiatan,rp.kredit,rp.idriwayatpengajuan,k.idjenispemeriksakerja, k.idjenisunsur, k.idunsur,k.idsubunsur,rp.idstep,u.idkategori, s.keterangan")
      ->join("riwayat_pengajuan rp", "rp.idpengajuan = p.idpengajuan")
      ->join("kegiatan k", "k.idkegiatan = rp.idkegiatan")
      ->join("m_unsur u", "u.idunsur = k.idunsur")
      ->join("m_sub_unsur s", "s.idsubunsur = k.idsubunsur")
      ->where($wer);
      return $query->get();
    }


}
