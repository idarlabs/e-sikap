<?php namespace App\Models;

use CodeIgniter\Model;

class Precutoff extends Model
{

    public function data() {
        $builder = $this->db->table('pegawai peg')
        ->select("peg.nip as nip1, peg.nama, mg.golongan, peg.tmt_pegawai, pre.*")
        ->join('precutoff pre', 'pre.nip=peg.nip', 'left')
        ->join('m_golongan mg', 'mg.idgolongan=peg.idgolongan');
        return $builder->get();
    }

    public function nilaiku($nip){
      $builder = $this->db->table('precutoff pre')
      ->where(['nip' => $nip]);
      return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('precutoff')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('precutoff')->update($data, array('idprecutoff' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('precutoff')->delete(array('idprecutoff' => $id));
        return $query;
    }
}
