<?php 
namespace App\Models;
use CodeIgniter\Model;

class M_Golongan extends Model
{

    protected $table = "m_golongan";
    protected $primaryKey = 'idgolongan';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['idjabatan,golongan'];
    protected $returnType = 'object';

}