<?php 
namespace App\Models;
use CodeIgniter\Model;

class Periode extends Model
{
    protected $table = "periode";
    protected $primaryKey = 'idperiode';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['kode','tgl_buka','tgl_tutup'];
    protected $returnType = 'object';

}