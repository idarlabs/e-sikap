<?php
namespace App\Models;
use CodeIgniter\Model;
class Pengangkatan extends Model{
    protected $table = 'pengangkatan';
    protected $primaryKey = 'idpengangkatan';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['idpengangkatan','gol_saatini','gol_naik','minimum'];
    protected $returnType = 'object';
}