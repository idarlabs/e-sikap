<?php 
namespace App\Models;
use CodeIgniter\Model;

class Kegiatan extends Model
{
    protected $table = "kegiatan";
    protected $primaryKey = "idkegiatan";
    protected $useAutoIncrement = true;
    protected $allowedFields = ['idjenispemeriksakerja','idjenisunsur','idunsur','idsubunsur','idmasterkegiatan','butirkegiatan','idjabatan','kredit'];
    protected $returnType = 'object';

}