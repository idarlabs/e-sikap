<?php namespace App\Models;

use CodeIgniter\Model;

class Eselon2_model extends Model
{

    protected $table = "eselon2";

    public function getEselon2($id = false)
    {
        if ($id === false) {
            return $this->table('eselon2')
                        ->join('eselon1', 'eselon1.ideselon1 = eselon2.ideselon1')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('eselon2')
                        ->where('ideselon2', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_eselon2($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_eselon2($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['ideselon2' => $id]);
    }

    public function delete_eselon2($id)
    {
        return $this->db->table($this->table)->delete(['ideselon2' => $id]);
    }
}