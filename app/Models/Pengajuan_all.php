<?php
namespace App\Models;
use CodeIgniter\Model;

class Pengajuan_all extends Model
{
    protected $table = "pengajuan";
    protected $primaryKey = 'idpengajuan';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['nip','idperiode','ideselon','status', 'idbidang'];
    protected $returnType = 'object';

    public function draft($id, $idstep) {
        $builder = $this->db->table('pengajuan p')
        ->select("p.idpengajuan, rp.idpengajuan, rp.idstep, rp.idriwayatpengajuan, rp.kredit, rp.idkegiatan, rd.path, mk.masterkegiatan, k.butirkegiatan, p.status")
        ->join("riwayat_pengajuan rp", "rp.idpengajuan=p.idpengajuan")
        ->join("riwayat_dokumen rd", "rd.idriwayatpengajuan=rp.idriwayatpengajuan")
        ->join("kegiatan k", "k.idkegiatan=rp.idkegiatan")
        ->join("m_kegiatan mk", "mk.idmasterkegiatan=k.idmasterkegiatan")
        ->where(['p.idpengajuan' => $id, 'rp.idstep' => $idstep])->orderBy("rp.idriwayatpengajuan","DESC");
        return $builder->get();
    }

    public function data($idperiode, $ideselon=null, $idbidang=null){ // list pengajuan penilai <--
      $builder = $this->db->table('pengajuan')
      ->join("pegawai p", "p.nip=pengajuan.nip")
      ->join("m_golongan mg", "mg.idgolongan=p.idgolongan")
      ->join("m_jabatan mj", "mj.idjabatan=mg.idjabatan")
      ->where(['pengajuan.idperiode' => $idperiode, 'pengajuan.status' => '1']);
      if(!empty($ideselon) && !empty($idbidang)){
          $builder->where(['pengajuan.ideselon' => $ideselon, 'pengajuan.idbidang' => $idbidang]);
      }
      return $builder->get();
    }

    public function dataSearchByPeriodeAndBidang($idperiode, $idbidang){ // list pengajuan penilai <--
      $builder = $this->db->table('pengajuan')
      ->join("pegawai p", "p.nip=pengajuan.nip")
      ->where(['pengajuan.idbidang' => $idbidang, 'pengajuan.idperiode' => $idperiode]);
      return $builder->get();
    }

    public function datapengajuan($idpengajuan){
      $builder = $this->db->table('pengajuan')
      ->join("pegawai p", "p.nip=pengajuan.nip")
      ->join("m_golongan mg", "mg.idgolongan=p.idgolongan")
      ->join("m_jabatan mj", "mj.idjabatan=mg.idjabatan")
      ->join("jenis_pemeriksa jp", "jp.idjp=p.idbidang")
      ->where(['idpengajuan' => $idpengajuan]);
      return $builder->get();
    }

    public function jPeriode($nip, $status){
      $this->join('periode p','p.idperiode = pengajuan.idperiode')
        ->where('nip',$nip)
        ->where('status',$status);
      return $this->findAll();
    }

}
