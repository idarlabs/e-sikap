<?php namespace App\Models;

use CodeIgniter\Model;

class ModelKegiatan extends Model
{

    public function data() {
        $builder = $this->db->table('kegiatan');
        return $builder->get();
    }

    public function getdata($a, $b, $c, $d, $e) {
      // Bisa berulang kali
      $arr = array('idjenispemeriksakerja' => $a, 'idjenisunsur' => $b, 'idunsur' => $c, 'idsubunsur' => $d, 'idmasterkegiatan' => $e);
      $builder = $this->db->table('kegiatan')->where($arr);

      // Kalo sudah pernah dia gak muncul
      // $arr = array('k.idjenispemeriksakerja' => $a, 'k.idjenisunsur' => $b, 'k.idunsur' => $c, 'k.idsubunsur' => $d, 'k.idmasterkegiatan' => $e, 'rp.idriwayatpengajuan is null');
      // $builder = $this->db->table('kegiatan k')->join('riwayat_pengajuan rp', 'rp.idkegiatan=k.idkegiatan', 'left')->where($arr);

      return $builder->get();
    }

    public function data2() {
        $builder = $this->db->table('kegiatan k')->select("jpk.jenis as jpkjenis, k.*, jpk.*, ju.*, mu.*, msu.*, mke.*, mj.*")->join("m_jpk jpk", "jpk.idjenispemeriksakerja=k.idjenispemeriksakerja")
        ->join("jenis_unsur ju", "ju.idjenisunsur=k.idjenisunsur")->join("m_unsur mu", "mu.idunsur=k.idunsur")
        ->join("m_sub_unsur msu", "msu.idsubunsur=k.idsubunsur")->join("m_kegiatan mke","mke.idmasterkegiatan=k.idmasterkegiatan")
        ->join("m_jabatan mj", "mj.idjabatan=k.idjabatan")->orderBy('idkegiatan', 'DESC');
        return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('kegiatan')->where("idkegiatan",$id);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('kegiatan')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('kegiatan')->update($data, array('idkegiatan' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('kegiatan')->delete(array('idkegiatan' => $id));
        return $query;
    }
}
