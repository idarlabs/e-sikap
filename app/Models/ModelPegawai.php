<?php namespace App\Models;

use CodeIgniter\Model;

class ModelPegawai extends Model
{

    public function data($nip) {
      $where = ['nip' => $nip];

      $builder = $this->db->table('pegawai')
      ->join("jenis_pemeriksa j","j.idjp=pegawai.idbidang")
      ->where($where);
      return $builder->get();
    }

    public function datalengkap($nip){
      $q = $this->db->table('pegawai p')
      ->join("jenis_pemeriksa jp", "jp.idjp=p.idbidang")
      ->join("m_golongan mg", "mg.idgolongan=p.idgolongan")
      ->join("m_pangkat mp", "mp.idpangkat=p.idpangkat")
      ->join("m_jekel je", "je.idjekel=p.jk")
      ->join("m_jabatan ja", "ja.idjabatan=mg.idjabatan")
      ->join("m_pendidikan mpen", "mpen.idpendidikan=p.idpendidikan")
      ->where('nip', $nip);
      return $q->get();
    }
}
