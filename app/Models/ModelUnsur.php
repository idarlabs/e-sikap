<?php namespace App\Models;

use CodeIgniter\Model;

class ModelUnsur extends Model
{

    public function data() {
        $builder = $this->db->table('m_unsur');
        return $builder->get();
    }

    public function data2($idjenisunsur, $idbidang) {
        $builder = $this->db->table('m_unsur')
                        ->where(['idjenisunsur' => $idjenisunsur, 'idbidang' => $idbidang]);
        return $builder->get();
    }

    public function dataid($id,$bidang_id) {
        $builder = $this->db->table('m_unsur')
                ->where(["idjenisunsur" => $id,'idbidang' =>$bidang_id]);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('m_unsur')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('m_unsur')->update($data, array('idunsur' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('m_unsur')->delete(array('idunsur' => $id));
        return $query;
    }
}
