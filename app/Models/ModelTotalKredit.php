<?php
namespace App\Models;
use CodeIgniter\Model;

class ModelTotalKredit extends Model
{

  public function simpan($data){
      $query = $this->db->table('total_kredit')->insert($data);
      return $query;
  }

  public function dataku($nip, $idpengajuan){
    $q = $this->db->table('total_kredit')
    ->where(['nip' => $nip, 'idpengajuan' => $idpengajuan]);
    return $q->get();
  }

}
