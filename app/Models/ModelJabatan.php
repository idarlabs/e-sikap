<?php namespace App\Models;

use CodeIgniter\Model;

class ModelJabatan extends Model
{

    public function data() {
        $builder = $this->db->table('m_jabatan');
        return $builder->get();
    }

    public function dataid($id) {
        $builder = $this->db->table('m_jabatan')->where("idjabatan",$id);
        return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('m_jabatan')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('m_jabatan')->update($data, array('idjabatan' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('m_jabatan')->delete(array('idjabatan' => $id));
        return $query;
    }
}
