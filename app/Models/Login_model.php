<?php namespace App\Models;

use CodeIgniter\Model;

class Login_model extends Model
{

    protected $table = "user";

    public function cekLogin($username, $password){
	return $this->table('user u')
                        ->where(array('username' => $username, 'password' => $password, 'status' => 1))
                        ->countAllResults();
    }

	public function getLogin($username, $password)
    {
	return $this->table('user')->join("level l", "l.idlevel=user.idlevel")
                        ->where(array('username' => $username, 'password' => $password, 'status' => 1))
                        ->get()
                        ->getRowArray();
    }

}
