<?php 
namespace App\Models;
use CodeIgniter\Model;

class M_Pendidikan extends Model
{

    protected $table = "m_pendidikan";
    protected $primaryKey = 'idpendidikan';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['pendidikan'];
    protected $returnType = 'object';

}