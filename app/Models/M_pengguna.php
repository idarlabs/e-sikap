<?php namespace App\Models;

use CodeIgniter\Model;

class M_pengguna extends Model
{

    public function get_data($username, $password)
    {
        return $this->db->table('pengguna')
        ->where(array('username' => $username, 'password' => $password))
        ->get()->getRowArray();
    }


}