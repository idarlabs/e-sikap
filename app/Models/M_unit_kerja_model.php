<?php namespace App\Models;

use CodeIgniter\Model;

class M_unit_kerja_model extends Model
{

    public function getM_unit_kerja()
    {
        $builder = $this->db->table('m_unit_kerja');
        return $builder->get();
        
//        if($id === false){
//          return $this->table('ms_jabatan')
//                      ->get()
//                      ->getResultArray();
//          } else {
//           return $this->table('ms_jabatan')
//                 ->where('id_jabatan', $id)
//                 ->get()
//                 ->getRowArray();
//}   
    }
    
    public function saveM_unit_kerja($data)
    {
        $query = $this->db->table('m_unit_kerja')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateM_unit_kerja($data, $id)
    {
        $query = $this->db->table('m_unit_kerja')->update($data, array('idunitkerja' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteM_unit_kerja($id)
    {
        $query = $this->db->table('m_unit_kerja')->delete(array('idunitkerja' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}