<?php 
namespace App\Models;
use CodeIgniter\Model;

class M_Pangkat_model extends Model
{

    protected $table = "m_pangkat";
    protected $primaryKey = 'idpangkat';
    protected $useAutoIncrement = true;
    protected $allowedFields = ['pangkat'];
    protected $returnType = 'object';

    public function getM_pangkat($id=null)
    {

        if (empty($id )) {
            return $this->table('m_pangkat')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_pangkat')
                        ->where('idpangkat', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_m_pangkat($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_pangkat($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idpangkat' => $id]);
    }

    public function delete_m_pangkat($id)
    {
        return $this->db->table($this->table)->delete(['idpangkat' => $id]);
    }
}