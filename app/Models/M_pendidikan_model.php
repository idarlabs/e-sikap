<?php namespace App\Models;

use CodeIgniter\Model;

class M_pendidikan_model extends Model
{

    public function getM_pendidikan()
    {
        $builder = $this->db->table('m_pendidikan');
        return $builder->get()
                        ->getResultArray();

//        if($id === false){
//          return $this->table('ms_jabatan')
//                      ->get()
//                      ->getResultArray();
//          } else {
//           return $this->table('ms_jabatan')
//                 ->where('id_jabatan', $id)
//                 ->get()
//                 ->getRowArray();
//}   
    }
    
    public function saveM_pendidikan($data)
    {
        $query = $this->db->table('m_pendidikan')->insert($data);
        return $query;
    }
//  public function insert_ms_jabatan($data)
//  {
//  return $this->db->table($this->table)->insert($data);
//  }

    public function updateM_pendidikan($data, $id)
    {
        $query = $this->db->table('m_pendidikan')->update($data, array('idpendidikan' => $id));
        return $query;
        //return $this->db->table($this->table)->update($data, ['id_jabatan' => $id]);
    }

    public function deleteM_pendidikan($id)
    {
        $query = $this->db->table('m_pendidikan')->delete(array('idpendidikan' => $id));
        return $query;
    }
//    public function delete_ms_jabatan($id)
//    {
//        return $this->db->table($this->table)->delete(['id_jabatan' => $id]);
//    }


}