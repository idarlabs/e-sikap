<?php namespace App\Models;

use CodeIgniter\Model;

class M_jabatan_model extends Model
{

    protected $table = "m_jabatan";

    public function getM_jabatan($id = null)
    {
        if (empty($id )) {
            return $this->table('m_jabatan')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_jabatan')
                        ->where('idjabatan', $id)
                        ->get()
                        ->getRowArray();
        }
    }    

    public function insert_m_jabatan($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_jabatan($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idjabatan' => $id]);
    }

    public function delete_m_jabatan($id)
    {
        return $this->db->table($this->table)->delete(['idjabatan' => $id]);
    }
}