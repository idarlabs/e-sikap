<?php namespace App\Models;

use CodeIgniter\Model;

class M_unsur_model extends Model
{

    protected $table = "m_unsur";

    public function getM_unsur($id = null)
    {
        if (empty($id )) {
            return $this->table('m_unsur')
                        ->join('jenis_unsur', 'jenis_unsur.idjenisunsur = m_unsur.idjenisunsur')
                        ->join('jenis_pemeriksa', 'jenis_pemeriksa.idjp = m_unsur.idbidang')
                        ->get()
                        ->getResultArray();
        } else {
            return $this->table('m_unsur')
                        ->where('idunsur', $id)
                        ->get()
                        ->getRowArray();
        }
    }

    public function insert_m_unsur($data)
    {
        return $this->db->table($this->table)->insert($data);
    }

   public function update_m_unsur($data, $id)
    {
        return $this->db->table($this->table)->update($data, ['idunsur' => $id]);
    }

    public function delete_m_unsur($id)
    {
        return $this->db->table($this->table)->delete(['idunsur' => $id]);
    }
}
