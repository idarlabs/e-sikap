<?php namespace App\Models;

use CodeIgniter\Model;

class ModelRiwayatBaru extends Model {
  protected $table = 'riwayat_baru';
  protected $primaryKey = 'idriwayatbaru';
  protected $useAutoIncrement = true;
  protected $allowedFields = ['nip','idpengajuan','status','sk','tmt','gol_sebelum','gol_setelah','tgl_submit','jam_submit','idperiode','path_dokumen'];
  protected $returnType = 'object';

    public function data() {
        $builder = $this->db->table('riwayat_baru');
        return $builder->get();
    }

    public function datalast($nip){
      $builder = $this->db->table('riwayat_baru')
      ->where('nip', $nip)
      ->orderBy('idriwayatbaru', 'DESC')
      ->limit(0, 1);
      return $builder->get();
    }

    public function simpan($data){
        $query = $this->db->table('riwayat_baru')->insert($data);
        return $query;
    }

    public function edit($data, $id) {
        $query = $this->db->table('riwayat_baru')->update($data, array('idriwayatbaru' => $id));
        return $query;
    }

    public function hapus($id){
        $query = $this->db->table('riwayat_baru')->delete(array('idriwayatbaru' => $id));
        return $query;
    }

}
