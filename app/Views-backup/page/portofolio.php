  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Pegawai</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Pegawai</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                  src="<?= base_url() ?>/assets/dist/img/avatar6.png"
                  alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">MARCHIENDA WERDANY, SH</h3>

                <p class="text-muted text-center">198103112003122001</p>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
             <!--   <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Activity</a></li>
                </ul>
              </div /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                    <div class="post">
                     <div class="col-12 table-responsive">
                       <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th colspan="2">Keterangan Perorangan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Nama</td>
                            <td>MARCHIENDA WERDANY, SH</td>
                          </tr>
                          <tr>
                            <td>NIP</td>
                            <td>198103112003122001</td>
                          </tr>
                          <tr>
                            <td>Nomor Seri Kartu Pegawai</td>
                            <td>M.020932</td>
                          </tr>
                          <tr>
                            <td>Pangkat dan Golongan Ruang / Terhitung Mulai Tanggal</td>
                            <td>Pembina Muda (IV/c) / 1 Oktober 2020</td>
                          </tr>
                          <tr>
                            <td>Tempat dan Tanggal Lahir</td>
                            <td>Jakarta, 11 Maret 1981</td>
                          </tr>
                          <tr>
                            <td>Jenis Kelamin</td>
                            <td>Perempuan</td>
                          </tr>
                          <tr>
                            <td>Pendidikan Tertinggi</td>
                            <td>Sarjana Hukum (S1)</td>
                          </tr>
                          <tr>
                            <td>Jabatan Pemeriksa Merek / Terhitung Mulai Tanggal</td>
                            <td>Pemeriksa Merek Madya / 1 Maret 2014</td>
                          </tr>
                          <tr>
                            <td>Unit Kerja</td>
                            <td>Direktorat Merek dan Indikasi Geografis</td>
                          </tr>

                        </tbody>
                      </table>

                    </div>
                      <!-- /.user-block
                        comment -->



                      </div>
                      <!-- /.post -->

                      <!-- Post -->

                      <!-- /.post -->

                      <!-- Post -->

                      <!-- /.post -->
                    </div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->


                    <!-- /.tab-pane -->
                  </div>
                  <!-- /.tab-content -->

                </div><!-- /.card-body -->

              </div>
              <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->

          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
        <div class="accordion" id="accordionGroup">
          <div class="card">
            <a class="card-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
              <div class="card-header" id="headingOne">
                UNSUR UTAMA
              </div>
            </a>

            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionGroup">
              <div class="card-body">
               <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Unsur Utama</th>
                    <th>Lama</th>
                    <th>Baru</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>a. (1) Pendidikan Formal dan mencapai Gelar / Ijasah</td>
                    <td>100</td>
                    <td>0</td>
                    <td>100</td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(2) Pendidikan dan Pelatihan dan mendapat Surat</td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tanda Tamat Pendidikan dan Pelatihan (STTPP)</td>
                    <td>15</td>
                    <td>0</td>
                    <td>15</td>
                  </tr>
                  <tr>
                    <td>b. Pemeriksaan Merek</td>
                    <td>591.8651</td>
                    <td>0.0000</td>
                    <td>591.8651</td>
                  </tr>
                  <tr>
                    <td>c. Pengembangan Profesi Pemeriksa Merek</td>
                    <td>33.5000</td>
                    <td><font color="red">0.0000</font></td>
                    <td>33.5000</td>
                  </tr>
                  <tr>
                    <td><strong>JUMLAH UNSUR UTAMA</strong></td>
                    <td>740.3651</td>
                    <td>0.0000</td>
                    <td>740.3651</td>
                  </tr>                                                                                    
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <div class="card">
          <a class="card-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
            <div class="card-header" id="headingTwo">
              UNSUR PENUNJANG
            </div>
          </a>
          <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionGroup">
            <div class="card-body">
               <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Unsur Penunjang</th>
                    <th>Lama</th>
                    <th>Baru</th>
                    <th>Jumlah</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Pendukung Kegiatan Pemeriksaan Merek</td>
                    <td>81.9440</td>
                    <td>0.0000</td>
                    <td>81.9440</td>
                  </tr>
                  <tr>
                    <td><strong>JUMLAH UNSUR PENUNJANG</strong></td>
                    <td>81.9440</td>
                    <td>0.0000</td>
                    <td>81.9440</td>
                  </tr>                   
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->



    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> 3.0.4
      </div>
      <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->

  <!-- jQuery -->
  <script src="../../plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="../../dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="../../dist/js/demo.js"></script>
</body>
</html>
