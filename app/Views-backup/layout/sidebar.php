  <aside class="main-sidebar elevation-4 sidebar-dark-danger">
    <!-- Brand Logo -->
    <a href="<?php echo base_url('home'); ?>" class="brand-link">
    <img src="<?= base_url() ?>/assets/dist/img/e.png" class="brand-image img-circle elevation-5" style="opacity: .8">
       <span class="brand-text font-weight-bold">E-SIKAP</span>
    </a>

    <!-- Sidebar -->

    <div class="sidebar">
      <!-- Sidebar user panel (optional) 
      <div class="user-panel   ">
        <div class="image">
        </div>
        
        <div class="info">
        </div>
      </div>
      -->
      

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-legacy nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
           <li class="nav-item has-treeview menu-open">
            <a href="<?= base_url() ?>/home" class="nav-link active">
              &nbsp;<i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
            
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-user-alt"></i>
              <p>
                Master Pegawai
                <i class="fas fa-angle-left right"></i>
                
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url() ?>/datapegawai" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Data Pegawai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/portofolio" class="nav-link">
                  <i class="nav-icon fas fa-users"></i>
                  <p>Portofolio</p>
                </a>
              </li>              
              
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Master Data
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url() ?>/pemilihanskp" class="nav-link">
                  <i class="nav-icon fas fa-book"></i>
                  <p>Pemilihan Periode SKP</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/pemilihanpenilai" class="nav-link">
                  <i class="nav-icon fas fa-book-open"></i>
                  <p>Pemilihan Tim Penilai</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/datapenilaian" class="nav-link">
                  <i class="nav-icon fas fa-coins"></i>
                  <p>Data Item Penilaian</p>
                </a>
              </li>

              
            </ul>
          </li>

         <!-- <li class="nav-header">CLASS</li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="fas fa-coins nav-icon"></i>
              <p>
                Class
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?= base_url() ?>/#" class="nav-link">
                  <i class="fas fa-shopping-bag nav-icon"></i>
                  <p>Data</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?= base_url() ?>/#" class="nav-link">
                  <i class="fas fa-tasks nav-icon"></i>
                  <p>Kategori</p>
                </a>
              </li>
              
              </ul>
            </li>-->
          </nav>
        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>